$(function(){
    $('.datetime-input').datetimepicker({
    	timeFormat: 'HH:mm',
		dateFormat: typeof(js_date_format)!=='undefined'?js_date_format:'dd/mm/yy',
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
    });
    
	$('.datetime-input-clear').button();
	
	$('.datetime-input-clear').click(function(){
		$(this).parent().find('.datetime-input').val("");
		return false;
	});	

});