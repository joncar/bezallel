<style type="text/css">
    .oculto{
        display:none;
    }
</style>
<form class="form-horizontal" id='formulario' onsubmit="insertar('boxes/admin/servicios/insert',this,'#response',function(data){var msj = data.success_message.replace('{id}',data.insert_primary_key);$('#response').html('<div class=\'alert alert-success\'>'+msj+'</div>');}); return false;" role="form">
	<div id="response"></div>
    <div class="well">
        <div class="row oculto">
            
            <div class="col-xs-12 col-md-4">
                <div class="form-group ui-widget">
                  <label for="proveedor" class="col-sm-4 control-label">Nro. Documento: </label>
                  <div class="col-sm-8" id="cliente_div">
                      <input type="text" name="cliente_nro_documento" value="" id="cliente-doc" placeholder="Nro. Documento">
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group ui-widget">
                  <label for="proveedor" class="col-sm-4 control-label">Nombre del cliente: </label>
                  <div class="col-sm-8" id="cliente_div">
                      <input type="text" name="cliente_nombre" value="" id="cliente-nombre" placeholder="Nombre del nuevo cliente">
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group ui-widget">
                  <label for="proveedor" class="col-sm-4 control-label">Apellidos del cliente: </label>
                  <div class="col-sm-8" id="cliente_div">
                      <input type="text" name="cliente_apellido" value="" id="cliente-apellido" placeholder="Apellidos del nuevo cliente">
                  </div>
                </div>
            </div>
           
        </div>
	    <div class="row">
	        <div class="col-xs-4">
	            <div class="form-group ui-widget">
	              <label for="proveedor" class="col-sm-4 control-label">Cliente: </label>
	              <div class="col-sm-8" id="cliente_div">
	                  <?= form_dropdown_from_query('clientes_id','clientes','id','nro_documento nombres apellidos',0,'id="cliente"') ?>
                      <a href="javascript:void()" onclick="$('.oculto').toggle('show');$('#cliente').val(''); $('#cliente').chosen().trigger('liszt:updated'); $('#cliente_chzn').toggle('show') ">Añadir</a>
	              </div>
	            </div>
	        </div>
	        <div class="col-xs-4">
	            <div class="form-group">
	              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
	              <div class="col-sm-8">
	                  <input type="search" name="fecha" value="<?= date("d/m/Y") ?>" id="fecha" class="form-control datetime-input">
	              </div>
	            </div>
	        </div>
	        <div class="col-xs-4">
	            <div class="form-group">
	              <label for="proveedor" class="col-sm-4 control-label">Fecha entrega: </label>
	              <div class="col-sm-8">
	                  <input type="search" name="fecha_a_entregar" value="<?= date("d/m/Y") ?>" id="fecha_entrega" class="form-control datetime-input">
	              </div>
	            </div>
	        </div>
	        
	        <div class="col-xs-9">
	            <div class="form-group">
	              <label for="proveedor" class="col-sm-2 control-label">Descrip.: </label>
	              <div class="col-sm-10">
	                  <input type="text" value="" class="form-control" name="descripcion_trabajo" id="descripcion_trabajo" placeholder="Descripción trabajo">
	              </div>
	            </div>
	        </div>
            <div class="col-xs-3">
                <div class="form-group">
                  <label for="proveedor" class="col-sm-4 control-label">Empleado: </label>
                  <div class="col-sm-8">
                      <?php 
                        $this->db->select('empleados.id, user.nombre, user.apellido');
                        $this->db->join('user','user.id = empleados.user_id');
                        echo form_dropdown_from_query('empleados_id','empleados','id','nombre apellido',0,'id="field-empleados_id"');
                      ?>
                  </div>
                </div>
            </div>
	    </div>
        
    </div>     
    <div class="row">
        <div class="col-xs-12">Detalle de ventas <a href="javascript:advancesearch()" class="btn btn-default">Busqueda avanzada de productos</a> <a target="_new" href="<?= base_url('movimientos/productos/inventario') ?>" class="btn btn-default">Inventariado</a></div>
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2" id="detall">
                <thead>
                    <tr>
                    	<th style="width:10%">T.Precio</th>
                        <th style="width:15%">Código</th>
                        <th style="width:35%">Nombre artículo</th>                                         
                        <th style="width:15%">Cant.</th>
                        <th style="width:15%">Precio Venta</th>                        
                        <th style="width:15%">Total</th>    
                        <th>Acciones</th>
                    </tr>                    
                </thead>
                <tbody>                    
                    <tr>
                    	<td><?= form_dropdown_from_query('tipo_precios_id[]','tipo_precios','id','nombre_tipo_precios',0,'',FALSE,'tipos'); ?></td>
                        <td><input name="codigos[]" type="text" class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="productos_id[]" type="text" class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><input name="cantidad[]" type="text" class="form-control cantidad" placeholder="Cantidad"></td>
                        <td><input name='precio[]' type="text" class="form-control precio_venta" placeholder="Precio venta" readonly=""></td>
                        <td>
                        	<input name='total[]' type="text" class="form-control total" placeholder="Total" readonly="">
                        	<input type="hidden" class="categoria_precios_id" name="categoria_precios_id[]">
                        </td>
                        <td>
                        	<p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Total Venta Gs: </label>
              <div class="col-sm-7">
                  <input type="text" class="form-control" name="total_venta" id="total_venta" readonly="" value="0">
              </div>
            </div>
        </div>
    </div>        
    <div class="row">       
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Entrega inicial:</label>
              <div class="col-sm-7">
                  <input type="text" class="form-control" name="entrega_inicial" id="entrega_inicial" value="0">
              </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top:40px">      
    	<input type="hidden" name="sucursales_id" value="<?= $this->user->sucursal ?>">
    	<input type="hidden" name="cajadiaria_id" value="<?= $this->user->cajadiaria ?>">
    	<button type="submit" id="guardar" class="btn btn-success">Guardar Servicio</button>
    </div>
</form>
<?php $this->load->view('predesign/datepicker',array('scripts'=>'a')) ?>
<?= $this->load->view('predesign/chosen.php') ?>
<script>
	var rangos = [];
	$(document).on('change','.cantidad',function(){$(document).trigger('total')});
	$(document).on('change','#total_efectivo,#total_debito,#total_credito,#total_cheque',function(){
        total_venta = (parseInt($("#total_venta").val()) - parseInt($("#total_efectivo").val()) - parseInt($("#total_debito").val()) - parseInt($("#total_credito").val()) - parseInt($("#total_cheque").val()))*-1;
        $("#vuelto").val(total_venta);
    });

	$("body").on('change','.codigo',function(){
       if($(this).val()!=''){
        focused = $(this);
        elements = $(".codigo").length;
        focusedcode = $(this).val();
        focusedlote = $(this).parents('tr').find('.lote').val();
        i = 0;
        $(".codigo").each(function(obj){
            i++;            
            if($(this).val()==focusedcode && $(this).parents('tr').find('.lote').val() == focusedlote && !$(this).is(focused))
            {
               row = $(this).parent().parent().find('.cantidad');
               row.val(parseInt(row.val())+1);
               focused.parents('tr').find('input').val('');
            }
            
            else if(i==elements && $(this).val()!=''){
                var cod = $(this).val();
                var tipo = $(this).parents('tr').find('.tipos').val();
                $.post('<?= base_url('json/getProduct') ?>',{codigo:cod,tipoprecioid:tipo},function(data){
                    data = JSON.parse(data);
                    data = data['producto'];
                    if(data!=''){
	                    focused = focused.parent().parent();
	                    focused.find('.producto').val(data['nombre_comercial']);
	                    focused.find('.cantidad').val(1);
	                    focused.find('.precio_venta').val(data['precio_venta']);
	                    focused.find('.total').val(data['precio_venta']);	                    
	                    if(typeof(rangos.find(x => x.codigo === data['rangos'].codigo))==='undefined'){
	                      if(data['rangos'].length>0){
	                        rangos.push({codigo:data['rangos'][0].codigo,rangos:data['rangos']});
	                      }else{
	                        rangos.push({codigo:cod,rangos:[{cantidad_desde:'1','cantidad_hasta':'600',precio:data['precio_venta']}]});
	                      }
	                    }
	                    $(document).trigger('total');	                    
                	}
                });
            }
        });
        }
    });

    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    });

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    }); 

    $(document).on('total',function(){      
        $("#total_venta").val(0);        
        $("tbody tr").each(function(){
            self = $(this);               
            var codigo = self.find('.codigo').val();
            var cantidad = parseFloat(self.find('.cantidad').val());
            var precio = parseFloat(self.find('.precio_venta').val());
            var precio2 = 0;
            var cat = '';
            //Tiene rango?            
            var rango = rangos.find(x => x.codigo == codigo);     

            if(typeof(rango)!=='undefined'){
                for(var i in rango.rangos){
                  var min = parseFloat(rango.rangos[i].cantidad_desde);
                  var max = parseFloat(rango.rangos[i].cantidad_hasta);
                  if(cantidad >= min){                    
                    precio2 = rango.rangos[i].precio;
                    cat = rango.rangos[i].id;       
                    console.log(precio2);             
                    console.log(rango);
                  }
                }  

                if(precio2===0){
                  precio2 = rango.rangos[0].precio_original;
                  cat = '';
                }                              
            }else{
              precio2 = precio;
            }

            self.find('.precio_venta').val(!isNaN(precio2)?precio2:'');
            console.log(cat);
            self.find('.categoria_precios_id').val(!isNaN(cat)?cat:'');
            total = parseFloat(self.find('.cantidad').val())*parseInt(self.find('.precio_venta').val());            

            
            
            if(!isNaN(total)){
                self.find('.total').val(total);
                /*Total Venta*/
                total_venta = parseInt($("#total_venta").val());            
                total_venta += total;
                $("#total_venta").val(total_venta);
            }
        });
    });

    //Eventos
    $(document).on('keydown','input',function(event){        
            
        if (event.which == 13){
            if($(this).hasClass('total')){
                addrow($(this));
            }
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } 
            else if($(this).val()!='' && $(this).hasClass('codigo')){
                $(this).trigger('change');                
            }
            else if($(this).val()=='' && $(this).hasClass('codigo')){
                return false;
            }
            else
            {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });

    $("#cliente-doc").on('change',function(){
        var val = $(this).val();
        if(val!=''){
            $.post('<?= base_url() ?>maestras/clientes/json_list',{
                'search_field[]':'nro_documento',
                'search_text[]':val,
                'operator':'where'
            },function(data){
                data = JSON.parse(data);
                if(data.length>0){
                    alert('El cliente ingresado ya existe');
                }
            });
        }
    });

    function addrow(obj){
        var row = $(obj).parents('tr');        	
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');            
            date_init_calendar();            
    }
    function removerow(obj){
        var o = $(obj).parents('tbody');
        $(obj).parent('td').parent('tr').remove();
        $(document).trigger('total');
    }

    function advancesearch()
    {
        $.post('<?= base_url('json/searchProduct/') ?>',{},function(data){
            emergente(data);
        });
    }
    
    function selCod(cod,lote){
        $("tbody tr:last-child").find('.codigo').val(cod);
        $("tbody tr:last-child").find('.codigo').trigger('change');
        $("tbody tr:last-child .addrow").trigger('click');
        $('#myModal').modal('hide');
    }    

    function refreshFields(){
    	$("input[type='text'],input[type='number'],tbody input[type='hidden'],select").val('');    	
        $('.oculto').hide();
        $.post('<?= base_url() ?>maestras/clientes/json_list',{            
            'per_page':1000,
        },function(data){
            data = JSON.parse(data);
            var opts = '<option value="">Seleccione una opción</option>';
            for(var i in data){
                opts += '<option value="">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>';
            }
            $("#cliente").html(opts);
            $("#cliente").chosen().trigger('liszt:updated');
            $('#cliente_chzn').show();
        });
    }
</script>