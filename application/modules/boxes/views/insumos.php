<?= $output ?>

<script>
	$(document).on('change','#field-productos_id',function(){
		var cant = $("#field-cantidad").val();
		cant = cant!=''?cant:1;
		$("#field-cantidad").val(cant)
		$.post('<?= base_url() ?>movimientos/productos/productos/json_list',{
			'search_text[]':$(this).val(),
			'search_field[]':'productos.id',
			'operator':'where'
		},function(data){
			data = JSON.parse(data);
			if(data.length>0){
				$("#field-precio_costo").val(data[0].precio_costo);
				$("#field-total").val(parseFloat($("#field-cantidad").val())*parseFloat(data[0].precio_costo));
			}
		});
	});

	$(document).on('change','#field-precio_costo',function(){		
		$("#field-total").val(parseFloat($("#field-cantidad").val())*parseFloat($(this).val()));
	});

	$(document).on('change','#field-cantidad',function(){		
		$("#field-total").val(parseFloat($(this).val())*parseFloat($('#field-precio_costo').val()));
	});
</script>