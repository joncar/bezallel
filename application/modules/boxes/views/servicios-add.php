<?php
    if(!empty($edit)){
        $servicio = $this->querys->getServicioDetalle($edit);        
    }
?>
<style>
    .panel{
        border-radius:0px;
    }

    .error{
        border: 1px solid red !important;
    }

    .patternCredito{
        background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">
            <b>Datos generales</b>
        </h1>
    </div>
    <div class="panel-body">
        <div class="row" style="position: relative;">
            <!--<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
                #FACTURA: <span id="nroFactura"></span>
            </div>-->
            <div class="col-xs-12 col-md-2">
                <div class='form-group' id="imagen_field_box" style="height:130px;">
                    <div style="position:relative; display:inline-block;">
                        <div class="slim" id="slim-imagen"          
                             data-service="<?= base_url('boxes/admin/servicios/cropper/imagen/') ?>"
                             data-post="input, output, actions"
                             data-size="120,120"
                             data-instant-edit="true"
                             data-push="true"
                             data-did-upload="imageSlimUpload"
                             data-download="true"
                             data-will-save="addValueSlim"
                             data-label="Subir imagen"
                             data-meta-name="imagen"                         
                             data-force-size="120,120"  style="width:200px; height:120px;">                                      
                             <input type="file" id="slim-imagen"/>                        
                             <input type="hidden" data-val="<?= @$servicio->imagen ?>" name="imagen" value="<?= @$servicio->imagen ?>" id="field-imagen">
                             <img id="image-imagen" src="<?= base_url().'img/servicios/'.@$servicio->imagen ?>" style="width:120px; height:120px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7">
                <div class="col-xs-12 col-md-4">
                    Cliente <a href="#addCliente" data-toggle="modal" style="color:green"><i class="fa fa-plus"></i></a>: 
                    <?php 
                        $this->db->limit(1);
                        echo form_dropdown_from_query('clientes_id','clientes','id','nro_documento nombres apellidos',1,'id="clientes_id"') 
                    ?>
                    <a href="javascript:saldo()" style="position: absolute;top: 0;right: 15px;"><i class="fa fa-credit-card"></i></a>
                </div>
                <div class="col-xs-12 col-md-4">
                    Fecha entrega:                     
                    <input type="date" name="fecha_a_entregar" class="form-control" id="fecha_a_entregar" value="<?= date("Y-m-d") ?>" placeholder="Fecha">                    
                </div>
                
                <div class="col-xs-12 col-md-4">
                    Empleado: 
                    <?php 
                        $this->db->select('empleados.id, user.nombre, user.apellido');
                        $this->db->join('user','user.id = empleados.user_id');
                        echo form_dropdown_from_query('empleados_id','empleados','id','nombre apellido',0,'id="field-empleados_id"');
                    ?>                    
                </div>
                <div class="col-xs-12 col-md-4">
                    Descripción trabajo: 
                    <input type="text" name="descripcion_trabajo" id="descripcion_trabajo" class="form-control">
                </div>
                <div class="col-xs-12 col-md-4">
                    Hora entrega:                                         
                    <input type="time" name="hora_entrega" class="form-control" id="hora_entrega" value="<?= @$servicio->hora_entrega ?>" placeholder="Fecha">                        
                </div>
                <div class="col-xs-12 col-md-4">
                    Ubicación: 
                    <input type="text" name="ubicacion" id="ubicacion" class="form-control" value="<?= @$servicio->ubicacion ?>">
                </div>
            </div>
            <div class="col-xs-12 col-md-3" style="position: relative;">
                <span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Servicio: </span>
                <input type="text" name="total_venta" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px;">
            </div>

        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9" style="padding-right: 39px;">
        <div class="panel panel-default" style="border:0">
            <div style="height:208px; overflow-y: auto;">
                <table class="table table-bordered" id="ventaDescr">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>                            
                            <th>Cantidad</th>
                            <th>Precio</th>                                                        
                            <th>Total</th>
                            <th>Stock</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr id="productoVacio">
                            <td>
                                <a href="javascript:void(0)" class="rem" style="display:none;color:red">
                                    <i class="fa fa-times"></i>
                                </a> 
                                <span>&nbsp;</span>
                            </td>
                            <td>&nbsp;</td>
                            <td><input name="cantidad" class="cantidad" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
                            <td><input name="precio" class="precio" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>                            
                            <td><input name="precio_descuento" class="precio_descuento" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
                            <td>&nbsp;</td>
                        </tr>

                        
                    </tbody>
                </table>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-12 col-md-2" style="text-align: center;padding: 6px;">
                        Cant: <span id="cantidadProductos">4</span>                 
                    </div>
                    <div class="col-xs-12 col-md-10" style="position: relative;">
                        <i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
                        <input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
                        <button style="position: absolute;top: 1px;right: 16px;padding: 1px;" class="btn btn-primary">Insertar</button>

                        <div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3" style="padding-left: 5px;">
        <div class="panel panel-default">
            <div class="panel-heading">Resumen de servicio</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-4">Pesos: </div>
                    <div class="col-xs-8" style="margin-bottom:5px">                        
                        <input type="text" name="total_pesos" value="300.000" readonly="" style="width: 100%;">
                    </div>
                    <div class="col-xs-12 col-md-4">Reales: </div>
                    <div class="col-xs-8" style="margin-bottom:5px">                        
                        <input type="text" name="total_reales" value="300.000" readonly="" style="width: 100%;">
                    </div>
                    <div class="col-xs-12 col-md-4">Dolares: </div>
                    <div class="col-xs-8" style="margin-bottom:5px">                        
                        <input type="text" name="total_dolares" value="300.000" readonly="" style="width: 100%;">
                    </div>
                    <div class="col-xs-12 col-md-4">Entrega inicial: </div>
                    <div class="col-xs-8" style="margin-bottom:5px">                        
                        <input type="text" name="entrega_inicial" value="0" style="width: 100%;">
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="respuestaVenta"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-9">
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-default" onclick="nuevaVenta();" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nuevo servicio</button>
          </div>
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary" onclick="sendVenta()"><i class="fa fa-floppy-o"></i> Solicitar servicio</button>
          </div>
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-default" onclick="imprimir(<?= @$edit ?>)" <?= empty($edit)?'disabled="true"':'' ?> style="color:#000 !important"><i class="fa fa-print"></i> Imprimir</button>
          </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3" style="margin-top: -40px;">
        <div class="panel panel-default">
            <div class="panel-heading">Atajos</div>
            <div class="panel-body">
                <span>(ALT+C) <small>Enfocar busqueda por código</small></span><br/>
                <span>(ALT+I) <small>Mostrar busqueda avanzada</small></span><br/>
                <span>(ALT+P) <small>Procesar servicio</small></span><br/>
                <span>(ALT+N) <small>Nueva servicio</small></span><br/>
            </div>
        </div>
    </div>
</div>


<div id="addCliente" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="insertar('maestras/clientes/insert',this,'.resultClienteAdd',function(data){setCliente(data)}); return false;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Agregar cliente</h4>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" class="form-control" name="nombres" placeholder="Nombre del cliente">
                  </div>
                </div>
                <div class="col-xs-12 col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Celular</label>
                    <input type="text" class="form-control" name="celular1" placeholder="Celular del cliente">
                  </div>                
                </div>

                <div class="col-xs-12 col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Apellido</label>
                    <input type="text" class="form-control" name="apellidos" placeholder="Apellido del cliente">
                  </div>
                </div>
                <div class="col-xs-12 col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">#Documento</label>
                    <input type="text" class="form-control" name="nro_documento" placeholder="Documento del cliente">
                  </div>
                </div>

                <div class="col-xs-12 col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Dirección</label>
                        <input type="text" class="form-control" name="direccion" placeholder="Dirección del cliente">
                      </div>
                </div>

                <div class="col-xs-12 col-md-12">
                    <div class="resultClienteAdd"></div>
                </div>
            </div>            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->



<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<script>
<?php 
    if(!empty($edit)){
        $venta = $this->querys->getServicioDetalle($edit);
        if($venta){
            echo 'var editar = '.json_encode($venta).';';
        }else{
            echo 'var editar = undefined;';
        }
    }else{
        echo 'var editar = undefined;';
    }

    if(!empty($default)){       
        echo 'var precargo = '.json_encode($default).';';
    }else{
        echo 'var precargo = undefined;';
    }
?>
</script>
<script src="<?= base_url() ?>js/servicios.js?v=1.1"></script>
<script>    
    <?php
        $ajustes = $this->db->get('ajustes')->row();
    ?>
    var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
    var tasa_real = <?= $ajustes->tasa_reales ?>;
    var tasa_peso = <?= $ajustes->tasa_pesos ?>;
    var codigo_balanza = <?= $ajustes->cod_balanza ?>;
    var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
    ?>;
    var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
    var onsend = false;
    var venta = new Servicio(); 


    if(precargo!=undefined){
        venta.setDatos(precargo);
        venta.datos.precargo = JSON.stringify(precargo);
    }
    

    function initDatos(){
        venta.datos.sucursales_id = '<?= $this->user->sucursal ?>';        
        venta.datos.cajadiaria_id = '<?= $this->user->cajadiaria ?>';
        venta.datos.fecha = '<?= date("Y-m-d H:i") ?>';                 
    }
    initDatos();
    venta.initEvents();
    venta.updateFields();
    if(editar!=undefined){
        venta.setDatos(editar);        
    }
    function imprimir(codigo){  
        var idReporte = '<?= $this->ajustes->id_reporte_servicios ?>';
        window.open('<?= base_url() ?>reportes/rep/verReportes/'+idReporte+'/html/servicios_id/'+codigo);          
    }

    function selCod(codigo){        
        venta.addProduct(codigo);       
        $("#inventarioModal").modal('toggle');
    }

    function nuevaVenta(){      
        venta.initVenta();
        initDatos();
        $(".respuestaVenta").html('').removeClass('alert alert-info alert-danger alert-success');       
        if($("#procesar").css('display')=='block'){
            $("#procesar").modal('toggle');
        }
        $(".btnNueva").hide();
        $("button").attr('disabled',false);
        $("#transaccion").val(1);
        $("#transaccion").chosen().trigger('liszt:updated');
        $.post(URI+'maestras/clientes/json_list',{   
            search_field:'id',     
            search_text:'1',
            operator:'where'
        },function(data){       
            data = JSON.parse(data);   
            venta.selectClient(data);
        });
        onsend = false;
    }

    
    function sendVenta(){
        if(!onsend){
            onsend = true;
            venta.updateFields();
            var datos =  JSON.parse(JSON.stringify(venta.datos));
            datos.productos = JSON.stringify(datos.productos);
            var accion = typeof(editar)=='undefined'?'insert':'update/'+editar.id;
            $("button").attr('disabled',true);
            insertar('boxes/admin/servicios/'+accion,datos,'.respuestaVenta',function(data){                        
                var id = data.insert_primary_key;                       
                var enlace = '';
                $(".respuestaVenta").removeClass('alert-info');
                imprimir(id);
                enlace = 'javascript:imprimir('+id+')';
                $('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');                    
                $("button").attr('disabled',false);                   
                onsend = false;
            },function(){
                onsend = false;
                $("button").attr('disabled',false);
            });
        }
    }

    function setCliente(data){
        if(data.success){
            $(".resultClienteAdd").html('<div class="alert alert-success">Cliente añadido con éxito</div>');
            $.post(URI+'maestras/clientes/json_list',{   
                search_field:'id',     
                search_text:data.insert_primary_key,
                operator:'where'
            },function(data){       
                data = JSON.parse(data);   
                venta.selectClient(data);
            });
        }
    }
</script>