<?php $servicio = $this->querys->getServicio($output->servicio,FALSE); ?>
<?php if(!is_string($servicio)): ?>
<?php if($servicio->facturado==0): ?>
	<div style="text-align: right; margin:20px">
		<a href="<?= base_url('movimientos/ventas/ventas/add/Servicio/'.$output->servicio) ?>" class="btn btn-success"><i class="fa fa-print"></i> Facturar</a>
	</div>
<?php else: ?>
	<div class="alert alert-info">
		El servicio se encuentra facturado, ya no se pueden hacer cambios a estos datos
	</div>
<?php endif ?>

<?= $output->output ?>
<script>
	var categorias = [];
	var precio_original = 0;
	$("#field-cantidad").on('change',function(){		
		var cantidad = $("#field-cantidad").val();
		var precios = categorias.find(x=>x.productos_id==$("#field-productos_id").val());		
		var precio = 0;
		var id = 0;
		var tipo_precio = $("#field-tipo_precios_id").val();
		if(tipo_precio==='1' && typeof(categorias)!=='undefined'){
			for(var i in categorias){
              var min = parseFloat(categorias[i].cantidad_desde);
              var max = parseFloat(categorias[i].cantidad_hasta);              
              if(cantidad >= min){  
              	console.log(categorias[i]);                  
                precio = categorias[i].precio;    
                id = categorias[i].id;
              }             
            }  

            $("#field-precio").val(precio);
            $("#field-categoria_precios_id").val(id);
		}else{
			$("#field-precio").val(precio_original);
		}
		total();
	});

	$("#field-precio").on('change',function(){
		total();
	});

	$("#field-tipo_precios_id").on('change',function(){
		if($("#field-productos_id").val()!==''){
			$("#field-productos_id").trigger('change');
		}
	});


	$("#field-productos_id").on('change',function(){
		$.post('<?= base_url('boxes/admin/categoria_precios/json_list') ?>',{
			'tipo_precios_id':$("#field-tipo_precios_id").val(),			
			'productos_id':$(this).val(),			
		},function(data){
			data = JSON.parse(data);
			categorias = data;

			$.post('<?= base_url('movimientos/productos/productos/json_list') ?>',{
				'search_text[]':$("#field-productos_id").val(),
				'search_field[]':'id'
			},function(data){
				data = JSON.parse(data);
				if(data.length>0){
					$("#field-cantidad").val(1);
					$("#field-precio").val(data[0].precio_venta);
					precio_original = data[0].precio_venta;
					$("#field-cantidad").trigger('change');
					total();
				}
			});
		});
		
	});

	$('#crudForm').on('submit',function(){
		$.post('<?= base_url('boxes/admin/servicios/json_list') ?>',{
			'search_text[]':<?= $output->servicio ?>,
			'search_field[]':'id'
		},function(data){
			data = JSON.parse(data);
			if(data.length>0){
				$("#total").val(data[0].total);
			}
		});
		
	});

	function total(){
		var cantidad = parseFloat($("#field-cantidad").val());
		var precio = parseFloat($("#field-precio").val());
		$("#field-total").val(cantidad*precio);
	}
</script>
<?php endif ?>