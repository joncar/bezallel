<?php

require_once APPPATH.'/controllers/Panel.php';    

class Admin extends Panel {

    function __construct() {
        parent::__construct();
    }

    public function empleados($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);            
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function vehiculos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);    
        $crud->set_relation_dependency('modelos_id','marcas_id','marcas_id');   
        $crud->set_field_upload('foto_vehiculo','img/vehiculos');
        $crud->set_relation('clientes_id','clientes','{nro_documento} {nombres} {apellidos}');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }    

    public function servicios($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);           
        $crud->field_type('imagen','image',array('path'=>'img/servicios','width'=>'800px','height'=>'600px'))
             ->field_type('facturado','true_false',array('0'=>'NO','1'=>'SI'))
             ->set_relation('clientes_id','clientes','{nro_documento}|{nombres}|{apellidos}');
        $crud->columns('servicios.id','fecha','j3eb7f57f.nro_documento','j3eb7f57f.nombres','j3eb7f57f.apellidos','estado_servicio_id','facturado','total');    
        $crud->callback_column('total',function($val,$row){
            $id = strip_tags($row->id);
            return $this->db->query('SELECT FORMAT(COALESCE(SUM(total)),0,"de_DE") as total from servicios_detalles where servicios_id = '.$id)->row()->total.' Gs.';
        })
        ->callback_column('j3eb7f57f.nro_documento',function($val,$row){
            return explode('|',$row->s3eb7f57f)[0];
        })
        ->callback_column('j3eb7f57f.nombres',function($val,$row){
            return explode('|',$row->s3eb7f57f)[1];
        })
        ->callback_column('j3eb7f57f.apellidos',function($val,$row){
            return explode('|',$row->s3eb7f57f)[2];
        })
        ->callback_column('servicios.id',function($val,$row){
            return '<a href="javascript:showDetail('.$row->id.')">'.$row->id.'</a>';
        });
        $crud->callback_after_insert(array($this,'addBody'));

        $crud->display_as('j3eb7f57f.nro_documento','#Documentos')
        ->display_as('j3eb7f57f.nombres','Nombres')
        ->display_as('j3eb7f57f.apellidos','Apellidos')
        ->display_as('vehiculos_id','Vehiculo')
        ->display_as('estados_servicio_id','Estado')
        ->display_as('servicios.id','#Servicio')
        ->order_by('id','DESC');
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');                
        $crud->add_action('Detalles','',base_url('boxes/admin/servicios_detalles/').'/');
        $crud->add_action('Facturar','',base_url('movimientos/ventas/ventas/add/Servicio/').'/');
        $crud->add_action('Imprimir ticket','',base_url('reportes/rep/verReportes/'.$this->ajustes->id_reporte_servicios.'/html/servicios_id').'/');
        $crud->unset_delete();        
        $crud->set_no_using_ajax('clientes_id');
        $output = $crud->render();
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = '';
            $default = array();
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('servicios-add',array('default'=>$default,'output'=>$output->output,'edit'=>$edit),TRUE);
        }else{
          $output->output = $this->load->view('servicios-list',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;
        $pagoGs = 0;

        if($_POST['total_venta']==0){
            $this->form_validation->set_message('validar','El importe total de la venta no puede ser igual a 0');        
            return false;
        }   

        if(empty($_POST['cliente'])){
            $this->form_validation->set_message('validar','Debe indicar a que cliente desea asignarle este servicio');
            return false;
        }
        //Validar stocks
        $productos = json_decode($_POST['productos']);        
        $return = true;
        if($this->db->get('ajustes')->row()->vender_sin_stock==0){
            $msj = '';
            foreach($productos as $p){                
                $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
                if($pro->num_rows()==0){
                    $return = false;
                    $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
                }else{
                    $pro = $pro->row();
                    if($pro->inventariable==1){
                        $producto = $this->db->get_where('productosucursal',array('producto'=>$p->codigo));
                        if($producto->num_rows()==0 || $producto->row()->stock<$p->cantidad){
                            $return = false;
                            $msj.= '<p>El producto '.$p->codigo.' no tiene stock suficiente</p>';
                        }
                    }
                }
            }
            if(!$return){
                $this->form_validation->set_message('validar',$msj);
            }
        }
        return $return;
    }

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        foreach($productos as $p){
            $pro[] = $p->nombre;
            $producto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row();
            $p->categoria_precios_id = empty($p->categoria_precios_id)?'':$p->categoria_precios_id;
            $this->db->insert('servicios_detalles',array(
                'servicios_id'=>$primary,
                'productos_id'=>$producto->id,
                'categoriaproducto_id'=>$producto->categoria_id,
                'categoria_precios_id'=>$p->categoria_precios_id,
                'cantidad'=>$p->cantidad,
                'precio'=>$p->precio_venta,
                'total'=>$p->total,
                'estado_servicio_id'=>1,
                'user_id'=>$this->user->id,
                'sucursal_id'=>$this->user->sucursal,
                'fecha_update'=>date("Y-m-d H:i:s")
            ));
        }
        $this->db->update('servicios',array('productos'=>implode($pro)),array('id'=>$primary));        
    }



    

    function categoria_precios(){
        $crud = $this->crud_function('','');
        $crud->columns('id','tipo_precios_id','producto','cantidad_desde','cantidad_hasta','precio');
        $crud->callback_column('producto',function($val,$row){return $row->productos_id;});
        $crud->order_by('id','ASC');
        if(!empty($_POST['productos_id']) && !empty($_POST['tipo_precios_id'])){
            $crud->where('productos_id',$_POST['productos_id']);
            $crud->where('tipo_precios_id',$_POST['tipo_precios_id']);
        }
        $crud = $crud->render();
        $this->loadView($crud);
    }

    function servicios_detalles($x = '', $y = ''){
        if(is_numeric($x)){
            $servicio = $this->db->get_where('servicios',array('id'=>$x));            
            if($servicio->num_rows()>0){
                $servicio = $servicio->row();                
                $crud = parent::crud_function($x, $y);  
                if($servicio->facturado==1){
                    $crud->unset_delete()
                         ->unset_edit()
                         ->unset_add();                
                }          
                $crud->where('servicios_id',$x)                     
                     ->field_type('servicios_id','hidden',$x)
                     ->field_type('categoria_precios_id','hidden')
                     ->field_type('user_id','hidden',$this->user->id)
                     ->field_type('sucursal_id','hidden',$this->user->sucursal)
                     ->field_type('fecha_update','hidden',date("Y-m-d H:i:s"))
                     ->field_type('tipo_precios_id','hidden',1)
                     ->columns('s10dcc541','s1b7eb4b5','s1a0bea0f','productos_id','cantidad','precio','total','s53aabcc4','estado_servicio_id')
                     ->display_as('s53aabcc4','Empleado')
                     ->display_as('categoriaproducto_id','Categoria')
                     ->display_as('productos_id','Producto')
                     ->display_as('categoria_precios_id','Tipo Precio')
                     ->display_as('s10dcc541','Categoria')
                     ->set_relation('empleados_id','empleados','user_id')
                     ->set_relation('j910725b6.user_id','user','{nombre}');

                 $crud->set_relation('productos_id','productos','{codigo} {nombre_comercial}')                                          
                      ->set_relation('j4b04c546.categoria_id','categoriaproducto','denominacion');

                $crud->callback_field('empleados_id',function($val){
                    $this->db->select('empleados.id, user.nombre, user.apellido');
                    $this->db->join('user','user.id = empleados.user_id');
                    return form_dropdown_from_query('empleados_id','empleados','id','nombre apellido',$val,'id="field-empleados_id"');
                });

                /*$crud->callback_field('tipo_precios_id',function($val){
                    $val = empty($val)?2:$val;               
                    return form_dropdown_from_query('tipo_precios_id','tipo_precios','id','nombre_tipo_precios',$val,'id="field-tipo_precios_id"');
                });*/

                $crud->set_relation_dependency('productos_id','categoriaproducto_id','categoria_id');
                
                if(!empty($_GET['json'])){
                  $crud->staticShowList = true;
                  $output = $crud->render();
                  echo $output->output;
                  die();
                }

                $output = $crud->render();                
                $header = new ajax_grocery_crud();
                $header->set_table('servicios')
                       ->set_theme('header_data')
                       ->set_subject('Servicio')
                       ->where('id',$x);
                $header->columns('fecha','clientes_id','estado_servicio_id','facturado','total');
                $header->callback_column('total',function($val,$row){
                    return $this->db->query('SELECT FORMAT(COALESCE(SUM(total)),0,"de_DE") as total from servicios_detalles where servicios_id = '.$row->id)->row()->total.' Gs.';
                });
                $header->set_url('boxes/admin/servicios/');
                $output->header = $header->render(1)->output;
                $output->servicio = $x;                
                $output->output = '<div class="alert alert-warning">Si el servicio ya ha sido facturado no se permite realizar cambios.</div>'.$this->load->view('servicios',array('output'=>$output),TRUE);
                $this->loadView($output);
            }
        }
    }

    function serviciosDetailModal($x = '', $y = ''){
        $this->as['serviciosDetailModal'] = 'servicios_detalles';
        if(is_numeric($x)){
            $servicio = $this->db->get_where('servicios',array('id'=>$x));            
            if($servicio->num_rows()>0){
                $servicio = $servicio->row();                
                $crud = parent::crud_function($x, $y);                  
                $crud->unset_delete()
                     ->unset_edit()
                     ->unset_add()
                     ->unset_read()
                     ->unset_export()
                     ->unset_print();
                $crud->where('servicios_id',$x)                                          
                     ->columns('s10dcc541','s1b7eb4b5','s1a0bea0f','productos_id','cantidad','precio','total','s53aabcc4','estado_servicio_id')
                     ->display_as('s53aabcc4','Empleado')
                     ->display_as('categoriaproducto_id','Categoria')
                     ->display_as('sub_categoria_producto_id','SubCategoria')
                     ->display_as('sub_sub_categoria_id','Clase')
                     ->display_as('productos_id','Producto')
                     ->display_as('categoria_precios_id','Tipo Precio')
                     ->display_as('s10dcc541','Categoria')
                     ->display_as('s1b7eb4b5','SubCategoria')
                     ->display_as('s1a0bea0f','Clase')
                     ->set_relation('empleados_id','empleados','user_id')
                     ->set_relation('j910725b6.user_id','user','{nombre}');
                 $crud->staticShowList = true;
                 $crud->set_relation('productos_id','productos','nombre_comercial')
                      ->set_relation('j4b04c546.categoria_id','categoriaproducto','denominacion')
                      ->set_relation('j4b04c546.sub_categoria_producto_id','sub_categoria_producto','nombre_sub_categoria_producto')
                      ->set_relation('j4b04c546.sub_sub_categoria_id','sub_sub_categoria','nombre_sub_sub_categoria');
                $output = $crud->render();
                echo $output->output;
            }
        }
    }


    public function presupuestos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);   
        $crud->set_lang_string('insert_success_message','Su factura se almaceno con éxito <script>document.location.href="'.base_url().'boxes/admin/presupuestos_detalles/{id}/add";</script>');;     
        $crud->field_type('facturado','hidden','0')
             ->field_type('anulado','hidden','0')
             ->field_type('nro_factura','hidden','')
             ->field_type('sucursales_id','hidden',$this->user->sucursal)
             ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
        if($crud->getParameters()=='list'){
            $crud->field_type('facturado','true_false',array('0'=>'NO','1'=>'SI'));
        }
        if($crud->getParameters()=='list'){
            $crud->set_relation('clientes_id','clientes','{nro_documento}|{nombres}|{apellidos}');
        }else{
            $crud->set_relation('clientes_id','clientes','{nro_documento} {nombres} {apellidos}');
        }
        $crud->set_relation_dependency('vehiculos_id','clientes_id','clientes_id');
        $crud->columns('id','fecha','j3eb7f57f.nro_documento','j3eb7f57f.nombres','j3eb7f57f.apellidos','estado_servicio_id','facturado','total');    
        $crud->callback_column('total',function($val,$row){
            return $this->db->query('SELECT FORMAT(COALESCE(SUM(total)),0,"de_DE") as total from presupuestos_detalles where presupuestos_id = '.$row->id)->row()->total.' Gs.';
        })
        ->callback_column('j3eb7f57f.nro_documento',function($val,$row){
            return @explode('|',$row->s3eb7f57f)[0];
        })
        ->callback_column('j3eb7f57f.nombres',function($val,$row){
            return @explode('|',$row->s3eb7f57f)[1];
        })
        ->callback_column('j3eb7f57f.apellidos',function($val,$row){
            return @explode('|',$row->s3eb7f57f)[2];
        })
        ->callback_field('empleados_id',function($val){
            $this->db->select('empleados.id, user.nombre, user.apellido');
            $this->db->join('user','user.id = empleados.user_id');
            return form_dropdown_from_query('empleados_id','empleados','id','nombre apellido',$val,'id="field-empleados_id"');
        })
        ->display_as('j3eb7f57f.nro_documento','#Documentos')
        ->display_as('j3eb7f57f.nombres','Nombres')
        ->display_as('j3eb7f57f.apellidos','Apellidos')
        ->display_as('vehiculos_id','Vehiculo')
        ->display_as('estados_servicio_id','Estado')
        ->display_as('id','#Servicio')
        ->order_by('id','DESC');
        $crud->add_action('Detalles','',base_url('boxes/admin/presupuestos_detalles/').'/');        
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    function presupuestos_detalles($x = '', $y = ''){
        if(is_numeric($x)){
            $servicio = $this->db->get_where('presupuestos',array('id'=>$x));            
            if($servicio->num_rows()>0){
                $servicio = $servicio->row();                
                $crud = parent::crud_function($x, $y);  
                if($servicio->facturado==1){
                    $crud->unset_delete()
                         ->unset_edit()
                         ->unset_add();                
                }          
                $crud->where('presupuestos_id',$x)                     
                     ->field_type('presupuestos_id','hidden',$x)
                     ->field_type('categoria_precios_id','hidden')
                     ->columns('categoriaproducto_id','sub_categoria_producto_id','sub_sub_categoria_id','productos_id','cantidad','precio','total','s53aabcc4','estado_servicio_id')
                     ->display_as('s53aabcc4','Empleado')
                     ->display_as('categoriaproducto_id','Categoria')
                     ->display_as('sub_categoria_producto_id','SubCategoria')
                     ->display_as('sub_sub_categoria_id','Clase')
                     ->display_as('productos_id','Producto')
                     ->display_as('categoria_precios_id','Tipo Precio');
                $crud->callback_field('tipo_precios_id',function($val){
                    $val = empty($val)?2:$val;               
                    return form_dropdown_from_query('tipo_precios_id','tipo_precios','id','nombre_tipo_precios',$val,'id="field-tipo_precios_id"');
                });
                $crud->set_relation('productos_id','productos','{codigo} {nombre_comercial}',array('anulado <'=>1))                                          
                     ->set_relation_dependency('sub_categoria_producto_id','categoriaproducto_id','categoriaproducto_id')
                     ->set_relation_dependency('sub_sub_categoria_id','sub_categoria_producto_id','sub_categoria_producto_id')
                     ->set_relation_dependency('productos_id','sub_categoria_producto_id','sub_categoria_producto_id')
                     ->set_no_using_ajax('sub_sub_categoria_id','productos_id');
                $output = $crud->render();


                ////Header
                $header = new ajax_grocery_crud();
                $header->set_table('presupuestos')
                       ->set_theme('header_data')
                       ->set_subject('Presupuesto')
                       ->where('id',$x);
                $header->columns('fecha','clientes_id','facturado','total');
                $header->callback_column('total',function($val,$row){
                    return $this->db->query('SELECT FORMAT(COALESCE(SUM(total)),0,"de_DE") as total from presupuestos_detalles where presupuestos_id = '.$row->id)->row()->total.' Gs.';
                });
                $header->set_url('boxes/admin/presupuestos/');
                $output->header = $header->render(1)->output;
                $output->servicio = $x;                
                $output->output = $this->load->view('presupuestos',array('output'=>$output),TRUE);
                $this->loadView($output);
            }
        }
    }

    function control($x = ''){
        if(empty($x) || $x=='list' || $x=='ajax_list' || $x=='ajax_list_info' || $x=='success'){
            $this->as['control'] = 'view_servicios';
        }else{
            $this->as['control'] = 'servicios_detalles';
        }
        $crud = $this->crud_function('','');
        $crud->set_primary_key('id','view_servicios');
        $crud->unset_delete()->unset_add()->unset_print()->unset_export()->unset_read();
        $crud->unset_columns('color');
        $crud->callback_column('estado',function($val,$row){
            return '<span class="label" style="background:'.$row->color.'">'.$val.'</span>';
        })->callback_column('detalle_trabajo',function($val,$row){
            return '<a href="javascript:detalleTrabajo(\''.$row->id.'\')">'.$val.'</a>';
        });
        $crud->callback_after_update(function($post,$primary){
            if($post['estado_servicio_id']==7){
                $ticket = base_url('reportes/rep/verReportes/'.get_instance()->ajustes->id_ticket_servicio_listo.'/html/servicios_id/'.$primary);
                $back = '<a href="'.base_url('boxes/admin/control/success/'.$primary).'">Volver al listado</a>';
                $back.= ' | <a href="'.$ticket.'">Imprimir ticket</a>';
                $data = array(
                  "success"=>true,
                  "insert_primary_key"=>true,
                  "success_message"=>"Tus datos han sido actualizado correcatamente. ".$back,
                  "insert_data"=>json_encode($post)
                );
                echo '<textarea>'.json_encode($data).'</textarea>';
                die();            
            }
        });
        if(!empty($x) && $x!='list' && $x!='ajax_list' && $x!='success'){
            $crud->field_type('user_id','hidden',$this->user->id)
                 ->field_type('sucursal_id','hidden',$this->user->sucursal)
                 ->field_type('fecha_update','hidden',date("Y-m-d H:i:s"));
           $crud->fields('estado_servicio_id','user_id','sucursal_id','fecha_update');
        }

        if($this->user->admin==0){
            $crud->where('user_id',$this->user->id)
                 ->unset_columns('se8701ad4');
        }        
        
        $crud = $crud->render();
        $crud->output = $this->load->view('control',array('output'=>$crud->output),true);
        $this->loadView($crud);
    }

    function insumos_servicios($x = ''){     
        $x = $this->db->get_where('servicios_detalles',array('id'=>$x))->row()->servicios_id;   
        $crud = $this->crud_function('','');        
        $crud->where('servicios_id',$x)
             ->field_type('servicios_id','hidden',$x)
             ->field_type('sucursales_id','hidden',$this->user->sucursal)
             ->unset_columns('servicios_id')
             ->set_relation('productos_id','productos','{codigo} {nombre_generico}');
        $crud = $crud->render();
        $crud->output = $this->load->view('insumos',array('output'=>$crud->output),TRUE);
        $header = new ajax_grocery_crud();
        $header->set_table('servicios')->set_subject('Servicio')->set_theme('header_data')->where('servicios.id',$x);
        $crud->header = $header->render(1)->output;
        $this->loadView($crud);
    }

    function servicios_log($x = ''){        
        $crud = $this->crud_function('','');
        $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
        $crud->where('servicios_detalles_id',$x)
             ->unset_columns('s67499b80')
             ->display_as('estado_servicio_id_nuevo','Modificado por')
             ->callback_column('sa761cf6f',function($val,$row){
                $color = $this->db->get_where('estado_servicio',array('id'=>$row->estado_servicio_id));
                $color = $color->num_rows()>0?$color->row()->color:'#000';
                return '<span class="label label-info" style="background:'.$color.'">'.$val.'</span>';
             })
             ->callback_column('estado_servicio_id_nuevo',function($val,$row){
                $color = $this->db->get_where('estado_servicio',array('id'=>$val));
                $estado = $color->num_rows()>0?$color->row()->nombre_estado:'N/A';
                $color = $color->num_rows()>0?$color->row()->color:'#000';
                return '<span class="label label-info" style="background:'.$color.'">'.$estado.'</span>';
             });
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header->where('servicios_detalles.id',$x)
                     ->set_table('servicios_detalles')
                     ->set_subject('Servicio')
                     ->set_theme('header_data');
        $crud->header = $crud->header->render(1)->output;
        $this->loadView($crud);
    }

    public function corte_costos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);                    
        $output = $crud->render();
        $this->loadView($output);
    } 

}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
