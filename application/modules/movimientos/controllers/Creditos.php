Creditos.phpire_once APPPATH.'/controllers/Panel.php';
	class Creditos extends Panel {
		function __construct(){
			parent::__construct();
		}

		function Creditos($venta = '',$return = false){
			if(empty($this->user->cajadiaria)){
				redirect('panel/selcajadiaria');
			}
			$vent = $this->db->get_where('ventas',array('id'=>$venta));
			if($vent->num_rows()>0){
				if($vent->row()->transaccion==1){
					throw new Exception("La venta seleccionada no es una venta a crédito", 503);
				}
				$crud = $this->crud_function('','');
				$crud->field_type('ventas_id','hidden',$venta)
					 ->unset_columns('ventas_id')
					 ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
					 ->unset_columns('cajadiaria')
					 ->field_type('forma_pago_id','dropdown',array('7'=>'Semanal','30'=>'Mensual'))
					 ->where('ventas_id',$venta);
				$crud->callback_after_insert(function($post,$primary){

					$cuotas = $post['cant_cuota'];
					$dias = $post['forma_pago_id'];
					for($i = 1; $i<= $cuotas; $i++){
						$fecha = $dias*($i-1);
						if($post['interes']==0){
							$capital = $post['monto_cuota'];
							$interes = 0;
						}else{
							$capital = $post['monto_credito']/$cuotas;
							$interes = $post['total_interes']/$cuotas;
						}
						$data = array(
							'creditos_id'=>$primary,
							'nro_cuota'=>$i,
							'fecha_a_pagar'=>date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_inicio_plan']).' +'.$fecha.' days')),
							'capital'=> $capital,
							'interes'=>$interes,
							'monto_a_pagar'=>$post['monto_cuota'],
							'pagado'=>0
						);					
						$this->db->insert('plan_credito',$data);						
					}
				});
				$crud->add_action('Plan de creditos','',base_url('movimientos/creditos/plan_credito').'/');
				$crud->set_lang_string('insert_success_message','Datos almacenados con éxito <a href="javascript:printPlan({id})">Imprimir plan</a> | <a href="javascript:printPagare({id})">Imprimir pagare</a> | <a href="'.base_url().'movimientos/creditos/plan_credito/{id}">Ir a plan de crédito</a> | ');
				$crud->add_action('Imprimir plan','',base_url('reportes/rep/verReportes/91/html/creditos_id').'/');
				$accion = $crud->getParameters();
				if($accion=='add'){
					$crud->field_type('anulado','hidden',0);
				}
				$crud = $crud->render();
				if($accion=='add'){
					$crud->output = $this->load->view('creditos',array('output'=>$crud->output,'venta'=>$vent->row()),TRUE);
				}

				if(is_numeric($venta)){
					$crud->header = new ajax_grocery_crud();
					$crud->header->set_table('ventas')->set_subject('Venta')->set_theme('header_data')->where('ventas.id',$venta)->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read()->set_url('movimientos/ventas/ventas/edit/'.$venta);
					$crud->header->columns('cliente','fecha','total_venta','productos')->set_relation('cliente','clientes','{nombres} {apellidos}');
					$crud->header = $crud->header->render(1)->output;
				}

				$this->loadView($crud);
			}else{
				$crud = $this->crud_function('','');
				$crud->add_action('Plan de creditos','',base_url('movimientos/creditos/plan_credito').'/');
				$encoded = $crud->encodeFieldName('ventas_id','s');
				get_instance()->ajustes = $this->db->get('ajustes')->row();
				$crud->unset_add()
					 ->unset_edit()
					 ->unset_delete()
					 ->unset_print()
					 ->unset_export()
					 ->where('(j5122c2ec.status = 0 OR j5122c2ec.status is NULL)','ESCAPE')
					 ->set_relation('ventas_id','ventas','nro_factura')
					 ->set_relation('j5122c2ec.cliente','clientes','{nro_documento} {nombres} {apellidos}')
					 ->callback_column('s5122c2ec',function($val,$row){
				 		return '<a href="javascript:showDetail('.$row->ventas_id.')">'.$val.'</a>';
					 })
					 ->unset_read();
				$crud->display_as('ventas_id','Nro Factura')
					 ->display_as('s51a61bd2','Cliente');
				$crud->columns('ventas_id','s51a61bd2','fecha_credito','monto_venta','entrega_inicial','monto_credito','interes','total_credito','monto_cuota','total_pagado','total_mora','saldo');
				$crud->callback_column('total_pagado',function($val,$row){
					return (string)$this->db->query("select IF(SUM(total_pagado) IS NULL,0,SUM(total_pagado)) as total from pagocliente where (pagocliente.anulado = 0 or pagocliente.anulado is null) AND creditos_id = '".$row->id."'")->row()->total;
				})->callback_column('total_mora',function($val,$row){
					$query = $this->db->get_where("plan_credito",array('creditos_id'=>$row->id));
					$total = 0;
					$ajustes = get_instance()->ajustes;
					foreach($query->result() as $q){
						$dias = strtotime(date("Y-m-d"))-strtotime(str_replace('/','-',$q->fecha_a_pagar));
				 		$dias = $dias/60/60/24;
				 		if($q->pagado==0 && $dias>$ajustes->cant_dias_mora){
					 		$dias = $dias-$ajustes->cant_dias_mora;
					 		$total += $dias * ($q->monto_a_pagar * $ajustes->porc_interes_mora/100);
					 	}
					}
					//Se suman los ya pagados
					$total+= $this->db->query("SELECT IF(SUM(total_mora) IS NULL,0,SUM(total_mora)) as total FROM plan_credito INNER JOIN pagocliente ON pagocliente.plan_credito_id = plan_credito.id WHERE plan_credito.pagado = 1 AND plan_credito.creditos_id = ".$row->id." AND (pagocliente.anulado = 0 or pagocliente.anulado is null)")->row()->total;
					return (string)$total;
				})
				->callback_column('saldo',function($val,$row){
					return (string)(($row->total_credito+$row->total_mora) - $row->total_pagado);
				});
				$crud->set_lang_string('insert_success_message','Datos almacenados con éxito <a href="javascript:printPlan({id})">Imprimir plan</a> | <a href="javascript:printPagare({id})">Imprimir pagare</a> | <a href="'.base_url().'movimientos/creditos/plan_credito/{id}">Ir a plan de crédito</a> | ');
				$crud->add_action('Imprimir plan','',base_url('reportes/rep/verReportes/91/html/creditos_id').'/');
				if($return){
					return $crud;
				}
				$crud = $crud->render();
				$crud->output = $this->load->view('ventas_list',array('output'=>$crud->output),TRUE);
				$this->loadView($crud);
			}
		}

		function plan_credito($venta = '',$return = false){
			$crud = $this->crud_function('','');
			$this->creditos_id = $venta;
			$crud->where('creditos_id',$venta)
				 ->field_type('creditos_id','hidden',$venta)
				 ->unset_add()
				 ->unset_delete()
				 ->fields('pagado')
				 ->display_as('fecha_retraso','Dias de retraso')
				 ->columns('nro_cuota','capital','interes','monto_a_pagar','pagado','fecha_a_pagar','fecha_retraso','mora','total_a_pagar','total_pagado','saldo','pagado')
				 ->callback_column('fecha_retraso',function($val,$row){
				 	$dias = strtotime(date("Y-m-d"))-strtotime(str_replace('/','-',$row->fecha_a_pagar));
				 	$dias = $dias/60/60/24;
				 	return $dias;
				 })
				 ->callback_column('mora',function($val,$row){
				 	$ajustes = $this->db->get('ajustes')->row();
				 	$mora = 0;
				 	if($row->pagado==0 && $row->fecha_retraso>$ajustes->cant_dias_mora){
				 		$dias = $row->fecha_retraso-$ajustes->cant_dias_mora;
				 		$mora = $dias * ($row->monto_a_pagar * $ajustes->porc_interes_mora/100);
				 	}
				 	elseif($row->pagado==1){
				 		$mora = $this->db->query("select SUM(total_mora) as mora from pagocliente where (pagocliente.anulado = 0 or pagocliente.anulado is null) AND creditos_id = '".$row->creditos_id."'")->row()->mora;
				 	}
				 	return (string)$mora;
				 })
				 ->callback_column('total_a_pagar',function($val,$row){				 	
				 	return $row->monto_a_pagar+$row->mora;
				 })
				 ->callback_column('total_pagado',function($val,$row){				 	
				 	$saldo = $this->db->query("SELECT 
							id,
							creditos_id,
							monto_a_pagar,
							totalpago.total_pagado as totalpagado
							FROM plan_credito
							INNER JOIN (
								SELECT pagocliente.plan_credito_id as plan_credito_id,
							    sum(pagocliente.total_pagado) as total_pagado
							    FROM pagocliente
							    WHERE (pagocliente.anulado = 0 or pagocliente.anulado is null)
							) as totalpago on totalpago.plan_credito_id = plan_credito.id
							WHERE creditos_id = ".$this->creditos_id);
					 	$totalpagado = 0;
					 	if($saldo->num_rows()>0){
					 		$totalpagado = $saldo->row()->totalpagado;				 		
					 	}
					 	return (string)$totalpagado;
				 })
				 ->callback_column('saldo',function($val,$row){					 		
					 	return (string)(($row->total_a_pagar+$row->mora)-$row->total_pagado);
				 })
				 ->add_action('Pagar credito','',base_url('movimientos/creditos/pagar_credito').'/');			
			if($return && $return==1){
				return $crud;
			}
			$crud = $crud->render();			
			$this->as['plan_credito'] = 'creditos';
			$crud->header = $this->creditos('',true);
			$crud->header->set_theme('header_data')
					  	 ->where('creditos.id',$venta)
					  	 ->set_url('movimientos/creditos/creditos/');
			$crud->header = $crud->header->render(1)->output;  	 
			$crud->output = $this->load->view('pagar_credito',array('output'=>$crud->output),TRUE);
			$this->loadView($crud);				 
		}

		function pagar_credito($plan_credito = ''){
			if(is_numeric($plan_credito)){
				$this->db->select('plan_credito.*, ventas.cliente');
				$this->db->join('creditos','creditos.id = plan_credito.creditos_id');
				$this->db->join('ventas','ventas.id = creditos.ventas_id');
				$plan_credito = $this->db->get_where('plan_credito',array('plan_credito.id'=>$plan_credito));
				if($plan_credito->num_rows()>0){
					$plan_credito = $plan_credito->row();


					$dias = strtotime(date("Y-m-d"))-strtotime(str_replace('/','-',$plan_credito->fecha_a_pagar));
				 	$dias = $dias/60/60/24;				 	
				 	$mora = 0;
				 	$ajustes = $this->db->get('ajustes')->row();
				 	if($dias>$ajustes->cant_dias_mora){
				 		$dias-= $ajustes->cant_dias_mora;
						$mora = $dias * (($plan_credito->monto_a_pagar * $ajustes->porc_interes_mora)/100);
					}

					$this->mora = $mora;
					$this->plan_credito = $plan_credito;
					$this->as['pagar_credito'] = 'pagocliente';
					$this->saldo = $this->db->query("SELECT 
						id,
						creditos_id,
						monto_a_pagar,
						totalpago.total_pagado as totalpagado
						FROM plan_credito
						INNER JOIN (
							SELECT pagocliente.plan_credito_id as plan_credito_id,
						    sum(pagocliente.total_pagado) as total_pagado
						    FROM pagocliente
						    WHERE (pagocliente.anulado = 0 or pagocliente.anulado is null)
						) as totalpago on totalpago.plan_credito_id = plan_credito.id
						WHERE creditos_id = ".$plan_credito->creditos_id);
					$crud = $this->crud_function('','');
					$crud->set_subject('Pago');
					$crud->field_type('clientes_id','hidden',$plan_credito->cliente)
						 ->field_type('cliente','hidden',@$this->db->get_where('clientes',array('id'=>$plan_credito->cliente))->row()->nombres)
						 ->field_type('plan_credito_id','hidden',$plan_credito->id)
						 ->field_type('creditos_id','hidden',$plan_credito->creditos_id)
						 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
						 ->field_type('anulado','hidden',0)
						 ->field_type('sucursal','hidden',$this->user->sucursal)
						 ->field_type('caja','hidden',$this->user->caja)
						 ->field_type('user','hidden',$this->user->id)
						 ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
						 ->where('plan_credito_id',$plan_credito->id)
						 ->callback_field('total_pagado',function($val){
						 		$saldo = $this->saldo;							 									
							 	if(empty($val) && $saldo->num_rows()>0){
							 		$saldo = $saldo->row();
							 		$val = ($this->plan_credito->monto_a_pagar+$this->mora)-$saldo->totalpagado;
							 	}else{
							 		$val = empty($val)?$this->plan_credito->monto_a_pagar:$val;
							 	}
							 	return form_input('total_pagado',$val,'id="field-total_pagado" class="form-control"');
						 })
						 ->callback_field('total_credito',function($val){
						 	$val = empty($val)?$this->plan_credito->monto_a_pagar:$val;
						 	return form_input('total_credito',$val,'id="field-total_credito" class="form-control" readonly');
						 })
						 ->callback_field('total_mora',function($val){
						 	$val = empty($val)?$this->mora:$val;
						 	return form_input('mora',$val,'id="field-mora" class="form-control" readonly');
						 })->callback_after_insert(function($post,$primary){
						 	$saldo = $this->saldo;
						 	if($saldo->num_rows()>0){
						 		$saldo = $saldo->row();
						 		$totalpagado = $saldo->totalpagado+$post['total_pagado'];
						 	}else{
						 		$totalpagado = $post['total_pagado'];
						 	}
						 	if(($post['total_credito']+$post['total_mora'])==$totalpagado){
						 		get_instance()->db->update('plan_credito',array('pagado'=>1),array('id'=>$this->plan_credito->id));
						 	}
						 })->unset_delete()->edit_fields('anulado')->field_type('anulado','true_false');
					$crud->add_action('Imprimir recibo','',base_url('reportes/rep/verReportes/90/html/pago_cliente_id/').'/')
						 ->set_lang_string('insert_success_message','Pago registrado con éxito <a href="javascript:printRecibo({id})">Imprimir recibo</a> | ');

					$crud = $crud->render();
					$crud->output = $this->load->view('pagar_credito',array('output'=>$crud->output),TRUE);
					$crud->title = 'Pago de cuotas';


					$this->as['pagar_credito'] = 'plan_credito';
					$crud->header = $this->plan_credito($plan_credito->creditos_id,true);
					$crud->header->set_theme('header_data')
								 ->where('plan_credito.id',$plan_credito->id)
								 ->set_url('movimientos/creditos/plan_credito/'.$plan_credito->creditos_id.'/');
					$crud->header = $crud->header->render(1)->output;
					$crud->output = $this->load->view('pagar_credito',array('crud'=>$crud),TRUE);
					$this->loadView($crud);

				}
			}
		}

		
	}
?>