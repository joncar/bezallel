<?php

require_once APPPATH.'/controllers/Panel.php';    

class Ventas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
    
    public function ventas($x = '', $y = '',$z = '') { 
        if($x=='nroFactura'){
            echo $this->querys->get_nro_factura();
            die();
        }       
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->unset_read()
             ->unset_delete();
        
        if(!empty($_POST['ventas_id'])){            
            if(is_numeric($_POST['ventas_id'])){
                $crud->where('ventas.id = '.$_POST['ventas_id'],'ESCAPE',FALSE);
            }else{
                $crud->where('ventas.id',-1);
            }
        }
        if(!empty($_POST['precargo'])){            
            get_instance()->precargo = $_POST['precargo'];
            unset($_POST['precargo']);
        }
        $crud->columns('id','transaccion', 'cliente', 'fecha', 'caja', 'nro_factura', 'total_venta', 'status');
        $crud->callback_column('nro_factura', function($val, $row) {
            return '<a href="javascript:showDetail('.$row->id.')">'.$val.'</a>';
        });
        $crud->callback_column('observacion',function($val,$row){
            return 'a';
        });
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '0':return $this->user->admin==1?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>':'Activa';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        if($this->user->admin!=1){
            $crud->unset_edit();
        }
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
             ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
             ->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('ventas.sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja'])){
            $crud->where('ventas.caja', $_SESSION['caja']);
        }
        $crud->add_action('Creditos','',base_url('movimientos/creditos/creditos').'/');        
        $crud->order_by('id','DESC');
        
        ///Validations
        $crud->set_rules('total_venta','Total de venta','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');                
        $crud->set_rules('nro_factura','Numero de factura','required');
        $crud->callback_before_insert(array($this,'validarFacturacion'));
        $crud->callback_after_insert(array($this,'addBody'));
        //$crud->callback_after_update(array($this,'addBody'));
        $crud->unset_back_to_list();
        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = '';
            $default = array();
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            if(is_numeric($z) && is_string($y)){
                $default = $this->querys->{'get'.$y}($z);
                if(is_string($default)){
                    redirect($default);
                    die();
                }
            }
            $output->output = $this->load->view('ventas',array('default'=>$default,'output'=>$output->output,'edit'=>$edit),TRUE);
        }else{
            $output->output = $this->load->view('ventas_list',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);     
    }

    public function ventadetalle($x = '', $y = '') {         
        $crud = parent::crud_function($x, $y);
        if(!empty($_POST['venta'])){
            $crud->where('venta',$_POST['venta']);
        }
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$val))->row();
        });        
        $crud->unset_add()->unset_edit()->unset_delete();
        $output = $crud->render();        
        $this->loadView($output);     
    }
    //Callback para validar si la venta almacena el id de factura o no
    function validarFacturacion($post){
        $tipoFacturacion = $this->db->get_where('tipo_facturacion',array('id'=>$post['tipo_facturacion_id']));
        if($tipoFacturacion->num_rows()==0 || $tipoFacturacion->row()->emite_factura==0){
            $post['nro_factura'] = 0;
        }
        $post['fecha'] = date("Y-m-d H:i:s");
        return $post;
    }
    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        foreach($productos as $p){
            $pro[] = $p->nombre;
            $p->preciocosto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row()->precio_costo;
            $this->db->insert('ventadetalle',array(
                'venta'=>$primary,
                'producto'=>$p->codigo,
                'lote'=>'',
                'cantidad'=>$p->cantidad,
                'pordesc'=>$p->por_desc,
                'preciocosto'=>$p->preciocosto,
                'precioventa'=>$p->precio_venta,
                'precioventadesc'=>$p->precio_venta,
                'totalsindesc'=>($p->precio_venta*$p->cantidad),
                'totalcondesc'=>$p->total,
                'iva'=>0,
                'inventariable'=>0
            ));
        }
        $this->db->update('ventas',array('productos'=>implode($pro)),array('id'=>$primary));
        //Actualizar correlativo
        $tipoFacturacion = $this->db->get_where('tipo_facturacion',array('id'=>$post['tipo_facturacion_id']));

        if($tipoFacturacion->num_rows()>0 && $tipoFacturacion->row()->emite_factura==1){
            $caja = $this->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'abierto'=>1));
            $correlativo = $caja->row()->correlativo+1;
            $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));
        }

        if(!empty(get_instance()->precargo)){
            $precargo = json_decode(get_instance()->precargo);
            if(!empty($precargo->tipo)){
                $precargo = get_instance()->querys->{'get'.$precargo->tipo}($precargo->id);
                if(is_callable(array(get_instance()->querys,$precargo->success[1]))){
                    call_user_func($precargo->success,$post,$primary);         
                }       
            }
        }
    }

    function getFactura($x = '',$y = ''){
        if ($x == 'imprimir' || $x=='imprimir2' || $x == 'imprimirticket') {
            if (!empty($y) && is_numeric($y)) {
                $this->load->library('enletras');        
                $this->db->select('ventas.*, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                $this->db->join('clientes', 'clientes.id = ventas.cliente');
                $this->db->join('cajas', 'cajas.id = ventas.caja');
                $this->db->join('sucursales', 'sucursales.id = ventas.sucursal');
                $venta = $this->db->get_where('ventas', array('ventas.id' => $y));
                if ($venta->num_rows() > 0) {
                    $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                    if ($x == 'imprimir' || $x=="imprimir2" || $x=="imprimirticket") {
                        $view = $x=="imprimir"?"imprimir_factura":"imprimir_factura_linea";
                        $margen = $x=="imprimir"?array(5, 1, 5, 8):array(2, 5, 5, 8);
                        $papel = 'L';
                        if($x=="imprimir" || $x=="imprimir2"){
                            if(!isset($_GET['pdf']) || $_GET['pdf']!=0){
                                ob_clean();
                                $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', $margen);
                                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                                $html2pdf->Output('Factura Legal.pdf', 'D');
                            }else{
                                $this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles));
                            }
                        } else {
                            $output = $this->load->view('reportes/imprimirTicket', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                            echo $output;
                        }
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            }
        }
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->db->get('ajustes')->row();
        $pagoGs = 0;
        if(!empty($_POST['pago_guaranies'])){
            $pagoGs+= $_POST['pago_guaranies'];
        }
        if(!empty($_POST['pago_dolares'])){
            $pagoGs+= $_POST['pago_dolares']*$ajustes->tasa_dolares;
        }
        if(!empty($_POST['pago_reales'])){
            $pagoGs+= $_POST['pago_reales']*$ajustes->tasa_reales;
        }
        if(!empty($_POST['pago_pesos'])){
            $pagoGs+= $_POST['pago_pesos']*$ajustes->tasa_pesos;
        }
        if(!empty($_POST['total_debito'])){
            $pagoGs+= $_POST['total_debito'];
        }

        if($_POST['total_venta']>$pagoGs && $_POST['transaccion']==1){
            $this->form_validation->set_message('validar','Debe pagar la totalidad de la factura');        
            return false;
        }   

        if($_POST['transaccion']==2 && $_POST['cliente']==1){
            $this->form_validation->set_message('validar','Debe indicar a que cliente desea asignarle esta venta a crédito');        
            return false;
        }    

        //Validar limite de crédito
        if($_POST['transaccion']==2){
            $cliente = $this->db->get_where('clientes',array('id'=>$_POST['cliente']));
            if($cliente->num_rows()>0){
                $cliente = $cliente->row();
                $limiteCredito = !empty($cliente->limite_credito)?$cliente->limite_credito:0;
                $saldo = $this->db->query('SELECT get_saldo('.$_POST['cliente'].') as saldo')->row()->saldo;
                if($limiteCredito>0 && ($saldo+$_POST['total_venta'])>$limiteCredito){
                    $this->form_validation->set_message('validar','El cliente excede el limite de crédito establecido');        
                    return false;
                }
            }else{
                $this->form_validation->set_message('validar','El cliente no existe');        
                return false;
            }
        }  

        //Validar stocks
        $productos = json_decode($_POST['productos']);        
        $return = true;
        if($this->db->get('ajustes')->row()->vender_sin_stock==0){
            $msj = '';
            foreach($productos as $p){                
                $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
                if($pro->num_rows()==0){
                    $return = false;
                    $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
                }else{
                    $pro = $pro->row();
                    if($pro->inventariable==1){
                        $producto = $this->db->get_where('productosucursal',array('producto'=>$p->codigo,'sucursal'=>$this->user->sucursal));
                        if($producto->num_rows()==0 || $producto->row()->stock<$p->cantidad){
                            $return = false;
                            $msj.= '<p>El producto '.$p->codigo.' no tiene stock suficiente</p>';
                        }
                    }
                }
            }
            if(!$return){
                $this->form_validation->set_message('validar',$msj);
            }
        }
        return $return;
    }

    function ventas_detail($ventaid){
        $this->as['ventas_detail'] = 'ventadetalle';
        $crud = $this->crud_function('','');
        $crud->where('venta',$ventaid);
        $crud->columns('producto','cantidad','totalcondesc');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud->staticShowList = true;
        $crud = $crud->render();
        echo $crud->output;

    }
    
    function next_nro_factura(){
        echo $this->querys->get_nro_factura();
    }
    /* Cruds */

    public function notas_credito($x = '', $y = '') {
        
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('notas_credito', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('notas_credito', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('notas_credito', array('nota' => $compra, 'detalles' => $this->db->get_where('notas_credito_detalles', array('nota_credito' => $y))), TRUE);
            }
        }

        $this->loadView($output);
    }

    public function notas_credito_cliente($x = '', $y = '') {
        if($x=='anular' && is_numeric($y)){
            $this->db->update('notas_credito_cliente',array('anulado'=>1),array('id'=>$y));
            redirect('movimientos/ventas/notas_credito_cliente/success');
            die();
        }
        $crud = parent::crud_function($x, $y);
        $crud->set_subject('notas de crédito');
        $crud->set_relation('venta','ventas','nro_factura');
        $crud->unset_delete();
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_notaCreditoDuplicar');
        if($crud->getParameters()=='add'){
            //$crud->set_rules('nro_nota_credito','#Nota','required|is_unique[notas_credito_cliente.nro_nota_credito]');
        }
        if($crud->getParameters()=='edit'){
            $crud->unset_fields('productos');
        }
        $crud->columns('id','nro_nota_credito','fecha','cliente','total_monto','actualizar_stock','productos','anulado');
        $crud->set_relation('cliente','clientes','nombres');

        $crud->callback_after_insert(function($post,$primary){
            $this->db = get_instance()->db;
            $productos = json_decode($_POST['productos']);
            $pro = array();            
            foreach($productos as $p){
                $pro[] = $p->producto->nombre_comercial;
                $this->db->insert('notas_credito_cliente_detalle',array(                    
                    'nota_credito'=>$primary,
                    'producto'=>$p->producto->id,
                    'lote'=>'',
                    'cantidad'=>$p->cantidad,
                    'precio_venta'=>$p->precioventadesc,
                    'total'=>$p->precioventadesc * $p->cantidad
                ));
            }
            $this->db->update('notas_credito_cliente',array('productos'=>implode($pro)),array('id'=>$primary));            
            //Hay que actualizar inventario?
            if($post['actualizar_stock']==1){
                $this->db->insert('entrada_productos',array(
                     'fecha'=>date("Y-m-d"),
                     'motivo'=>3,
                     'proveedor'=>'',
                     'total_monto'=>$post['total_monto'],
                     'usuario'=>get_instance()->user->id,
                     'cajadiaria'=>$_SESSION['cajadiaria'],
                     'observacion'=>'Devolución cargada desde el módulo notas de crédito #'.$post['nro_nota_credito']
                ));
                $entrada = $this->db->insert_id();
                foreach($productos as $p){
                     $this->db->insert('entrada_productos_detalles',array(
                        'entrada_producto'=>$entrada,
                        'producto'=>$p->producto->codigo,
                        'lote'=>'',
                        'vencimiento'=>'',
                        'cantidad'=>$p->cantidad,
                        'precio_costo'=>$p->producto->precio_costo,
                        'precio_venta'=>$p->producto->precio_venta,
                        'total'=>$p->totalcondesc * $p->cantidad,
                        'sucursal'=>$_SESSION['sucursal']
                    ));
                }
            }
        });
        $crud->add_action('Anular','',base_url('movimientos/ventas/notas_credito_cliente/anular').'/');
        $output = $crud->render();
        $output->crud = 'compras';
        if ($crud->getParameters()!='list'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('nota_credito',array('output'=>$output->output,'edit'=>$edit),TRUE);            
        }
        $output->title = 'Notas de crédito';
        $this->loadView($output);
    }

    function notaCreditoDuplicar(){
        $existe = $this->db->get_where('notas_credito_cliente',array('venta'=>$_POST['venta'],'anulado'=>0));
        if($existe->num_rows()>0){
            $this->form_validation->set_message('notaCreditoDuplicar','Nota de crédito ya ha sido generada anteriormente');
            return false;
        }
        return true;
    }

    function saldo(){
        if(!empty($_POST['cliente'])){
            $saldo = $this->db->query("select get_saldo('".$_POST['cliente']."') as total")->row()->total;
            echo number_format($saldo,0,',','.').'Gs.';
        }else{
            echo '0 Gs.';
        }
    }

    public function pagocliente($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_subject('pago de clientes');        
        $crud->unset_add()->unset_delete()
        ->set_relation('sucursal','sucursales','denominacion')
        ->set_relation('caja','cajas','denominacion')
        ->set_relation('clientes_id','clientes','{nro_documento}|{nombres}|{apellidos}');
        $crud->columns('j3eb7f57f.cedula','j3eb7f57f.nombres','j3eb7f57f.apellidos','fecha','concepto','total_pagado','anulado','sucursal','caja','cajadiaria','anulado')
             ->callback_column('j3eb7f57f.cedula',function($val,$row){return explode('|',$row->s3eb7f57f)[0];})
             ->callback_column('j3eb7f57f.nombres',function($val,$row){return explode('|',$row->s3eb7f57f)[1];})
             ->callback_column('j3eb7f57f.apellidos',function($val,$row){return explode('|',$row->s3eb7f57f)[2];})
             ->display_as('j3eb7f57f.cedula','Cédula')
             ->display_as('j3eb7f57f.nombres','Nombres')
             ->display_as('j3eb7f57f.apellidos','Apellidos');
        $crud->edit_fields('anulado');
        $output = $crud->render();
        $output->title = 'Pagos de clientes';
        $this->loadView($output);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
