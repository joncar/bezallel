<?php

require_once APPPATH.'/controllers/Panel.php';    

class Productos extends Panel {

    function __construct() {
        parent::__construct();        
    }     
    
    public function categorias($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','denominacion','control_vencimiento','descuentos');
        if($crud->getParameters()=='add'){
            $crud->set_rules('denominacion','Denominación','required|is_unique[categoriaproducto.denominacion]');
        }
        $crud->unset_delete();
        $crud->field_type('control_vencimiento', 'true_false', array(0 => 'No', 1 => 'Si'));
        $output = $crud->render();
        $this->loadView($output);
    }

    public function sub_categoria_producto($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','nombre_sub_categoria_producto','categoriaproducto_id');
        $crud->display_as('nombre_sub_categoria_producto','nombre')
             ->display_as('categoriaproducto_id','categoria');
        $crud->unset_delete();        
        $output = $crud->render();
        $this->loadView($output);
    }

    public function sub_sub_categoria($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','nombre_sub_sub_categoria','sub_categoria_producto_id');
        $crud->display_as('nombre_sub_sub_categoria','nombre')
             ->display_as('sub_categoria_producto_id','SubCategoria');
        $crud->unset_delete();        
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function productos($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y, $this);
        if(!empty($_POST) && empty($_POST['stock'])){
            $_POST['stock'] = 0;
        }
        $crud->set_theme('bootstrap2');
        $crud->columns('id','codigo', 'codigo_interno','nombre_comercial', 'nombre_generico', 'proveedor_id', 'categoria_id', 'sub_categoria_producto_id','sub_sub_categoria_id','precio_costo','precio_venta','precio_venta_mayorista1','precio_venta_mayorista2','precio_venta_mayorista3','propiedades','stock','descmax','cant_1','cant_2','cant_3');
        $crud->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->set_relation('categoria_id', 'categoriaproducto', 'denominacion');
        $crud->set_relation('nacionalidad_id', 'paises', 'denominacion');
        $crud->callback_column('stock',function($val,$row){
            return @$this->db->get_where('productosucursal',array('producto'=>$row->codigo,'sucursal'=>$this->user->sucursal))->row()->stock;
        });
        if (empty($x) || $x == 'ajax_list' || $x == 'success'){
            $crud->set_relation('usuario_id', 'user', 'nombre');        
        }
        //Etiquetas
        $crud->display_as('proveedor_id', 'Proveedor')
                ->display_as('categoria_id', 'Categoria')
                ->display_as('id', 'ID de producto')
                ->display_as('nacionalidad_id', 'Nacionalidad')
                ->display_as('descmin', 'Minimo de descuento')
                ->display_as('descmax', 'Maximo de descuento')
                ->display_as('usuario_id', 'Usuario')
                ->field_type('iva_id', 'dropdown', array('0' => 'Exento', '5' => '5%', '10' => '10%'))
                ->field_type('no_inventariable', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->field_type('no_caja', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->field_type('foto_principal','image',array('path'=>'img/productos','width'=>'150px','height'=>'150px'))
                ->display_as('iva_id', 'IVA')
                ->display_as('no_inventariable','Inventariable');
        //$crud->unset_fields('created', 'modified', 'stock');
        //Validaciones
        if($crud->getParameters()=='add'){
            $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo]');
        }
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);       
        $crud->unset_delete();        
        $crud->callback_before_update(function($post,$primary){
            $producto = get_instance()->db->get_where('productos',array('id'=>$primary));
            if($producto->row()->precio_venta!=$post['precio_venta']){
                get_instance()->db->update('productosucursal',array('precio_venta'=>$post['precio_venta']),array('producto'=>$producto->row()->codigo));
            }
        });
        /*$crud->callback_column('precio_venta',function($val,$row){
            if(!empty($_POST['cliente']) && is_numeric($_POST['cliente'])){
                $cliente = $this->db->get_where('clientes',array('id'=>$_POST['cliente']));
                if($cliente->num_rows()>0){
                    if($cliente->row()->mayorista==1){
                        $val = $row->precio_venta_mayorista1;
                    }
                }
            }
            return $val;
        });*/

        $crud->where("(anulado IS NULL OR anulado = 0)","ESCAPE",TRUE);

        $crud->unset_read();

        $crud->add_action('Productos asociados','',base_url('movimientos/productos/producto_asociado').'/');        
        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = (is_numeric($y))?$edit = $this->db->get_where('productos',array('id'=>$y)):'';
            $output->output = $this->load->view('productos',array('output'=>$output->output,'edit'=>$edit),TRUE);
        }
        $this->loadView($output);
    }

    function producto_asociado($x = ''){
        if(is_numeric($x)){
            $crud = parent::crud_function("",'');
            $crud->where('productos_id',$x)
                 ->field_type('productos_id','hidden',$x)
                 ->unset_columns('productos_id')
                 ->set_relation('descontar','productos','{codigo} {nombre_comercial}');
            $output = $crud->render();

            $head = new ajax_grocery_crud();
            $head->set_table('productos')->set_theme('header_data')->set_subject('producto')->columns('codigo','nombre_comercial','nombre_generico','propiedades')->where('productos.id',$x)->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $output->header = $head->render(1)->output;
            $this->loadView($output);
        }
    }
    
    function buscador_productos(){
        $this->as['buscador_productos'] = 'productos';
        $crud = parent::crud_function("",''); 
        $crud->columns('id','codigo','nombre_comercial','precio_venta','precio_costo','stock');
        if($crud->getParameters(FALSE)!=='json_list'){
            $crud->callback_column('nombre_comercial',function($val,$row){
                return '<a href="javascript:parent.seleccionarProducto('.$row->id.',\''.$val.'\','.$row->precio_venta.','.$row->precio_costo.')">'.$val.'</a>';
            });
        }
        $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
        $output = $crud->render();
        $output->view = "json";
        $output->output = $this->load->view('busqueda',array('output'=>$output->output),TRUE);
        $this->loadView($output);
    }
    
    public function inventario($x = '', $y = '') {   
        if($x=='reload'){
            $this->db->query('call set_stock('.$y.')');
            redirect('movimientos/productos/inventario/success');
        }     
        $this->as['inventario'] = 'productosucursal';        
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->unset_read()
             ->unset_export()
             ->unset_print()
             ->unset_edit()
             ->unset_delete()
             ->unset_add();     
        $crud->columns('j286e18ee.codigo','j286e18ee.nombre_comercial','j286e18ee.nombre_generico','stock','precio_venta','sucursal');
        $crud->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}');
        $crud->set_relation('sucursal','sucursales','denominacion');
        $crud->callback_column('scb5fc3d6', function($val, $row) {
            return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
        });
        $crud->callback_column('j286e18ee.codigo', function($val, $row) {
            return explode('|',$row->s286e18ee)[0];
        });
        $crud->callback_column('j286e18ee.nombre_comercial', function($val, $row) {
            return explode('|',$row->s286e18ee)[1];
        });
        $crud->callback_column('j286e18ee.nombre_generico', function($val, $row) {
            return explode('|',$row->s286e18ee)[2];
        });
        $crud->callback_column('stock', function($val, $row) {
            return $val.' <a href="' . base_url('movimientos/productos/inventario/reload/'.$row->producto).'"><i class="fa  fa-refresh "></i></a>';
        });
        $crud->display_as('j286e18ee.codigo','Codigo')
             ->display_as('j286e18ee.nombre_comercial','Nombre Comercial')
             ->display_as('j286e18ee.nombre_generico','Nombre Genérico');
        $crud->set_primary_key('codigo','productos');
        if (is_numeric($x)) {
            $crud->where('sucursal', $x);
        }
        $crud->where("(j286e18ee.anulado IS NULL OR j286e18ee.anulado = 0)","ESCAPE",TRUE);
        $output = $crud->render();
        $output->crud = 'productosucursal';
        $this->loadView($output);
    }

    public function inventario_modal($sucursal = ''){
        $crud = new ajax_grocery_crud();
        $crud->set_table('productosucursal')
             ->set_subject('Inventario')
             ->set_theme('bootstrap2')
             ->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read()             
             ->callback_column('Codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             });
            $crud->columns('j286e18ee.codigo','j286e18ee.nombre_comercial','j286e18ee.nombre_generico','stock','precio_venta','sucursal');
            $crud->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}');
            $crud->set_relation('sucursal','sucursales','denominacion');
            $crud->callback_column('scb5fc3d6', function($val, $row) {
                return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
            });
            $crud->callback_column('j286e18ee.codigo', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[0];
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });
            $crud->callback_column('j286e18ee.nombre_comercial', function($val, $row) {
                $cod = explode('|',$row->s286e18ee)[0];
                $val = explode('|',$row->s286e18ee)[1];
                return '<a href="javascript:selCod(\''.$cod.'\',\'\',\'\')">'.$val.'</a>';
            });
            $crud->callback_column('j286e18ee.nombre_generico', function($val, $row) {
                return explode('|',$row->s286e18ee)[2];
            });
            $crud->callback_column('stock', function($val, $row) {
                return $val.' <a href="' . base_url('movimientos/productos/inventario/reload/'.$row->producto).'"><i class="fa  fa-refresh "></i></a>';
            });
            $crud->display_as('j286e18ee.codigo','Codigo')
                 ->display_as('j286e18ee.nombre_comercial','Nombre Comercial')
                 ->display_as('j286e18ee.nombre_generico','Nombre Genérico');
            $crud->set_primary_key('codigo','productos');

        if(!empty($this->user->sucursal) && !is_numeric($sucursal)){
            $crud->where('sucursal',$this->user->sucursal);
        }elseif(is_numeric($sucursal)){
            $crud->where('sucursal',$sucursal);
        }
        $crud = $crud->render();
        $this->loadView($crud);

    }

    public function inventario_modal_compras($sucursal = ''){
        $crud = new ajax_grocery_crud();
        $crud->set_table('productos')
             ->set_subject('Inventario')
             ->set_theme('bootstrap2')
             ->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read()             
             ->callback_column('codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             });
            $crud->columns('codigo','nombre_comercial','nombre_generico','precio_venta');                                    
            $crud->callback_column('codigo', function($val, $row) {                
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });            
            $crud->set_primary_key('codigo','productos');
            $crud = $crud->render();
            $this->loadView($crud);
    }
    
    public function transferencias($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/ventas'));
        else {
            $crud = parent::crud_function($x, $y);
            $crud->set_theme('bootstrap2');
            $crud->unset_delete();
            $crud->set_relation('sucursal_origen', 'sucursales', 'denominacion')
                    ->set_relation('sucursal_destino', 'sucursales', 'denominacion');
            $crud->display_as('procesado', 'Status');            
            $crud->field_type('procesado', 'dropdown', array('0' => 'No procesado', '-1' => 'Rechazado', '2' => 'Aprobado', '3' => 'Entregado'));
            if ($this->router->fetch_class() != 'admin') {
                $crud->where('sucursal_destino', $_SESSION['sucursal']);
                $crud->or_where('sucursal_origen', $_SESSION['sucursal']);
            }

            $crud->callback_column('procesado', function($val, $row) {
                if ($row->sucursal_origen == $_SESSION['sucursal'] && $val == 0){
                    return form_dropdown('procesado', array('0' => 'Sin procesar', '-1' => 'Rechazado', '1' => 'Aprobado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                elseif ($row->sucursal_destino == $_SESSION['sucursal'] && $val == 1){
                    return form_dropdown('procesado', array('1' => 'Aprobado', '2' => 'Entregado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                else {
                    switch ($val) {
                        case '0': return 'No procesado';
                        case '-1': return 'Rechazado';
                        case '1': return 'Aprobado';
                        case '2': return 'Entregado';
                    }
                }
            });
            $crud->order_by('id','DESC');
            $crud->columns('id','sucursal_origen','sucursal_destino','fecha_solicitud','procesado','user_id');
            $output = $crud->render();
            $output->crud = 'transferencias';
            if ($x == 'add'){
                $output->output = $this->load->view('transferencias', array(), TRUE);
            }
            if ($x == 'edit' && !empty($y) && is_numeric($y)) {
                $transferencia = $this->db->get_where('transferencias', array('id' => $y));
                $detalles = $this->db->get_where('transferencias_detalles', array('transferencia' => $transferencia->row()->id));
                $output->output = $this->load->view('transferencias', array('transferencia' => $transferencia->row(), 'detalles' => $detalles), TRUE);
            }
            if ($x == 'imprimir') {
                if (!empty($y) && is_numeric($y)) {
                    $this->db->select('transferencias.*,suco.id as sucursalOrigenId, suco.denominacion as sucursalOrigen, sucd.denominacion as sucursalDestino');
                    $this->db->join('sucursales suco', 'suco.id = transferencias.sucursal_origen');
                    $this->db->join('sucursales sucd', 'sucd.id = transferencias.sucursal_destino');
                    $venta = $this->db->get_where('transferencias', array('transferencias.id' => $y));
                    if ($venta->num_rows() > 0) {
                        $this->db->join('productos', 'transferencias_detalles.producto = productos.codigo');
                        $detalles = $this->db->get_where('transferencias_detalles', array('transferencia' => $venta->row()->id));
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', array(20, 5, 5, 8));
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/imprimir_transferencia', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                        $html2pdf->Output('Reporte Transferencia.pdf', 'D');
                        //echo $this->load->view('reportes/imprimir_transferencia',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                    
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            } else {
                $this->loadView($output);
            }
        }
    }

    public function productosucursal_ainsert($post, $id) {
        $this->db->update('productosucursal', array('precio_venta' => $post['precio_venta']), array('producto' => $post['producto']));
        $this->db->update('productos', array('precio_venta' => $post['precio_venta']), array('codigo' => $post['producto']));
    }
    /* Cruds */

    function productos_costos($prodId){
        $crud = $this->crud_function("","");
        $crud->where('productos_id',$prodId)
             ->field_type('productos_id','hidden',$prodId)
             ->unset_columns('productos_id')
             ->set_relation('ingredientes_id','productos','{codigo} {nombre_comercial}',array('ingrediente'=>1));                
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header = $crud->header->set_table('productos')
               ->set_subject('productos')
               ->set_theme('header_data')
               ->columns('codigo','nombre_generico','nombre_comercial')
               ->where('productos.id',$prodId)
               ->render('1')
               ->output;         
        $crud->title = "Adm. Costos de productos";
        $crud->output = $this->load->view('productos_costos',array('crud'=>$crud),TRUE);
        $this->loadView($crud);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
