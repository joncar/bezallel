<div id="responseMasDetalles">
	<div class="modal fade" id="masdetalles" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document" style="max-width:90vw">
	        <div class="modal-content"  > 
	            <div class="modal-header">
	                <h4 class="modal-title">Más detalles</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-12 col-md-6">        		
	                        <table class="table table-bordered">
	                            <thead>
	                                <tr>
	                                    <th colspan="4">
	                                        Últimas 5 compras
	                                    </th>
	                                </tr>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Fecha</th>
	                                    <th>Cantidad</th>
	                                    <th>Proveedor</th>
	                                    <th>P.Costo</th>
	                                    <th>Venc.</th>
	                                    <th>Total</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <?php foreach ($this->elements->compradetalles(array('producto' => $producto), 5, array('id', 'DESC'))->result() as
	                                        $v): $c = $this->elements->compras(array('compras.id' => $v->compra))->row(); ?>
	                                    <tr>
	                                        <th><?= $v->compra ?></th>
	                                        <th><?= @$c->fecha ?></th>
	                                        <th><?= $v->cantidad ?></th>
	                                        <th><?= $c->proveedor ?></th>
	                                        <th><?= number_format($v->precio_costo,0,',','.') ?></th>
	                                        <th><?= !empty($v->vencimiento)?date("d-m-Y",strtotime($v->vencimiento)):'N/A' ?></th>
	                                        <th><?= number_format($v->total,0, ',', '.'); ?></th>
	                                    </tr>
									<?php endforeach ?>
	                            </tbody>
	                        </table>
	                    </div>
	                    <div class="col-12 col-md-6">        		
	                        <table class="table table-bordered">
	                            <thead>
	                                <tr>
	                                    <th colspan="5">Últimas 5 ventas</th>
	                                </tr>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Fecha</th>
	                                    <th>Cantidad</th>
	                                    <th>P.Venta</th>
	                                    <th>Total</th>
	                                </tr>
	                            </thead>
	                            <tbody>
								<?php foreach ($this->elements->ventadetalle(array('producto' => $producto), 5, array('id', 'DESC'))->result() as $v): $c = $this->elements->ventas(array('ventas.id' => $v->venta))->row(); ?>
	                                    <tr>
	                                        <th><?= $v->venta ?></th>
	                                        <th><?= $c->fecha ?></th>
	                                        <th><?= $v->cantidad ?></th>
	                                        <th><?= number_format($v->precioventa,0, ',', '.'); ?></th>
	                                        <th><?= number_format($v->totalcondesc,0, ',', '.'); ?></th>
	                                    </tr>
								<?php endforeach ?>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-12 col-md-6">
	                        <h4>Ventas por mes</h4>  
	                        <div id="ventasmes"></div> 
	                    </div>
	                    <div class="col-12 col-md-6">
	                        <h4>Ventas por semana</h4>
	                        <div id="ventasSemana">

	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<script>

	    window.afterLoad.push(function(){
	    $("#masdetalles").on('shown.bs.modal', function () {
	        Morris.Bar({
	            element: 'ventasmes',
	            data: [
	        <?php
	        for ($i = 1;
	                $i <= 12;
	                $i++):
	            $val = $this->db->query("SELECT COUNT(ventadetalle.id) as val FROM ventadetalle INNER JOIN ventas ON ventas.id = ventadetalle.venta WHERE ventadetalle.producto = '" . $producto . "' AND MONTH(ventas.fecha) = '" . $i . "' AND YEAR(ventas.fecha) = '" . date("Y") . "'")->row()->val;
	            ?>
	                            {y: '<?= meses_short($i) ?>', a:<?= $val ?>},
	        <?php endfor ?>
	                    ],
	                    xkey: 'y',
	                            ykeys: ['a'],
	                    labels: ['Meses 2019']
	                });

	                Morris.Bar({
	                    element: 'ventasSemana',
	                    data: [
	        <?php
	        $thisweek = date("Y-m-d", strtotime('monday this week'));
	        for ($i = 0;
	                $i < 7;
	                $i++):
	            $time = date("Y-m-d", strtotime($thisweek . ' +' . $i . ' days'));
	            $val = $this->db->query("SELECT COUNT(ventadetalle.id) as val FROM ventadetalle INNER JOIN ventas ON ventas.id = ventadetalle.venta WHERE ventadetalle.producto = '" . $producto . "' AND DATE(ventas.fecha) = '" . $time . "'")->row()->val;
	            ?>
	                            {y: '<?= $time ?>', a:<?= $val ?>},
	        <?php endfor ?>
	                    ],
	                    xkey: 'y',
	                            ykeys: ['a'],
	                    labels: ['Semana <?= $thisweek ?>']
	                });
	    });
	    });

	</script>
</div>