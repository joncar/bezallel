<div id="addCliente" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="insertar('maestras/clientes/insert',this,'.resultClienteAdd',function(data){setCliente(data)}); return false;">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">	        
	        <h4 class="modal-title">Agregar cliente</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	  		  <div class="row" style="margin-left: 0; margin-right: 0">
  		  		<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nombre</label>
				    <input type="text" class="form-control" name="nombres" placeholder="Nombre del cliente">
				  </div>
				</div>
				

				<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Apellido</label>
				    <input type="text" class="form-control" name="apellidos" placeholder="Apellido del cliente">
				  </div>
				</div>

				<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">#Documento</label>
				    <input type="text" class="form-control" name="nro_documento" placeholder="Documento del cliente">
				  </div>
				</div>

				<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Celular</label>
				    <input type="text" class="form-control" name="celular" placeholder="Celular del cliente">
				  </div>				
				</div>

				<div class="col-12 col-md-12">
					<div class="form-group">
					    <label for="exampleInputEmail1">Dirección</label>
					    <input type="text" class="form-control" name="direccion" placeholder="Dirección del cliente">
					  </div>
				</div>

				<div class="col-12 col-md-12">
					<div class="resultClienteAdd"></div>
			    </div>
			</div>			  
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="mayorista" value="1">
	      	<input type="hidden" name="zonas_id" value="1">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<script>
  window.afterLoad.push(function(){
    $("#addCliente").on("show.bs.modal",function(){      
      $(".resultClienteAdd").html('').removeClass('alert alert-info alert-success alert-danger');
    });     
    $("#addCliente").on("shown.bs.modal",function(){
      $('#addCliente input[name="nombres"]').focus();
    });

    $("#addCliente").on("hidden.bs.modal",function(){         
      setTimeout(function(){$("#codigoAdd").focus();},600);
    });

  });
</script>