<?php
if (empty($edit) || $edit->num_rows() > 0):
    $edit = !empty($edit) ? $edit->row() : '';
    $accion = empty($edit) ? 'insert' : 'update/' . $edit->id;
    ?>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>-->
    <script src="<?= base_url() ?>js/morris.js"></script>                                
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">-->
    <link rel="stylesheet" href="<?= base_url() ?>css/morris.css">
    <form action="" onsubmit="enviar(this); return false;" method="post">
        <style>
            .panel-body > div{
                margin-top:10px;
            }

            .agrupado, .agrupado2{
                position: relative;
                width: 100%;
                margin-left:5px;
            }
            .agrupado:first-child, .agrupado2:first-child{
                margin-left: 0;
            }

            .agrupado > label, .agrupado2 > label{
                position:absolute;
                top: 5px;
                left: 7px;
            }

            .agrupado > input{
                padding-left:30px;
            }

            .agrupado2 > input{
                padding-left:50px;
            }
        </style>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Datos generales</h1>
                    </div>
                    <div class="panel-body">
                        <div class="form-group row" id="codigo_field_box">
                            <label for="field-codigo" id="codigo_display_as_box" class="col-md-2">
                                Codigo interno<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php 
                                    if($accion=='insert'){                                        
                                        $codigo = $this->db->query('select getUltimoCodigoInterno()+1 as cod')->row()->cod;
                                    }else{
                                        $codigo = @$edit->codigo_interno;
                                    }
                                ?>
                                <input id="field-codigo_interno" name="codigo_interno" class="form-control codigo_interno" type="text" value="<?= $codigo ?>">                    
                            </div>
                        </div>

                        <div class="form-group row" id="codigo_field_box">
                            <label for="field-codigo" id="codigo_display_as_box" class="col-md-2">
                                Codigo de barra<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <input id="field-codigo" name="codigo" class="form-control codigo" type="text" value="<?= @$edit->codigo ?>">                    
                            </div>
                        </div>
                        
                        <div class="form-group row" id="nombre_comercial_field_box">
                            <label for="field-nombre_comercial" id="nombre_comercial_display_as_box" class="col-md-2">
                                Nombre comercial<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <input id="field-nombre_comercial" name="nombre_comercial" class="form-control codigo" type="text" value="<?= @$edit->nombre_comercial ?>">                    
                            </div>
                        </div>
                        <div class="form-group row" id="nombre_generico_field_box">
                            <label for="field-nombre_generico" id="nombre_generico_display_as_box" class="col-md-2">
                                Nombre genérico<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <input id="field-nombre_generico" name="nombre_generico" class="form-control codigo" type="text" value="<?= @$edit->nombre_generico ?>">                    
                            </div>
                        </div>
                        <div class="form-group row" id="propiedades_field_box">
                            <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2">
                                Propiedades<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <input id="field-propiedades" name="propiedades" class="form-control codigo" type="text" value="<?= @$edit->propiedades ?>">                    
                            </div>
                        </div>
                        <div class="form-group row" id="propiedades_field_box">
                            <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2">
                                Proveedor<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php echo form_dropdown_from_query('proveedor_id', 'proveedores', 'id', 'denominacion', @$edit->proveedor_id, 'id="field-proveedor_id"'); ?>
                            </div>
                        </div>
                        <div class="form-group row" id="categoria_id_field_box">
                            <label for="field-propiedades" id="categoria_id_display_as_box" class="col-md-2">
                                Categoria <a href="#categorias" data-toggle="modal"><i class="fa fa-plus"></i></a><span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php $categorias = $this->db->get_where('categoriaproducto'); ?>
                                <select name="categoria_id" id="field-categoria_id" class="form-control chosen-select">
                                    <option value="">Seleccione una opcion</option>
                                    <?php foreach($categorias->result() as $categoria): ?>
                                        <option value="<?= $categoria->id ?>" data-descuento="<?= $categoria->descuento ?>" <?= $categoria->id==@$edit->categoria_id?'selected="true"':'' ?>><?= $categoria->denominacion ?></option>
                                    <?php endforeach ?>
                                </select>
                                
                            </div>
                        </div>
                        <div class="form-group row" id="sub_categoria_producto_id_field_box">
                            <label for="field-sub_categoria_producto_id" id="sub_categoria_producto_id_display_as_box" class="col-md-2">
                                Sub Categoria  <a href="#subcategorias" data-toggle="modal"><i class="fa fa-plus"></i></a><span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php echo form_dropdown_from_query('sub_categoria_producto_id', 'sub_categoria_producto', 'id', 'nombre_sub_categoria_producto', @$edit->sub_categoria_producto_id, 'id="field-sub_categoria_producto_id"'); ?>
                            </div>
                        </div>
                        <div class="form-group row" id="sub_sub_categoria_field_box">
                            <label for="field-propiedades" id="sub_sub_categoria_display_as_box" class="col-md-2">
                                Sub Categoria 2 <a href="#subsubsubcategorias" data-toggle="modal"><i class="fa fa-plus"></i></a><span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php echo form_dropdown_from_query('sub_sub_categoria_id', 'sub_sub_categoria', 'id', 'nombre_sub_sub_categoria', @$edit->sub_sub_categoria_id, 'id="field-sub_sub_categoria_id"'); ?>
                            </div>
                        </div>
                        <div class="form-group row" id="propiedades_field_box">
                            <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2">
                                Marca: <a href="#marcas" data-toggle="modal"><i class="fa fa-plus"></i></a>
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php echo form_dropdown_from_query('marcas_id', 'marcas', 'id', 'marcas_nombre', @$edit->marcas_id, 'id="field-marcas_id"'); ?>
                            </div>
                        </div>
                        <div class="form-group row" id="unidad_medida_id_field_box">
                            <label for="field-unidad_medida_id" id="unidad_medida_id_display_as_box" class="col-md-2">
                                Unidad de medida: <a href="#unidades" data-toggle="modal"><i class="fa fa-plus"></i></a>
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php echo form_dropdown_from_query('unidad_medida_id', 'unidad_medida', 'id', 'denominacion', @$edit->unidad_medida_id, 'id="field-unidad_medida_id"'); ?>
                            </div>
                        </div>
                        <div class="form-group row" id="propiedades_field_box">
                            <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2">
                                IVA<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-10">
                                <?php $val = !isset($edit->iva_id) ? 10 : $edit->iva_id; ?>
                                <?php echo form_dropdown('iva_id', array('0' => 'Exento', '5' => '5%', '10' => '10%'), $val, 'id="field-iva_id" class="form-control"') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Precios</h1>
                    </div>
                    <div class="panel-body">
                        <div style="display: flex;">
                            <div class="" id="precio_costo_field_box" style="width:100%;">
                                <label for="field-precio_costo" id="precio_costo_display_as_box">
                                    P.costo<span class="required">*</span>:
                                </label>
                                <input id="field-precio_costo" name="precio_costo" class="form-control codigo" type="text" value="<?= @$edit->precio_costo ?>" placeholder="Precio de costo">
                            </div>
                            <div class="" id="precio_venta_field_box"  style="width:100%; margin-left:5px;">
                                <label for="field-precio_venta" id="precio_venta_display_as_box">
                                    P.venta<span class="required">*</span>:
                                </label>
                                <input id="field-precio_venta" name="precio_venta" class="form-control codigo" type="text" value="<?= @$edit->precio_venta ?>"  placeholder="Precio de venta">                    
                            </div>
                        </div>
                        <div class="form-group row" id="precio_venta_field_box">
                            <label for="field-precio_venta" id="precio_venta_display_as_box" class="col-md-12">
                                Precios mayoristas<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-12" style="display: flex">
                                <div class="agrupado">
                                    <label for="field-precio_venta_mayorista1" id="precio_venta_mayorista1_display_as_box">
                                        1<span class="required">*</span>:
                                    </label>
                                    <input id="field-precio_venta_mayorista1" name="precio_venta_mayorista1" class="form-control codigo" type="text" value="<?= @$edit->precio_venta_mayorista1 ?>" placeholder="Nivel 1">
                                </div>
                                <div class="agrupado">
                                    <label for="field-precio_venta_mayorista2" id="precio_venta_mayorista2_display_as_box">
                                        2<span class="required">*</span>:
                                    </label>
                                    <input id="field-precio_venta_mayorista2" name="precio_venta_mayorista2" class="form-control codigo" type="text" value="<?= @$edit->precio_venta_mayorista2 ?>" placeholder="Nivel 2">
                                </div>
                                <div class="agrupado">
                                    <label for="field-precio_venta_mayorista3" id="precio_venta_mayorista3_display_as_box">
                                        3<span class="required">*</span>:
                                    </label>
                                    <input id="field-precio_venta_mayorista3" name="precio_venta_mayorista3" class="form-control codigo" type="text" value="<?= @$edit->precio_venta_mayorista3 ?>" placeholder="Nivel 3">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="precio_venta_field_box">
                                <label for="field-precio_venta" id="precio_venta_display_as_box" class="col-md-12">
                                    Cantidad personalizada de mayoristas<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-12" style="display: flex">
                                    <div class="agrupado">
                                        <label for="field-cant_1" id="cant_1_display_as_box">
                                            1<span class="required">*</span>:
                                        </label>
                                        <input id="field-cant_1" name="cant_1" class="form-control codigo" type="text" value="<?= empty($edit->cant_1)?0:$edit->cant_1 ?>" placeholder="Nivel 1">
                                    </div>
                                    <div class="agrupado">
                                        <label for="field-cant_2" id="cant_2_display_as_box">
                                            2<span class="required">*</span>:
                                        </label>
                                        <input id="field-cant_2" name="cant_2" class="form-control codigo" type="text" value="<?= empty($edit->cant_2)?0:$edit->cant_2 ?>" placeholder="Nivel 2">
                                    </div>
                                    <div class="agrupado">
                                        <label for="field-cant_3" id="cant_3_display_as_box">
                                            3<span class="required">*</span>:
                                        </label>
                                        <input id="field-cant_3" name="cant_3" class="form-control codigo" type="text" value="<?= empty($edit->cant_3)?0:$edit->cant_3 ?>" placeholder="Nivel 3">
                                    </div>
                                </div>
                            </div>

                        <div class="form-group row" id="precio_venta_field_box">
                            <label for="field-precio_venta" id="precio_venta_display_as_box" class="col-md-12">
                                %Descuentos<span class="required">*</span>:
                            </label>
                            <div class="col-xs-12 col-md-12" style="display: flex">
                                <div class="agrupado2">
                                    <label for="field-descmin" id="descmin_display_as_box">
                                        Min<span class="required">*</span>:
                                    </label>
                                    <input id="field-descmin" name="descmin" class="form-control codigo" type="text" value="<?= @$edit->descmin ?>" placeholder="Mínimo">
                                </div>
                                <div class="agrupado2">
                                    <label for="field-descmax" id="descmax_display_as_box">
                                        Max<span class="required">*</span>:
                                    </label>
                                    <input id="field-descmax" name="descmax" class="form-control codigo" type="text" value="<?= @$edit->descmax ?>" placeholder="Maximo">
                                </div>
                            </div>
                        </div>
                        <div style="display: flex">
                            <div class="form-group" id="stock_minimo_field_box" style="width:100%">
                                <label for="field-stock_minimo" id="stock_minimo_display_as_box">
                                    Min. Stock<span class="required">*</span>:
                                </label>
                                <input id="field-stock_minimo" name="stock_minimo" class="form-control codigo" type="text" value="<?= @$edit->stock_minimo ?>" placeholder="Cantidad mínima en stock">
                            </div>
                            <div class="form-group" id="precio_venta_mayorista3_field_box" style="width:100%">                      
                                <div style="text-align: left;margin: 19px 19px 0 19px;">                                    
                                    Balanza
                                    <input type="radio" name="balanza" value="1" <?= @$edit->balanza == 1 ? 'checked' : '' ?>> SI 
                                    <input type="radio" name="balanza" value="0" <?= @$edit->balanza == 0 ? 'checked' : '' ?>> NO <br> 
                                    Inventariable
                                    <input type="radio" name="inventariable" value="1" <?= $accion == 'insert' || $accion!='insert' && $edit->inventariable == 1 ? 'checked' : '' ?>> SI
                                    <input type="radio" name="inventariable" value="0" <?= $accion!='insert' && $edit->inventariable == 0 ? 'checked' : '' ?>> NO

                                    Anulado
                                    <input type="radio" name="anulado" value="1" <?= $accion!='insert' && $edit->anulado == 1 ? 'checked' : '' ?>> SI
                                    <input type="radio" name="anulado" value="0" <?= $accion == 'insert' || $accion!='insert' && $edit->anulado == 0 ? 'checked' : '' ?>> NO
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-chevron-down"></i> Web
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class='form-group' id="descripcion_field_box">
                                    <label for='field-descripcion' id="descripcion_display_as_box">
                                        Descripcion:
                                    </label>
                                    <textarea id='field-descripcion' name='descripcion' class='texteditor' ><?= @$edit->descripcion ?></textarea>
                                </div>
                                <div class='form-group' id="foto_principal_field_box">
                                    <label for='field-foto_principal' id="foto_principal_display_as_box" style="width:100%">
                                        Foto principal:
                                    </label>
                                    <div class="crop" data-width="150px" data-height="150px" id="crop-foto_principal">
                                        <div class="slim" id="slim-foto_principal"          
                                         data-service="<?= base_url('movimientos/productos/productos/cropper/foto_principal/') ?>"
                                         data-post="input, output, actions"
                                         data-size="120,120"
                                         data-instant-edit="true"
                                         data-push="true"
                                         data-did-upload="imageSlimUpload"
                                         data-download="true"
                                         data-will-save="addValueSlim"
                                         data-label="Subir foto"
                                         data-meta-name="foto"                         
                                         data-force-size="120,120"  style="width:200px; height:120px;">                                      
                                         <input type="file" id="slim-foto_principal"/>                        
                                         <input type="hidden" data-val="<?= @$edit->foto_principal ?>" name="foto_principal" value="<?= @$edit->foto_principal ?>" id="field-foto_principal">
                                         <img id="image-foto_principal" style="margin-bottom: 0" src="<?= base_url().'img/productos/'.@$edit->foto_principal ?>" style="width:120px; height:120px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="stock" value="<?= empty($edit->stock)?0:$edit->stock ?>">

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">         
                    <div class="btn-group" role="group">
                        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#procesar">Guardar producto</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" <?= empty($edit) ? 'disabled' : 'data-target="#masdetalles" data-toggle="modal"' ?>>Más datos</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="responseDiv"></div>
            </div>
        </div>
    </form>
    <?php if (!empty($edit)): ?>
        <div class="modal fade" id="masdetalles" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Más detalles</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="4">
                                                Últimas 5 compras
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>#Compra</th>
                                            <th>Fecha</th>
                                            <th>Cantidad</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($this->elements->compradetalles(array('producto' => $edit->codigo), 5, array('id', 'DESC'))->result() as
                                                $v): $c = $this->elements->compras(array('compras.id' => $v->compra))->row(); ?>
                                            <tr>
                                                <th><?= $v->compra ?></th>
                                                <th><?= $c->fecha ?></th>
                                                <th><?= $v->cantidad ?></th>
                                                <th><?= number_format($v->total, 2, ',', '.'); ?></th>
                                            </tr>
        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12 col-md-6">                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="5">Últimas 5 ventas</th>
                                        </tr>
                                        <tr>
                                            <th>#Factura</th>
                                            <th>Fecha</th>
                                            <th>Cantidad</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
        <?php foreach ($this->elements->ventadetalle(array('producto' => $edit->codigo), 5, array('id', 'DESC'))->result() as
                $v): $c = $this->elements->ventas(array('ventas.id' => $v->venta))->row(); ?>
                                            <tr>
                                                <th><?= $v->venta ?></th>
                                                <th><?= $c->fecha ?></th>
                                                <th><?= $v->cantidad ?></th>
                                                <th><?= number_format($v->totalcondesc, 2, ',', '.'); ?></th>
                                            </tr>
        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <h4>Ventas por mes</h4>  
                                <div id="ventasmes"></div> 
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <h4>Ventas por semana</h4>
                                <div id="ventasSemana">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>
            $("#masdetalles").on('shown.bs.modal', function () {
                Morris.Bar({
                    element: 'ventasmes',
                    data: [
        <?php
        for ($i = 1;
                $i <= 12;
                $i++):
            $val = $this->db->query("SELECT COUNT(ventadetalle.id) as val FROM ventadetalle INNER JOIN ventas ON ventas.id = ventadetalle.venta WHERE ventadetalle.producto = '" . $edit->codigo . "' AND MONTH(ventas.fecha) = '" . $i . "' AND YEAR(ventas.fecha) = '" . date("Y") . "'")->row()->val;
            ?>
                            {y: '<?= meses_short($i) ?>', a:<?= $val ?>},
        <?php endfor ?>
                    ],
                    xkey: 'y',
                            ykeys: ['a'],
                    labels: ['Meses 2019']
                });

                Morris.Bar({
                    element: 'ventasSemana',
                    data: [
        <?php
        $thisweek = date("Y-m-d", strtotime('monday this week'));
        for ($i = 0;
                $i < 7;
                $i++):
            $time = date("Y-m-d", strtotime($thisweek . ' +' . $i . ' days'));
            $val = $this->db->query("SELECT COUNT(ventadetalle.id) as val FROM ventadetalle INNER JOIN ventas ON ventas.id = ventadetalle.venta WHERE ventadetalle.producto = '" . $edit->codigo . "' AND DATE(ventas.fecha) = '" . $time . "'")->row()->val;
            ?>
                            {y: '<?= $time ?>', a:<?= $val ?>},
        <?php endfor ?>
                    ],
                    xkey: 'y',
                            ykeys: ['a'],
                    labels: ['Semana <?= $thisweek ?>']
                });
            });
        </script>
    <?php endif ?>
<?php else: ?>
    Ocurrio un error al procesar
<?php endif ?>


<?php $this->load->view('_unidades_medida_modal'); ?>
<?php $this->load->view('_marcas_modal'); ?>
<?php $this->load->view('_categorias_modal'); ?>

<script>
    function enviar(form){
        var ci = $("input[name='codigo_interno']").val();
        var cc = $("input[name='codigo']").val();
        if(cc=='' && ci!=''){
            $("input[name='codigo']").val(ci);
            $("form").submit();
        }else{
            insertar('movimientos/productos/productos/<?= $accion ?>', form, '.responseDiv',function(data){
                $(".responseDiv").html(data.success_message);
                var lastCod = parseInt($("#field-codigo_interno").val());
                $("input,select").val('');
                $("#field-codigo_interno").val(lastCod+1);
                $("select.chosen-select").chosen().trigger('liszt:updated');
                $("#field-iva_id").val(10);
            });
        }
    }

    var sub1 = new relation_dependency();
    var sub2 = new relation_dependency();
    sub1.select_dependency_chosen(
        'maestras/sub_categoria_producto/json_list',
        $("#field-categoria_id"),
        $("#field-sub_categoria_producto_id"),
        'categoriaproducto_id',
        'id',
        'nombre_sub_categoria_producto'
    );

    sub2.select_dependency_chosen(
        'maestras/sub_sub_categoria/json_list',
        $("#field-sub_categoria_producto_id"),
        $("#field-sub_sub_categoria_id"),
        'sub_categoria_producto_id',
        'id',
        'nombre_sub_sub_categoria'
    ); 

    $(document).on('change','#field-categoria_id',function(){
        $(".categoriaproducto_id").val($('#field-categoria_id option:selected').html());
        $("#subcategorias input[name='categoriaproducto_id']").val($(this).val());
        
        //if($("#field-descmin").val()==''){
            $("#field-descmin").val(0);
        //}
        //if($("#field-descmax").val()==''){
            $("#field-descmax").val($(this).find('option:selected').data('descuento'));
        //}
    });

    $(document).on('change','#field-sub_categoria_producto_id',function(){
        $(".sub_categoria_producto_id").val($('#field-sub_categoria_producto_id option:selected').html());
        $("#subsubsubcategorias input[name='sub_categoria_producto_id']").val($(this).val());
    });
</script>