<style>
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}
</style>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">
			<b>Datos generales</b>
		</h1>
	</div>
	<div class="panel-body">
		<div class="row" style="position: relative;">
			<!--<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
				#FACTURA: <span id="nroFactura"></span>
			</div>-->
			<div class="col-xs-12 col-md-9">
				<div class="col-xs-12 col-md-4">
					Cliente <a href="#addCliente" data-toggle="modal" style="color:green"><i class="fa fa-plus"></i></a>: 
					<?php 
						$this->db->limit(1);
						echo form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',1,'id="cliente"') 
					?>
					<a href="javascript:saldo()" style="position: absolute;top: 0;right: 15px;"><i class="fa fa-credit-card"></i></a>
				</div>
				<div class="col-xs-12 col-md-4">
					Transacción: 
					<?php 
						echo form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',1,'id="transaccion"');
					?>
				</div>
				<div class="col-xs-12 col-md-4">
					Forma de Pago: 
					<?php 
						$formas = $this->db->get_where('formapago');								
					?>
					<select name="forma_pago" id="formapago" class="form-control chosen-select">
						<option value="">Seleccione una forma de pago</option>	
						<?php foreach($formas->result() as $f): ?>
							<option value="<?= $f->id ?>" data-porVenta="<?= $f->por_venta ?>"><?= $f->denominacion ?> <?= $f->por_venta ?>%</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-xs-12 col-md-4">
					Observaciones: 
					<input type="text" name="observaciones" id="observacion" class="form-control">
				</div>
				<div class="col-xs-12 col-md-4">
					Pedido: 
					<?php 
						echo form_dropdown('pedido',array('0'=>'NO','1'=>'SI'),0,'id="field-pedido" class="form-control"');
					?>
				</div>	
				<div class="col-xs-12 col-md-4">
					Pagado: 
					<?php 
						echo form_dropdown('pagado',array('0'=>'NO','1'=>'SI'),0,'id="field-pagado" class="form-control"');
					?>
				</div>	
			</div>
			<div class="col-xs-12 col-md-3" style="position: relative;">
				<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Venta: </span>
				<input type="text" name="total_venta" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px;">
			</div>

		</div>

	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-9" style="padding-right: 39px;">
		<div class="panel panel-default" style="border:0">
			<div style="height:208px; overflow-y: auto;">
				<table class="table table-bordered" id="ventaDescr">
					<thead>
						<tr>
							<th>Código</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>%Desc</th>
							<th>P.Desc</th>
							<th>Total</th>
							<th>Stock</th>
						</tr>
					</thead>
					<tbody>

						<tr id="productoVacio">
							<td>
								<a href="javascript:void(0)" class="rem" style="display:none;color:red">
									<i class="fa fa-times"></i>
								</a> 
								<span>&nbsp;</span>
							</td>
							<td>&nbsp;</td>
							<td><input name="cantidad" class="cantidad" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
							<td><input name="precio" class="precio" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>
							<td><input name="por_desc" class="por_desc" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
							<td><input name="precio_descuento" class="precio_descuento" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						
					</tbody>
				</table>
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cantidadProductos">4</span>					
					</div>
					<div class="col-xs-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;" autocomplete="off">
						<button style="position: absolute;top: 1px;right: 16px;padding: 1px;" class="btn btn-primary">Insertar</button>

						<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-3" style="padding-left: 5px;">
		<div class="panel panel-default">
			<div class="panel-heading">Resumen de venta</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-4">Pesos: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_pesos" value="300.000" readonly="" style="width: 100%;">
					</div>
					<div class="col-xs-12 col-md-4">Reales: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_reales" value="300.000" readonly="" style="width: 100%;">
					</div>
					<div class="col-xs-12 col-md-4">Dolares: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_dolares" value="300.000" readonly="" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>

		
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-9">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" onclick="nuevaVenta();" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nueva venta</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#procesar"><i class="fa fa-floppy-o"></i> Procesar venta</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" <?= empty($edit)?'disabled="true"':'' ?> style="color:#000 !important"><i class="fa fa-print"></i> Imprimir</button>
		  </div>
		</div>
	</div>
	<div class="col-xs-12 col-md-3" style="margin-top: -70px;">
		<div class="panel panel-default">
			<div class="panel-heading">Atajos</div>
			<div class="panel-body">
				<span>(ALT+C) <small>Enfocar busqueda por código</small></span><br/>
				<span>(ALT+I) <small>Mostrar busqueda avanzada</small></span><br/>
				<span>(ALT+P) <small>Procesar venta</small></span><br/>
				<span>(ALT+N) <small>Nueva venta</small></span><br/>
				<span>(ALT+B) <small>Consultar saldo del cliente</small></span><br/>
			</div>
		</div>
	</div>
</div>

<div id="procesar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
        	Procesar venta
        </h4>
      </div>
      <div class="col-modal-body">
      	<div class="row" style="margin-left: 0; margin-right: 0">
      		<div class="col-xs-12">
      			<div class="radio">
      			  <?php foreach($this->db->get('tipo_facturacion')->result() as $t): ?>
					  <label>
					    <input type="radio" data-emitefactura="<?= $t->emite_factura ?>" data-url="<?= $t->reporte ?>" class="tipoFacturacion" name="tipo_facturacion_id" id="optionsRadios1" value="<?= $t->id ?>">
					    <?= $t->denominacion ?>
					  </label>
				  <?php endforeach ?>
				  <span id="facturaD" style="display: none; font-weight:bold; position: absolute; right:0; top:0">#FACTURA: <span id="nroFactura"></span></span>
				</div>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-xs-12 col-md-4">
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Monto a pagar</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Pesos</td>
      						<td><input type="text" name="total_pesos" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td><input type="text" name="total_dolares" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td><input type="text" name="total_reales" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Guaranies</td>
      						<td><input type="text" name="total_venta" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      				</tbody>
      			</table>   
      			<div id="pago_total" style="padding: 0px 30px;margin: 0 0 31px;">Gs.: <span class="totalGs" style="font-size: 30px;font-weight: bold;">0</span></div>   			
      		</div>
      		<div class="col-xs-12 col-md-4 pagoEfectivo">
      			<div class="patternCredito"></div>
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Pago</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Pesos</td>
      						<td><input type="text" name="pago_pesos" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td><input type="text" name="pago_dolares" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td><input type="text" name="pago_reales" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Guaranies</td>
      						<td><input type="text" name="pago_guaranies" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Debito/Crédito</td>
      						<td><input type="text" name="total_debito" value="0" style="width: 100%;"></td>
      					</tr>
      				</tbody>
      			</table>
      		</div>
      		<div class="col-xs-12 col-md-4 pagoEfectivo">
      			<div class="patternCredito"></div>
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Vuelto</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Pesos</td>
      						<td><input type="text" name="vuelto_pesos" readonly="true" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td><input type="text" name="vuelto_dolares" readonly="true" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td><input type="text" name="vuelto_reales" readonly="true" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Guaranies</td>
      						<td><input style="color:red; width:100%;" type="text" name="vuelto" readonly="true" value="0"></td>
      					</tr>
      				</tbody>
      			</table>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-xs-12">
      			<div class="respuestaVenta"></div>
      		</div>
      	</div>		
      </div>
      <div class="modal-footer">
      	<button type="button" class="btnNueva btn btn-info" onclick="nuevaVenta()" style="display: none">Nueva venta</button>
        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>-->
        <button type="button" onclick="sendVenta()" class="btn btn-primary">Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="addCliente" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="insertar('maestras/clientes/insert',this,'.resultClienteAdd',function(data){setCliente(data)}); return false;">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Agregar cliente</h4>
	      </div>
	      <div class="panel-body">
	  		  <div class="row">
  		  		<div class="col-xs-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nombre</label>
				    <input type="text" class="form-control" name="nombres" placeholder="Nombre del cliente">
				  </div>
				</div>
				<div class="col-xs-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Celular</label>
				    <input type="text" class="form-control" name="celular1" placeholder="Celular del cliente">
				  </div>				
				</div>

				<div class="col-xs-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Apellido</label>
				    <input type="text" class="form-control" name="apellidos" placeholder="Apellido del cliente">
				  </div>
				</div>
				<div class="col-xs-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">#Documento</label>
				    <input type="text" class="form-control" name="nro_documento" placeholder="Documento del cliente">
				  </div>
				</div>

				<div class="col-xs-12 col-md-12">
					<div class="form-group">
					    <label for="exampleInputEmail1">Dirección</label>
					    <input type="text" class="form-control" name="direccion" placeholder="Dirección del cliente">
					  </div>
				</div>

				<div class="col-xs-12 col-md-12">
					<div class="resultClienteAdd"></div>
			    </div>
			</div>			  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<div id="saldo" class="modal fade" tabindex="-1" role="dialog">  
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Saldo del cliente</h4>
	      </div>
	      <div class="panel-body">
	  		  <h1 id="saldoTag" style="text-align: center">2.000GS</h1>		  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>	        
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<script>
<?php 
	if(!empty($edit)){
		$venta = $this->querys->getVenta($edit);
		if($venta){
			echo 'var editar = '.json_encode($venta).';';
		}else{
			echo 'var editar = undefined;';
		}
	}else{
		echo 'var editar = undefined;';
	}

	if(!empty($default)){		
		echo 'var precargo = '.json_encode($default).';';
	}else{
		echo 'var precargo = undefined;';
	}
?>
</script>
<script src="<?= base_url() ?>js/ventas.js?v=<?= time() ?>"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/ajax-chosen-jsonlist.js?v=1.3"></script>
<script>	
	<?php
		$ajustes = $this->ajustes;
	?>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= $ajustes->cod_balanza ?>;
	var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
	?>;
	var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
	var onsend = false;
	var venta = new PuntoDeVenta();	
	if(editar!=undefined){
		console.log(editar);
		venta.setDatos(editar);
	}

	if(precargo!=undefined){
		console.log(precargo);
		venta.setDatos(precargo);
		venta.datos.precargo = JSON.stringify(precargo);
	}
	

	function initDatos(){
		venta.datos.sucursal = '<?= $this->user->sucursal ?>';
		venta.datos.caja = '<?= $this->user->caja ?>';
		venta.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		venta.datos.fecha = '<?= date("Y-m-d H:i") ?>';	
		venta.datos.usuario = '<?= $this->user->id ?>';
		venta.datos.tipo_facturacion_id = '<?= $this->db->get('ajustes')->row()->tipo_facturacion_id ?>';		
	}
	initDatos();
	venta.initEvents();
	venta.updateFields();

	function imprimir(codigo){
		var url = $(".tipoFacturacion:checked").data('url');
        window.open('<?= base_url() ?>'+url+codigo);
	}

	function selCod(codigo){		
		venta.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
	}

	function nuevaVenta(){		
		venta.initVenta();
		initDatos();
		$(".respuestaVenta").html('').removeClass('alert alert-info alert-danger alert-success');		
		if($("#procesar").css('display')=='block'){
			$("#procesar").modal('toggle');
		}
		$(".btnNueva").hide();
		$("button").attr('disabled',false);
		$("#transaccion").val(1);
		$("#transaccion").chosen().trigger('liszt:updated');
		$.post(URI+'maestras/clientes/json_list',{   
            search_field:'id',     
            search_text:'1',
            operator:'where'
        },function(data){       
            data = JSON.parse(data);   
            venta.selectClient(data);
        });
		onsend = false;
	}

	
	function sendVenta(){
		if(typeof(editar)=='undefined'){
			if(!onsend){
				onsend = true;
				var datos =  JSON.parse(JSON.stringify(venta.datos));
				datos.empleados_id = <?= $this->user->id ?>;
				datos.productos = JSON.stringify(datos.productos);
				var accion = typeof(editar)=='undefined'?'insert':'update/'+editar.id;
				$("button").attr('disabled',true);
				insertar('movimientos/ventas/ventas/'+accion,datos,'.respuestaVenta',function(data){						
					var id = data.insert_primary_key;						
					var enlace = '';
					$(".respuestaVenta").removeClass('alert-info');
					imprimir(id);
					enlace = 'javascript:imprimir('+id+')';
					$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');
					$(".btnNueva").show();
					$(".btnNueva").attr('disabled',false);
					<?php if($ajustes->plan_credito==1): ?>
						if(venta.datos.transaccion==2){
							document.location.href="<?= base_url() ?>movimientos/creditos/creditos/"+id+'/add';						
						}
					<?php endif ?>
					onsend = false;
				},function(){
					onsend = false;
					$("button").attr('disabled',false);
				});
			}
		}else{
			alert('Edición no permitida');
		}
	}

	function setCliente(data){
		if(data.success){
			$(".resultClienteAdd").html('<div class="alert alert-success">Cliente añadido con éxito</div>');
			$.post(URI+'maestras/clientes/json_list',{   
	            search_field:'id',     
	            search_text:data.insert_primary_key,
	            operator:'where'
	        },function(data){       
	            data = JSON.parse(data);   
	            venta.selectClient(data);
	        });
		}
	}

	$(document).on('shown.bs.modal',"#procesar",function(){
		$("#procesar input[name='pago_guaranies']").focus();
	});

	function saldo(){
		$.post(base_url+'movimientos/ventas/saldo',{cliente:$("#cliente").val()},function(data){
			$("#saldoTag").html(data);
			$("#saldo").modal('toggle');
		});
	}
</script>