<form action="" method="post" onsubmit="insertarMarca(this); return false;">
	<div class="modal fade" id="marcas" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Añadir marcas</h4>
	            </div>
	            <div class="modal-body">                
	            	<div class="form-group" id="denominacion_field_box">
	                	<label for="field-marcas_nombre" id="marcas_nombre_display_as_box" style="width:100%">
	                		Nombre<span class="required">*</span>  :
	                	</label>
	                	<input id="field-marcas_nombre" name="marcas_nombre" class="form-control denominacion" type="text" value="" maxlength="40">
	                </div>
	           		<div id="report-error"></div>
	            </div>
	            <div class="modal-footer">
	            	<button type="submit" class="btn btn-success">Guardar</button>        
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

<script>
	function insertarMarca(obj){		
		$('#marcas #report-error').removeClass('alert alert-success alert-danger').html('');
		insertar('maestras/boxes/marcas/insert',obj,'',function(data){
			if(data.success){
				$('#marcas').modal('toggle');
				$.post('<?= base_url() ?>maestras/boxes/marcas/json_list',{per_page:1000},function(medidas){
					var opt = '';
					medidas = JSON.parse(medidas);
					for(var i in medidas){
						var selected = data.insert_primary_key==medidas[i].id?'selected="true"':'';
						opt+= '<option value="'+medidas[i].id+'" '+selected+'>'+medidas[i].marcas_nombre+'</option>';
					}
					$("select[name='marcas_id']").html(opt);
					$("select[name='marcas_id']").chosen().trigger('liszt:updated');
				});
			}else{				
				$('#marcas #report-error').addClass('alert alert-danger').html(data.error_message);
			}
		},function(data){$('#marcas #report-error').addClass('alert alert-danger').html(data.error_message);});
	}
</script>