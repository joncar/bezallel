<form action="" method="post" onsubmit="insertarCategoria(this); return false;">
	<div class="modal fade" id="categorias" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Añadir categoria</h4>
	            </div>
	            <div class="modal-body">                
	            	<div class="form-group" id="denominacion_field_box">
                    	<label for="field-denominacion" id="denominacion_display_as_box" style="width:100%">
                                Denominacion :
                        </label>
                        <input id="field-denominacion" name="denominacion" class="form-control denominacion" type="text" value="" maxlength="35">
                	</div>
                	<div class="form-group" id="control_vencimiento_field_box">
                    	<label for="field-control_vencimiento" id="control_vencimiento_display_as_box" style="width:100%">
                                Control vencimiento<span class="required">*</span>  :
                        </label>
                        <label>
                        <input id="field-control_vencimiento-true" name="control_vencimiento" class="ace" type="radio" value="1"><span class="lbl">Si</span></label>  <label><input id="field-control_vencimiento-true" name="control_vencimiento" class="ace" type="radio" value="0"><span class="lbl">No</span></label>
            		</div>
	           		<div id="report-error"></div>
	            </div>
	            <div class="modal-footer">
	            	<button type="submit" class="btn btn-success">Guardar</button>        
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

<script>
	function insertarCategoria(obj){
		$('#categorias #report-error').removeClass('alert alert-success alert-danger').html('');
		insertar('movimientos/productos/categorias/insert',obj,'',function(data){
			if(data.success){
				$('#categorias').modal('toggle');
				$.post('<?= base_url() ?>movimientos/productos/categorias/json_list',{per_page:1000},function(medidas){
					var opt = '';
					medidas = JSON.parse(medidas);
					for(var i in medidas){
						var selected = data.insert_primary_key==medidas[i].id?'selected="true"':'';
						opt+= '<option value="'+medidas[i].id+'" '+selected+'>'+medidas[i].denominacion+'</option>';
					}
					$("select[name='categoria_id']").html(opt);
					$("select[name='categoria_id']").chosen().trigger('liszt:updated');
				});
			}else{
				$('#categorias #report-error').addClass('alert alert-danger').html(data.error_message);
			}
		},function(data){$('#categorias #report-error').addClass('alert alert-danger').html(data.error_message);});
	}
</script>

<form action="" method="post" onsubmit="insertarSubCategoria(this); return false;">
	<div class="modal fade" id="subcategorias" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Añadir Subcategoria 1</h4>
	            </div>
	            <div class="modal-body">   
	            	<div class="form-group" id="denominacion_field_box">
                    	<label for="field-categoriaproducto" id="categoriaproducto_display_as_box" style="width:100%">
                                Categoria :
                        </label>
                        <input type="text" id="categoriaproducto_id" value="Seleccione una categoría en el formulario" readonly="" class="categoriaproducto_id form-control">
                        <input type="hidden" name="categoriaproducto_id" value="0">
                	</div>             
	            	<div class="form-group" id="denominacion_field_box">
                    	<label for="field-nombre_sub_categoria_producto" id="nombre_sub_categoria_producto_display_as_box" style="width:100%">
                                Nombre :
                        </label>
                        <input id="field-nombre_sub_categoria_producto" name="nombre_sub_categoria_producto" class="form-control nombre_sub_categoria_producto" type="text" value="" maxlength="35">
                	</div>
	           		<div id="report-error"></div>
	            </div>
	            <div class="modal-footer">
	            	<button type="submit" class="btn btn-success">Guardar</button>        
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>



<form action="" method="post" onsubmit="insertarSubSubCategoria(this); return false;">
	<div class="modal fade" id="subsubsubcategorias" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Añadir Subcategoria 2</h4>
	            </div>
	            <div class="modal-body">   
	            	<div class="form-group" id="denominacion_field_box">
                    	<label for="field-categoriaproducto" id="categoriaproducto_display_as_box" style="width:100%">
                                Categoria :
                        </label>
                        <input type="text" id="categoriaproducto_id" value="Seleccione una categoría en el formulario" readonly="" class="categoriaproducto_id form-control">                        
                	</div> 
                	<div class="form-group" id="denominacion_field_box">
                    	<label for="field-categoriaproducto" id="categoriaproducto_display_as_box" style="width:100%">
                                SubCategoria :
                        </label>
                        <input type="text" id="sub_categoria_producto_id" value="Seleccione una subcategoría en el formulario" readonly="" class="sub_categoria_producto_id form-control">
                        <input type="hidden" name="sub_categoria_producto_id" value="0">
                	</div>            
	            	<div class="form-group" id="denominacion_field_box">
                    	<label for="field-nombre_sub_sub_categoria" id="nombre_sub_sub_categoria_display_as_box" style="width:100%">
                                Nombre :
                        </label>
                        <input id="field-nombre_sub_sub_categoria" name="nombre_sub_sub_categoria" class="form-control nombre_sub_sub_categoria" type="text" value="" maxlength="35">
                	</div>
	           		<div id="report-error"></div>
	            </div>
	            <div class="modal-footer">
	            	<button type="submit" class="btn btn-success">Guardar</button>        
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

<script>
	function insertarSubCategoria(obj){
		$('div#subcategorias #report-error').removeClass('alert alert-success alert-danger').html('');
		insertar('movimientos/productos/sub_categoria_producto/insert',obj,'',function(data){
			if(data.success){
				$('div#subcategorias').modal('toggle');
				$.post('<?= base_url() ?>movimientos/productos/sub_categoria_producto/json_list',{per_page:1000},function(medidas){
					var opt = '';
					medidas = JSON.parse(medidas);
					for(var i in medidas){
						var selected = data.insert_primary_key==medidas[i].id?'selected="true"':'';
						opt+= '<option value="'+medidas[i].id+'" '+selected+'>'+medidas[i].nombre_sub_categoria_producto+'</option>';
					}
					$("#field-sub_categoria_producto_id").html(opt);
					$("#field-sub_categoria_producto_id").chosen().trigger('liszt:updated');
					$("#field-sub_categoria_producto_id").trigger('change');
				});
			}else{
				$('div#subcategorias #report-error').addClass('alert alert-danger').html(data.error_message);
			}
		},function(data){$('div#subcategorias #report-error').addClass('alert alert-danger').html(data.error_message);});
	}

	function insertarSubSubCategoria(obj){
		$('div#subsubsubcategorias #report-error').removeClass('alert alert-success alert-danger').html('');
		insertar('movimientos/productos/sub_sub_categoria/insert',obj,'',function(data){
			if(data.success){
				$('div#subsubsubcategorias').modal('toggle');
				$.post('<?= base_url() ?>movimientos/productos/sub_sub_categoria/json_list',{per_page:1000},function(medidas){
					var opt = '';
					medidas = JSON.parse(medidas);
					for(var i in medidas){
						var selected = data.insert_primary_key==medidas[i].id?'selected="true"':'';
						opt+= '<option value="'+medidas[i].id+'" '+selected+'>'+medidas[i].nombre_sub_sub_categoria+'</option>';
					}
					$("#field-sub_sub_categoria_id").html(opt);
					$("#field-sub_sub_categoria_id").chosen().trigger('liszt:updated');
					$("#field-sub_sub_categoria_id").trigger('change');
				});
			}else{
				$('div#subsubsubcategorias #report-error').addClass('alert alert-danger').html(data.error_message);
			}
		},function(data){$('div#subsubsubcategorias #report-error').addClass('alert alert-danger').html(data.error_message);});
	}
</script>