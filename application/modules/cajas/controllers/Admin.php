<?php

require_once APPPATH.'/controllers/Panel.php';    

class Admin extends Panel {

    function __construct() {
        parent::__construct();
    }
    
    public function timbrados($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $crud->set_relation('cajas_id','cajas','denominacion');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function cajas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->fields('sucursal', 'denominacion', 'serie');
        $crud->field_type('abierto', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->add_action('Cajas diarias','',base_url('cajas/admin/vercajadiaria').'/');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    public function vercajadiaria($x = '', $y = '') {
        $this->as['vercajadiaria'] = 'cajadiaria';
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('caja',$x);
        $crud->order_by('cajadiaria.id','DESC');
        $crud->unset_delete()
             ->unset_add()
             ->unset_edit()             
             ->unset_print()
             ->unset_export()
             ->field_type('abierto','true_false',array('0'=>'NO','1'=>'SI'))
             ->columns('timbrados_id','fecha_apertura','fecha_cierre','usuario','monto_inicial','total_ventas','total_contado','total_pago_clientes','total_credito','total_egreso','efectivoarendir','abierto');
        $crud->callback_column('monto_inicial',array($this,'formatear'))
             ->callback_column('total_ventas',array($this,'formatear'))
             ->callback_column('total_contado',array($this,'formatear'))
             ->callback_column('total_pago_clientes',array($this,'formatear'))
             ->callback_column('total_credito',array($this,'formatear'))
             ->callback_column('total_egreso',array($this,'formatear'))
             ->callback_column('efectivoarendir',array($this,'formatear'));
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->set_relation('caja', 'cajas', 'denominacion', array('abierto' => 0));
        $crud->set_relation('usuario', 'user', '{nombre} {apellido}');
        $output = $crud->render();
        $this->loadView($output);
    }

    function formatear($val){
        return number_format($val,0,',','.').' GS';
    }

    public function cajadiaria($x = '', $y = '', $z = '') {        
        $crud = parent::crud_function($x, $y);
        $crud->set_relation_dependency('caja', 'sucursal', 'sucursal');
        $crud->unset_delete();
        $crud->required_fields('monto_inicial', 'caja', 'sucursal');
        if (!empty($_SESSION['caja']) && $this->db->get_where('cajadiaria', array('caja' => $_SESSION['caja'], 'sucursal' => $_SESSION['sucursal'], 'abierto' => 1))->num_rows() > 0) {
            $crud->unset_add();
            $crud->where('cajadiaria.abierto', 1);
        }
        $crud->field_type('fecha_apertura', 'invisible')
                ->field_type('efectivoarendir', 'invisible')
                ->field_type('total_descuentos', 'invisible')
                ->field_type('total_debito', 'invisible')
                ->field_type('total_cheque', 'invisible')
                ->field_type('fecha_cierre', 'invisible')
                ->field_type('total_contado', 'invisible')
                ->field_type('total_pago_clientes', 'invisible')
                ->field_type('total_credito', 'invisible')
                ->field_type('total_egreso', 'invisible')
                ->field_type('total_efectivo', 'invisible')
                ->field_type('total_ventas', 'invisible')
                ->field_type('total_entrega_credito', 'invisible')
                ->field_type('sucursal', 'hidden', $_SESSION['sucursal'])
                ->field_type('caja', 'hidden', @$_SESSION['caja'])
                ->field_type('usuario', 'hidden', $_SESSION['user']);

        if ($crud->getParameters() == 'edit') {
            $crud->field_type('correlativo', 'input');
        }
        $crud->display_as('abierto', 'Tipo de apertura');
        if ($crud->getParameters() == 'add') {
            $crud->field_type('abierto', 'invisible');
        } else {
            $crud->field_type('abierto', 'true_false', array('0' => 'Cerrado', '1' => 'Abierto'));
        }

        if(is_numeric($y) && $this->db->get_where('cajadiaria',array('id'=>$y,'abierto'=>1))->num_rows()==0){
            redirect('cajas/admin/cajadiaria');
        }

        $crud->callback_add_field('correlativo', function($val) {
            get_instance()->db->order_by('correlativo', 'DESC');
            $aperturas = get_instance()->db->get_where('cajadiaria', array('sucursal' => $_SESSION['sucursal'], 'caja' => $_SESSION['caja']));
            $correlativo = $aperturas->num_rows() == 0 ? '1' : $aperturas->row()->correlativo;
            return form_input('correlativo', $correlativo, 'id="field-correlativo" class="form-control"');
        });

        $crud->callback_before_insert(function($post) {
            $post['abierto'] = '1';
            $post['total_efectivo'] = $post['monto_inicial'];
            $post['fecha_apertura'] = date("Y-m-d H:i:s");
            get_instance()->db->update('cajas', array('abierto' => 1), array('id' => $post['caja']));
            return $post;
        });

        $crud->callback_before_update(function($post, $id) {
            $p = (array) $this->db->get_where('cajadiaria', array('id' => $id))->row();
            if ($post['abierto'] == 0) {
                get_instance()->db->update('cajas', array('abierto' => 0), array('id' => $p['caja']));
                

                $egresos = $this->db->query('select SUM(monto) as total from gastos where cajadiaria = '.$id)->row()->total;

                $this->db->select('SUM(totalcondesc) as total');
                $this->db->join('ventas','ventas.id = ventadetalle.venta');
                $this->db->join('productos','productos.codigo = ventadetalle.producto');
                $contado = $this->db->get_where('ventadetalle',array('cajadiaria'=>$id,/*'no_caja'=>0,*/'status'=>0,'transaccion'=>1))->row()->total;

                $this->db->select('SUM(total_pagado) as total');
                $this->db->where('(anulado = 0 OR anulado IS NULL)',null,FALSE);
                $pagocliente = $this->db->get_where('pagocliente',array('cajadiaria'=>$id))->row()->total;

                $this->db->select('SUM(totalcondesc) as total');
                $this->db->join('ventas','ventas.id = ventadetalle.venta');
                $this->db->join('productos','productos.codigo = ventadetalle.producto');
                $credito = $this->db->get_where('ventadetalle',array('cajadiaria'=>$id,/*'no_caja'=>0,*/'status'=>0,'transaccion'=>2))->row()->total;

                $this->db->select('SUM(totalcondesc) as total');
                $this->db->join('ventas','ventas.id = ventadetalle.venta');
                $this->db->join('productos','productos.codigo = ventadetalle.producto');
                $ventas = $this->db->get_where('ventadetalle',array('cajadiaria'=>$id,/*'no_caja'=>0,*/'status'=>0))->row()->total;

                $this->db->select('sum(entrega_inicial) as total_entrega');
                $this->db->where('(creditos.anulado = 0 or creditos.anulado is null) and creditos.cajadiaria ='.$id,null,FALSE);
                $totalcreditos = $this->db->get('creditos');
                $totalcreditos = $totalcreditos->num_rows()>0?$totalcreditos->row()->total_entrega:0;

                $post['total_contado'] = empty($contado)?0:$contado;
                $post['total_pago_clientes'] = empty($pagocliente)?0:$pagocliente;
                $post['total_credito'] = empty($credito)?0:$credito;              
                $post['total_ventas'] = empty($ventas)?0:$ventas;              
                $post['total_descuentos'] = 0;   
                $post['total_entrega_credito'] = $totalcreditos;
                $post['total_egreso'] = empty($egresos)?0:$egresos;
                $post['efectivoarendir'] = $p['monto_inicial']+$post['total_contado']+$post['total_pago_clientes']-$post['total_egreso']+$totalcreditos;
                $post['fecha_cierre'] = date("Y-m-d H:i:s");
                unset($_SESSION['cajadiaria']);
            }
            return $post;
        });
        if ($crud->getParameters() == 'list' || $crud->getParameters() == 'ajax_list' || $crud->getParameters() == 'success') {
            $crud->set_relation('sucursal', 'sucursales', 'denominacion');
            $crud->set_relation('caja', 'cajas', 'denominacion', array('abierto' => 0));
            $crud->set_relation('usuario', 'user', '{nombre} {apellido}');
        }
        $crud->callback_add_field('fecha_apertura', function($val, $row) {
            return '<input id="field-fecha_apertura" name="fecha_apertura" type="text" value="' . date("d/m/Y h:i:s") . '" maxlength="19" class="datetime-input">';
        });
        $crud->columns('sucursal','caja','fecha_apertura','monto_inicial','correlativo','efectivoarendir','abierto');

        $crud->callback_column('efectivoarendir',array($this,'formatear_read_cierre'));
        $crud->callback_column('monto_inicial',array($this,'formatear_read_cierre'));
        if($crud->getParameters()=='read'){
            $crud->callback_field('efectivoarendir',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_credito',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_contado',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_egreso',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_pago_clientes',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_ventas',array($this,'formatear_read_cierre'));
            $crud->callback_field('monto_inicial',array($this,'formatear_read_cierre'));
        }
        $crud->order_by('id','desc');
        //$crud->add_action('Imprimir ventas','',base_url('reportes/detalle_ventas').'/');
        $output = $crud->render();
        $this->loadView($output);
    }

    public function formatear_read_cierre($val){
        return number_format($val,0,',','.').' Gs';
    }
    
    public function gastos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y); 
        $crud->field_type('cajadiaria','hidden',$_SESSION['cajadiaria']);
        $crud->field_type('user_created','hidden',$_SESSION['user']);
        $crud->field_type('user_modified','hidden',$_SESSION['user']);
        $crud->field_type('fecha_created','hidden',date("Y-m-d"));
        $crud->field_type('fecha_modified','hidden',date("Y-m-d"));
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function pagocliente($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
             ->unset_delete();
        $crud->set_relation('clientes_id', 'clientes', 'nombres');
        $crud->field_type('anulado', 'true_false', array(0 => 'No', 1 => 'Si'));
        $crud->where('sucursal', $_SESSION['sucursal'])
             ->where('caja', $_SESSION['caja']);
        //$crud->columns('cliente', 'recibo', 'fecha', 'totalefectivo', 'totaldebito', 'totalnotacredito', 'totalcheque', 'totalcredigo', 'totalpagado', 'saldo', 'anulado');
        $crud->unset_columns('cliente');
        $crud->callback_column('saldo', function($val, $row) {
            get_instance()->db->select('SUM(total_venta) as total');
            return number_format(get_instance()->db->get_where('ventas', array('cliente' => $row->cliente, 'transaccion' => 2, 'status' => 0))->row()->total, 2, ',', '.');
        });
        $crud->order_by('pagocliente.id','DESC');
        $output = $crud->render();
        $output->crud = 'ventas';
        if ($x == 'add')
            $output->output = $this->load->view('pagosclientes', array(), TRUE);
        if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $venta = $this->db->get_where('pagocliente', array('id' => $y));
            $detalles = $this->db->get_where('pagoclientesfactura', array('pagocliente' => $venta->row()->id));
            $output->output = $this->load->view('pagosclientes', array('pagocliente' => $venta->row(), 'detalles' => $detalles), TRUE);
        }
        if ($x == 'imprimir' || $x == 'imprimirticket') {
            if (!empty($y) && is_numeric($y)) {
                $this->db->select('pagocliente.*,clientes.id as cliente, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                $this->db->join('clientes', 'clientes.id = pagocliente.clientes_id','left');
                $this->db->join('cajas', 'cajas.id = pagocliente.caja','left');
                $this->db->join('sucursales', 'sucursales.id = pagocliente.sucursal','left');
                $venta = $this->db->get_where('pagocliente', array('pagocliente.id' => $y));
                if ($venta->num_rows() > 0) {
                    $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                    $this->db->select('SUM(total_venta) as total');
                    $venta->row()->saldo = $this->db->get_where('ventas', array('cliente' => $venta->row()->cliente, 'transaccion' => 2, 'status' => 0))->row()->total;
                    if ($venta->row()->saldo > 0) {
                        $total = 0;
                        $this->db->select('pagoclientesfactura.*');
                        $this->db->join('pagoclientesfactura', 'pagoclientesfactura.pagocliente = pagocliente.id');
                        foreach ($this->db->get_where('pagocliente', array('cliente' => $venta->row()->cliente))->result() as $p) {
                            $total+=$p->total + $p->nota_credito;
                        }
                        $venta->row()->saldo-=$total;
                    } else {
                        $venta->row()->saldo = 0;
                    }
                    $output = $this->load->view('reportes/imprimirTicketPago', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                    if ($this->router->fetch_class() == 'admin') {
                        echo $output;
                    } else {
                        return $output;
                    }
                } else {
                    echo "Factura no encontrada";
                }
            } else {
                echo 'Factura no encontrada';
            }
        } else {
             $this->loadView($output);
        }
    }
    
    public function pagoproveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
                ->unset_delete();
        $output = $crud->render();
        $output->crud = 'ventas';
        if ($x == 'add')
            $output->output = $this->load->view('pagoproveedor', array(), TRUE);
        if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $venta = $this->db->get_where('pagoproveedores', array('id' => $y));
            $detalles = $this->db->get_where('pagoproveedor_detalles', array('pagoproveedor' => $venta->row()->id));
            $output->output = $this->load->view('pagoproveedor', array('pagoproveedor' => $venta->row(), 'detalles' => $detalles), TRUE);
        }
        $this->loadView($output);
    }

    public function saldos($x = '', $y = '') {
        $this->as = array('saldos' => 'ventas');
        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
                ->unset_delete()
                ->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_export()
                ->unset_print();
        $crud->set_relation('cliente', 'clientes', '{nro_documento}|{nombres}|{apellidos}|{celular}');
        $crud->where('ventas.transaccion', 2);
        $crud->where('ventas.status != ', -1);
        $crud->group_by('ventas.cliente');
        $crud->columns('nro_documento', 'nombres', 'apellidos', 'celular','ultima_compra', 'total_compra', 'ultimo_pago', 'total_pagos','saldo');
        $crud->callback_column('nro_documento', function($val, $row) {
            return explode('|', $row->s4983a0ab)[0];
        });
        $crud->callback_column('nombres', function($val, $row) {
            return explode('|', $row->s4983a0ab)[1];
        });
        $crud->callback_column('apellidos', function($val, $row) {
            return explode('|', $row->s4983a0ab)[2];
        });
        $crud->callback_column('celular', function($val, $row) {
            return explode('|', $row->s4983a0ab)[3];
        });

        $crud->callback_column('ultima_compra', function($val, $row) {
            $this->total = $this->db->query("SELECT
                clientes.nro_documento,
                clientes.nombres,
                clientes.apellidos,
                ifnull(clientes.celular,'Falta_cargar') as celular,
                ifnull(max(date(ventas.fecha)),0) as ultima_compra,
                ifnull(SUM(ventadetalle.totalcondesc),0) as total_compra,
                ifnull(pagos.ultimo_pago,0) as ultimo_pago,
                ifnull(pagos.total_pagos,0) as total_pagos,
                ifnull(SUM(ventadetalle.totalcondesc),0)- ifnull(pagos.total_pagos,0) as saldo
                FROM ventas
                INNER JOIN clientes ON clientes.id = ventas.cliente
                INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
                LEFT JOIN(
                SELECT 
                clientes.id as clienteid,
                max(date(pagocliente.fecha)) as ultimo_pago,
                sum(pagocliente.total_pagado) as total_pagos
                FROM pagocliente 
                INNER JOIN clientes on clientes.id = pagocliente.clientes_id
                WHERE pagocliente.anulado = 0 or pagocliente.anulado is null
                GROUP BY pagocliente.clientes_id
                ) as pagos on pagos.clienteid =ventas.cliente
                WHERE ventas.transaccion = 2 and ventas.status = 0 AND clientes.id = ".$row->cliente."
                GROUP BY ventas.cliente");            
            return $this->total->num_rows()>0?$this->total->row()->ultima_compra:0;
        })->callback_column('total_compra',function($val,$row){
             return $this->total->num_rows()>0?$this->total->row()->total_compra:0;
          })->callback_column('ultimo_pago',function($val,$row){
             return $this->total->num_rows()>0?$this->total->row()->ultimo_pago:0;
          })->callback_column('total_pagos',function($val,$row){
             return $this->db->query('select get_total_pagado('.$row->cliente.') as saldo')->row()->saldo;
          })->callback_column('saldo',function($val,$row){
             return $this->db->query('select get_saldo('.$row->cliente.') as saldo')->row()->saldo;
          });       
        $crud->add_action('Abonar deuda', '', base_url('cajas/admin/abonarDeuda/') . '/');        
        $output = $crud->render();
        $output->title = 'Saldos';
        $this->loadView($output);
    }
    
    public function actualizardescuentos(){
        if(!empty($_POST)){
            $this->form_validation->set_rules('porcentaje','Porcentaje','required|numeric');
            $this->form_validation->set_rules('proveedores','Proveedor','required|numeric');
            $this->form_validation->set_rules('categoriaproducto','Categoria del producto','required|numeric');
            if($this->form_validation->run()){
                $where = array('categoria_id'=>$this->input->post('categoriaproducto'),'proveedor_id'=>$this->input->post('proveedores'));
                if(!empty($_POST['paises'])){
                    $where['nacionalidad_id'] = $_POST['paises'];
                }
                $this->db->update('productos',array('descmax'=>$this->input->post('porcentaje')),$where);
                $msj = $this->success('Los datos se han actualizado con éxito');
            }else{
                $msj = $this->error($this->form_validation->error_string());
            }
        }else{
            $msj = '';
        }
        $this->loadView(array('view'=>'actualizardescuentos','msj'=>$msj));
    }
    
    public function saldos_proveedores($x = '', $y = '') {
            $this->as = array('saldos_proveedores' => 'compras');
            $crud = parent::crud_function($x, $y);
            $crud->unset_read()
                    ->unset_delete()
                    ->unset_add()
                    ->unset_edit()
                    ->unset_read()
                    ->unset_export()
                    ->unset_print();           
            $crud->set_subject('Pago a Proveedores');
            $crud->where('compras.status != ', -1);
            $crud->group_by('compras.proveedor');
            $crud->columns('sucursal', 'proveedor', 'total_pagos', 'total_compra', 'ultima_compra', 'ultimo_pago', 'saldo');
            $crud->set_relation('sucursal','sucursales','denominacion');
            $crud->set_relation('proveedor','proveedores','denominacion');
            
            $crud->callback_column('total_compra', function($val, $row) {
                get_instance()->db->select('SUM(compras.total_compra) as total_compra');
                $p = get_instance()->db->get_where('compras', array('compras.proveedor' => $row->proveedor,'compras.status >='=>0,'transaccion'=>2));
                return $p->row()->total_compra == null ? (string) 0 : $p->row()->total_compra;
            });

            $crud->callback_column('total_pagos', function($val, $row) {
                get_instance()->db->select('SUM(pagoproveedores.total_abonado) as total_pagos');
                $p = get_instance()->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $row->proveedor));
                ($p->num_rows()>0 && $p->row()->total_pagos==null)?$p->row()->total_pagos = 0:$p->row()->total_pagos;
                return $p->num_rows() == 0 ? (string) '0' : (string) $p->row()->total_pagos;
            });

            $crud->callback_column('ultima_compra', function($val, $row) {
                get_instance()->db->select('compras.fecha');                
                get_instance()->db->order_by('id', 'DESC');
                $p = get_instance()->db->get_where('compras', array('compras.proveedor' => $row->proveedor,'compras.status >='=>0,'transaccion'=>2));
                return $p->num_rows()>0?date("d-m-Y H:i:s", strtotime($p->row()->fecha)):'N/A';
            });

            $crud->callback_column('ultimo_pago', function($val, $row) {
                get_instance()->db->select('pagoproveedores.fecha');
                $p = get_instance()->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $row->proveedor,'pagoproveedores.anulado !='=>1));
                return $p->num_rows() == 0 ? (string) 'N/A' : date("d-m-Y H:i:s", strtotime($p->row()->fecha));
            });
            $crud->add_action('Abonar deuda', '', base_url('cajas/admin/abonarDeudaProveedor/') . '/');
            $crud->callback_column('saldo', function($val, $row) {
                return number_format($row->total_compra - $row->total_pagos, 0, ',', '.');
            });
            $output = $crud->render();
            $output->title = 'Saldos';
            $this->loadView($output);
        }
        
        
        public function abonarDeuda($x = '') {
            $this->validate_caja();
            if (!empty($x) && is_numeric($x)) {
                $this->db->select('ventas.*,clientes.id as clienteid, clientes.nombres as clientename, clientes.apellidos as apellidos');
                $this->db->join('clientes', 'clientes.id = ventas.cliente');
                $cliente = $this->db->get_where('ventas', array('ventas.id' => $x));
                if ($cliente->num_rows() > 0) {
                    $nombre = $cliente->row()->clientename . ' ' . $cliente->row()->apellidos;                    
                    $clienteid = $cliente->row()->clienteid;
                    $cliente = $cliente->row()->cliente;
                    //Total venta
                    $this->db->select('SUM(ventas.total_venta) as total_venta');
                    $this->db->where('transaccion', '2');
                    $this->db->where('ventas.status !=',-1);
                    $total_venta = $this->db->get_where('ventas', array('ventas.cliente' => $cliente));
                    $totalVentas = $total_venta->row()->total_venta == null ? 0 : $total_venta->row()->total_venta;
                    //Total pagos
                    /*$this->db->select('SUM(pagocliente.total_pagado) as total_pagos');
                    $total_pago = $this->db->get_where('pagocliente', array('pagocliente.clientes_id' => $cliente));
                    $totalPagos = $total_pago->num_rows() == 0 ? 0 : $total_pago->row()->total_pagos;
                    $saldo = $totalVentas - $totalPagos;*/
                    $totalPagos = 0;
                    $saldo = $this->db->query('select get_saldo('.$cliente.') as saldo')->row()->saldo;

                    //saldo notas de credito
                    /*$this->db->select('SUM(notas_credito_cliente.total_monto) as notas');
                    $this->db->join('ventas', 'ventas.id = notas_credito_cliente.venta');
                    $total_nota = $this->db->get_where('notas_credito_cliente', array('ventas.cliente' => $cliente));
                    $notas = $total_nota->num_rows() == 0 ? 0 : $total_nota->row()->notas;*/
                    $notas = 0;
                    //Se resta a las notas de credito las facturas a las cuales se les ha pagado con notas de credito
                    /*$this->db->select('SUM(totalnotacredito) as notasusadas');
                    $notasusadas = $this->db->get_where('pagocliente', array('cliente' => $cliente));
                    $notasusadas = $notasusadas->num_rows() == 0 ? 0 : $notasusadas->row()->notasusadas;*/
                    $notasusadas = 0;
                    $notas = $notas - $notasusadas;
                    $msj = '';
                    if (!empty($_POST)) {
                        $this->form_validation->set_rules('monto', 'Monto', 'required|numeric');
                        if ($this->form_validation->run()) {
                            //Traemos saldos
                            if ($saldo < $notas + $_POST['monto']) {
                                $msj = $this->error('El monto ingresado excede de la cantidad total de la deuda');
                            } else {
                                $detalles = array();
                                $notasdisponible = $notas;
                                $saldodisponible = $_POST['monto'];
                                //Traemos las facturas que se deben
                                $facturas = $this->db->get_where('ventas', array('ventas.cliente' => $cliente, 'transaccion' => 2));
                                foreach ($facturas->result() as $factura) {
                                    //Cuanto se debe de esta factura
                                    $this->db->order_by('id', 'DESC');
                                    $deuda = $this->db->get_where('pagoclientesfactura', array('venta' => $factura->id));
                                    if ($deuda->num_rows() > 0) {
                                        $deuda = $deuda->row()->monto - ($deuda->row()->total + $deuda->row()->nota_credito);
                                    } else {
                                        $deuda = $factura->total_venta;
                                    }

                                    if ($deuda > 0 && ($saldodisponible > 0 || $notasdisponible > 0)) {
                                        $pago = array('venta' => $factura->id, 'nro_factura' => $factura->nro_factura, 'monto' => $deuda);
                                        if ($notasdisponible > 0) {
                                            $pago['nota_credito'] = $notasdisponible > $deuda ? $deuda : $notasdisponible;
                                            $notasdisponible-=$deuda;
                                            $deuda -= $pago['nota_credito'];
                                        } else {
                                            $pago['nota_credito'] = 0;
                                        }
                                        if ($saldodisponible > 0) {
                                            $pago['total'] = $saldodisponible > $deuda ? $deuda : $saldodisponible;
                                            $saldodisponible-= $pago['total'];
                                        }
                                        array_push($detalles, $pago);
                                    }
                                }
                                //Se carga la cabecera del pago
                                $this->db->insert('pagocliente', array(
                                    'clientes_id' => $cliente,
                                    'fecha' => date("Y-m-d H:i:s"),
                                    /*'totalefectivo' => $_POST['monto'],
                                    'totalnotacredito' => $notas,*/                                                                        
                                    'formapago_id' =>$_POST['formapago_id'],
                                    'total_pagado' => $notas + $_POST['monto'],
                                    'sucursal' => $_SESSION['sucursal'],
                                    'caja' => $_SESSION['caja'],
                                    'cajadiaria'=>$_SESSION['cajadiaria'],
                                    'user' => $_SESSION['user']
                                ));
                                $idpago = $this->db->insert_id();
                                foreach ($detalles as $d) {
                                    $d['pagocliente'] = $idpago;
                                    $this->db->insert('pagoclientesfactura', $d);
                                }
                                $msj = $this->success('El pago se ha realizado satisfactoriamente <a href="javascript:imprimir(' . $idpago . ')">Imprimir</a> | <a href="javascript:imprimirTicket(' . $idpago . ')">imprimir Ticket</a>');
                                redirect('cajas/admin/abonarDeuda/'.$x.'?pago='.$idpago);
                            }
                        }
                    }

                    $output = $this->load->view('abonarDeuda',array( 'msj' => $msj, 'totalPagos' => $totalPagos, 'cliente' => $nombre, 'totalVentas' => $totalVentas, 'saldo' => $saldo, 'notas' => $notas,'clienteid'=>$clienteid),TRUE);
                    $this->loadView(array('view' => 'panel','crud'=>'user','output'=>$output));
                } else {
                    header("Location:" . base_url('cajero/saldos'));
                }
            } else {
                header("Location:" . base_url('cajero/saldos'));
            }
        }

        public function abonarDeudaProveedor($x = '') {
            if (!empty($x) && is_numeric($x)) {
                $this->db->select('compras.*, proveedores.denominacion as nombre');
                $this->db->join('proveedores', 'proveedores.id = compras.proveedor');
                $proveedor = $this->db->get_where('compras', array('compras.id' => $x));
                if ($proveedor->num_rows() > 0) {
                    $nombre = $proveedor->row()->nombre;
                    $proveedor = $proveedor->row()->proveedor;
                    //Total venta
                    $this->db->select('SUM(compras.total_compra) as total_compra');
                    $this->db->where('transaccion', '2');
                    $total_venta = $this->db->get_where('compras', array('compras.proveedor' => $proveedor));
                    $totalVentas = $total_venta->row()->total_compra == null ? 0 : $total_venta->row()->total_compra;
                    //Total pagos
                    $this->db->select('SUM(pagoproveedores.total_abonado) as total_pagos');
                    $total_pago = $this->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $proveedor));
                    $totalPagos = $total_pago->num_rows() == 0 ? 0 : $total_pago->row()->total_pagos;
                    $saldo = $totalVentas - $totalPagos;
                    //saldo notas de credito
                    $this->db->select('SUM(notas_credito.total_monto) as notas');
                    $this->db->join('compras', 'compras.id = notas_credito.compra');
                    $total_nota = $this->db->get_where('notas_credito', array('compras.proveedor' => $proveedor));
                    $notas = $total_nota->num_rows() == 0 ? 0 : $total_nota->row()->notas;
                    //Se resta a las notas de credito las facturas a las cuales se les ha pagado con notas de credito
                    $this->db->select('SUM(totalnotacredito) as notasusadas');
                    $notasusadas = $this->db->get_where('pagoproveedores', array('proveedor' => $proveedor));
                    $notasusadas = $notasusadas->num_rows() == 0 ? 0 : $notasusadas->row()->notasusadas;
                    $notas = $notas - $notasusadas;
                    $msj = '';
                    if (!empty($_POST)) {
                        $this->form_validation->set_rules('monto', 'Monto', 'required|numeric');
                        if ($this->form_validation->run()) {
                            //Traemos saldos
                            if ($saldo < $notas + $_POST['monto']) {
                                $msj = $this->error('El monto ingresado excede de la cantidad total de la deuda');
                            } else {
                                $detalles = array();
                                $notasdisponible = $notas;
                                $saldodisponible = $_POST['monto'];
                                //Traemos las facturas que se deben
                                $facturas = $this->db->get_where('compras', array('compras.proveedor' => $proveedor, 'transaccion' => 2,'status >='=>0));
                                foreach ($facturas->result() as $factura) {
                                    //Cuanto se debe de esta factura
                                    $this->db->order_by('id', 'DESC');
                                    $deuda = $this->db->get_where('pagoproveedor_detalles', array('compra' => $factura->id));
                                    if ($deuda->num_rows() > 0) {
                                        $deuda = $deuda->row()->monto - ($deuda->row()->total + $deuda->row()->nota_credito);
                                    } else {
                                        $deuda = $factura->total_compra;
                                    }

                                    if ($deuda > 0 && ($saldodisponible > 0 || $notasdisponible > 0)) {
                                        $pago = array('compra' => $factura->id, 'nro_factura' => $factura->nro_factura, 'monto' => $deuda);
                                        if ($notasdisponible > 0) {
                                            $pago['nota_credito'] = $notasdisponible > $deuda ? $deuda : $notasdisponible;
                                            $notasdisponible-=$deuda;
                                            $deuda -= $pago['nota_credito'];
                                        } else {
                                            $pago['nota_credito'] = 0;
                                        }
                                        if ($saldodisponible > 0) {
                                            $pago['total'] = $saldodisponible > $deuda ? $deuda : $saldodisponible;
                                            $saldodisponible-= $pago['total'];
                                        }
                                        array_push($detalles, $pago);
                                    }
                                }
                                //Se carga la cabecera del pago
                                $this->db->insert('pagoproveedores', array(
                                    'proveedor' => $proveedor,
                                    'fecha' => date("Y-m-d H:i:s"),
                                    'totalefectivo' => $_POST['monto'],
                                    'totalnotacredito' => $notas,
                                    'total_abonado' => $notas + $_POST['monto'],
                                    'forma_pago'=>$_POST['forma_pago'],
                                    'comprobante'=>$_POST['comprobante'],
                                    'nro_recibo'=>$_POST['nro_recibo'],
                                    'sucursal' => $_SESSION['sucursal'],
                                    'caja' => $_SESSION['caja'],
                                    'user' => $_SESSION['user']
                                ));
                                $idpago = $this->db->insert_id();
                                foreach ($detalles as $d) {
                                    $d['pagoproveedor'] = $idpago;
                                    $this->db->insert('pagoproveedor_detalles', $d);
                                }
                                $msj = $this->success('El pago se ha realizado satisfactoriamente <a href="javascript:imprimir(' . $idpago . ')">Imprimir</a> | <a href="javascript:imprimirTicket(' . $idpago . ')">imprimir Ticket</a>');
                            }
                        }
                    }


                    $this->loadView(array('view' => 'abonarDeudaProveedor', 'msj' => $msj, 'totalPagos' => $totalPagos, 'cliente' => $nombre, 'totalVentas' => $totalVentas, 'saldo' => $saldo, 'notas' => $notas));
                } else {
                    header("Location:" . base_url('cajero/saldos'));
                }
            } else {
                header("Location:" . base_url('cajero/saldos'));
            }
        /* Cruds */
    }

}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
