<?php

require_once APPPATH.'/controllers/Panel.php';    

class Maestras extends Panel {

    function __construct() {
        parent::__construct();             
    }
    
    public function paises($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function ciudades($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }
    
    public function proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('tipo_proveedor', 'tipo_proveedores', 'denominacion');
        $crud->required_fields('denominacion', 'ruc', 'direccion', 'ciudad', 'pais_id', 'telefonofijo', 'telefax', 'celular', 'email');
        $crud->unset_fields('created', 'modified');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        $crud->columns('denominacion', 'tipo_proveedor', 'ruc', 'telefonofijo', 'celular', 'Observacion');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function sucursales($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('ciudad_id', 'ciudades', 'denominacion');
        $crud->set_relation_dependency('ciudad_id', 'pais_id', 'pais');
        $crud->field_type('principal', 'true_false', array('0' => 'No', '1' => 'Si'));

        $crud->callback_before_insert(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $crud->callback_before_update(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function clientes($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y);        
        $crud->field_type('tipo_doc', 'hidden','')            
            ->set_relation('pais','paises','denominacion')
            ->set_relation('ciudad','ciudades','denominacion')
            ->set_relation('barrio','barrios','denominacion')                      
            ->field_type('celular2', 'hidden','')
            ->field_type('celular3', 'hidden','')
            ->field_type('email', 'hidden','')            
            ->field_type('foto','image',array('path'=>'img/clientes','width'=>'50px','height'=>'50px'));        
        $crud->columns('id','nombres','apellidos','nro_documento','celular','barrio','ciudad','mayorista','descuentos');
        $crud->required_fields('nombres','apellidos');
        $crud->set_rules('email', 'Email', 'valid_email');
        $crud->callback_before_update(function($post){
            if(empty($_POST['mayorista'])){
                $post['mayorista'] = 0;
            }
            if(empty($_POST['descuentos'])){
                $post['descuentos'] = 0;
            }
            return $post;
        });
        if ($x == 'add' || $x == 'insert' || $x == 'insert_validation') {
            $crud->set_rules('nro_documento', 'Cedula', 'required|is_unique[clientes.nro_documento]');
        }
        $crud->unset_fields('created', 'modified');
        if (!empty($x) && !empty($y) && $y == 'json') {
            $crud->unset_back_to_list();            
        }
        if($x=='json_list'){
            $crud->limit(10000);
        }        
        $output = $crud->render();
        $output->crud = 'clientes';
        if (!empty($x) && !empty($y) && $y == 'json') {
            $output->json = 'Activo';
            $output->view = 'json';
        }
        if($crud->getParameters(TRUE)!='update' || $crud->getParameters(TRUE)!='insert'){
            $crud->set_relation_dependency('ciudad','pais','pais')            
                     ->set_relation_dependency('barrio','ciudad','ciudad');
        }
        if($crud->getParameters(FALSE)=='add' || $crud->getParameters(FALSE)=='edit'){            
            $edit = (is_numeric($y))?$edit = $this->db->get_where('clientes',array('id'=>$y)):'';
            $output->output = $this->load->view('clientes',array('o'=>$output->output,'edit'=>$edit),TRUE);
        }
        $this->loadView($output);
    }
    
    public function monedas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('primario', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'user';
        $this->loadView($output);
    }
    
    public function motivo_salida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function motivo_entrada($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function tipo_proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }
    
    public function cuentas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();
        $this->loadView($output);
    }  
    
    function condiciones(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Condiciones de pago');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Condiciones de pago';
            $this->loadView($crud);
        }
        function estados(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Estados de ocupacion');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Estados de ocupacion';
            $this->loadView($crud);
        }
        
        function empleados(){
            $crud = $this->crud_function('','');            
            $crud->set_relation('user_id','user','{nombre} {apellido}');
            $crud->set_subject('Empleado');
            $crud->display_as('user_id','Usuario');
            $crud->unset_delete();
            $crud = $crud->render();            
            $crud->title = 'Empleados';
            $this->loadView($crud);
        }
        
        function tipo_pedidos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('tipo de pedidos');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Administrar de tipos de pedidos';
            $this->loadView($crud);
        }

        function sub_categoria_producto(){
            $crud = $this->crud_function('',''); 
            $crud->columns('id','nombre_sub_categoria_producto');           
            $crud->unset_delete();
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function sub_sub_categoria(){
            $crud = $this->crud_function('','');  
            $crud->columns('id','nombre_sub_sub_categoria');                     
            $crud->unset_delete();
            $crud = $crud->render();            
            $this->loadView($crud);
        }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
