<script type="text/javascript">
var ajax_relation_url = '<?= base_url() ?>maestras/clientes/ajax_relation';
</script>
<script type="text/javascript">
var base_page_url = '<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/chosen/loading.gif';
</script>
<script type="text/javascript">
$(document).ready(function() {$(document).on('change','#field-pais',function(e) {if($(this).val()!=''){e.stopPropagation();var selectedValue = $('#field-pais').val();$.post('ajax_extension/ciudad/pais/'+encodeURI(selectedValue.replace(/\//g,'_agsl_')), {}, function(data) {var $el = $('#field-ciudad');var newOptions = data;$el.empty();$el.append($('<option></option>').attr('value', '').text(''));$.each(newOptions, function(key, value) {$el.append($('<option></option>').attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));});$el.chosen().trigger('liszt:updated');},'json');$('#field-ciudad').change();}});});$(document).ready(function(){$(document).on('change','#field-ciudad',function(e) {if($(this).val()!=''){e.stopPropagation();var selectedValue = $('#field-ciudad').val();$.post('ajax_extension/barrio/ciudad/'+encodeURI(selectedValue.replace(/\//g,'_agsl_')), {}, function(data){var $el = $('#field-barrio');var newOptions = data;$el.empty();$el.append($('<option></option>').attr('value', '').text(''));$.each(newOptions, function(key, value) {$el.append($('<option></option>').attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));});$el.chosen().trigger('liszt:updated');},'json');$('#field-barrio').change();}});});                        
</script>
<?php 
	if(empty($edit) || $edit->num_rows()>0): 
	$edit = !empty($edit)?$edit->row():'';
	$accion = empty($edit)?'insert':'update/'.$edit->id;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>
<script src="<?= base_url() ?>js/morris.js"></script>                                
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
<link rel="stylesheet" href="<?= base_url() ?>css/morris.css">
<form action="" onsubmit="insertar('maestras/clientes/<?= $accion ?>',this,'.responseDiv'); return false;" method="post">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Datos del cliente</h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class='form-group' id="foto_field_box" style="height:130px; overflow:hidden">
					            <div style="position:relative; display:inline-block;">
			                        <div class="slim" id="slim-foto"          
			                             data-service="<?= base_url('maestras/clientes/cropper/foto/') ?>"
			                             data-post="input, output, actions"
			                             data-size="120,120"
			                             data-instant-edit="true"
			                             data-push="true"
			                             data-did-upload="imageSlimUpload"
			                             data-download="true"
			                             data-will-save="addValueSlim"
			                             data-label="Subir foto"
			                             data-meta-name="foto"                         
			                             data-force-size="120,120"  style="width:200px; height:120px;">			                             
			                             <input type="file" id="slim-foto"/>                        
			                             <input type="hidden" data-val="<?= @$edit->foto ?>" name="foto" value="<?= @$edit->foto ?>" id="field-foto">
			                             <img id="image-foto" src="<?= base_url().'img/clientes/'.@$edit->foto ?>" style="width:120px; height:120px;">
			                        </div>
			                    </div>
					        </div>
					        <div class='form-group' id="empresas_id_field_box">
                    			<label for='field-empresas_id' id="empresas_id_display_as_box" style="width:100%">
                                	Empresa
                        		</label>
                        		<?= form_dropdown_from_query('empresas_id','empresas','id','denominacion',@$edit->empresas_id,'id="field-empresas_id"',TRUE) ?>
                			</div>
					        <div class='form-group' id="limite_credito_field_box">
                    			<label for='field-limite_credito' id="limite_credito_display_as_box" style="width:100%">
                                	Límite de crédito
                        		</label>
                        		<input id='field-limite_credito' name='limite_credito' type='text' value='' class='form-control'/>
                			</div>
                			<div class='form-group' id="mayorista_field_box">
                    			<label for='field-mayorista' id="mayorista_display_as_box" style="width:100%">
                                	<input id='field-mayorista' name='mayorista' type='checkbox' value='1' <?= @$edit->mayorista==1?'checked':'' ?>/> Mayorista
                        		</label>

                        		<label for='field-descuentos' id="descuentos_display_as_box" style="width:100%">
                                	<input id='field-descuentos' name='descuentos' type='checkbox' value='1' <?= @$edit->descuentos==1?'checked':'' ?>/> Habilitar Descuentos
                        		</label>                        		
                			</div>
				        </div>
				        <div class="col-xs-12 col-md-6">
				        	<div class='form-group' id="nombres_field_box">
			                    <label for='field-nombres' id="nombres_display_as_box" style="width:100%">
                            		Nombres<span class='required'>*</span>  :
                            	</label>
                            	<input id='field-nombres' name='nombres'  class='form-control nombres' type='text' value="<?= @$edit->nombres ?>" />
			                </div>
			                <div class='form-group' id="apellidos_field_box">
			                    <label for='field-apellidos' id="apellidos_display_as_box" style="width:100%">
                            		Apellidos<span class='required'>*</span>  :
                            	</label>
                            	<input id='field-apellidos' name='apellidos'  class='form-control nombres' type='text' value="<?= @$edit->apellidos ?>" />
			                </div>
			                <div class='form-group' id="nro_documento_field_box">
			                    <label for='field-nro_documento' id="nro_documento_display_as_box" style="width:100%">
                            		#documento<span class='required'>*</span>  :
                            	</label>
                            	<input id='field-nro_documento' name='nro_documento'  class='form-control nombres' type='text' value="<?= @$edit->nro_documento ?>" />
			                </div>
			                <div class='form-group' id="fecha_nac_field_box">
                    			<label for='field-fecha_nac' id="fecha_nac_display_as_box" style="width:100%">
                                	F.Nacimiento:
                        		</label>
                        		<input id='field-fecha_nac' name='fecha_nac' type='text' value='' class='datepicker-input form-control' placeholder='dd/mm/yyyy' value="<?= @date("d/m/Y",strtotime($edit->fecha_nac)) ?>" />
                			</div>
                			<div class='form-group' id="sexo_field_box">
                    			<label for='field-sexo' id="sexo_display_as_box" style="width:100%">
                                	Sexo
                        		</label>
                        		<?= form_dropdown('sexo',array(''=>'Seleccione un sexo','M'=>'Masculino','F'=>'Femenino'),@$edit->sexo,'id="field-sexo" class="form-control"') ?>
                			</div>
				        </div>
			        </div>

				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Ubicación</h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class='form-group' id="pais_field_box">
			                    <label for='field-pais' id="pais_display_as_box" style="width:100%">
		                    		País:
		                    	</label>
		                    	<?= form_dropdown_from_query('pais','paises','id','denominacion',@$edit->pais,'id="field-pais"',TRUE,'pais chosen-select-ajax') ?>		                    	
			                </div>
			                <div class='form-group' id="barrio_field_box">
			                    <label for='field-barrio' id="barrio_display_as_box" style="width:100%">
		                    		Barrio:
		                    	</label>
		                    	<?= form_dropdown_from_query('barrio','barrios','id','denominacion',@$edit->barrio,'id="field-barrio"',TRUE,'barrio chosen-select-ajax') ?>		                    	
			                </div>
	                	</div>
						<div class="col-xs-12 col-md-6">
							<div class='form-group' id="ciudad_field_box">
			                    <label for='field-ciudad' id="ciudad_display_as_box" style="width:100%">
		                    		Ciudad:
		                    	</label>
		                    	<?= form_dropdown_from_query('ciudad','ciudades','id','denominacion',@$edit->ciudad,'id="field-ciudad"',TRUE,'ciudad chosen-select-ajax') ?>		                    	
			                </div>
			                <div class='form-group' id="direccion_field_box">
			                    <label for='field-direccion' id="direccion_display_as_box" style="width:100%">
		                    		Zona:
		                    	</label>
		                    	<input id='field-direccion' name='direccion'  class='form-control nombres' type='text' value="<?= @$edit->direccion ?>" />
			                </div>
						</div>

					</div>
				</div>
			</div> <!--- End ubicacion -->

			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Contacto</h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class='form-group' id="email_field_box">
			                    <label for='field-email' id="email_display_as_box" style="width:100%">
		                    		Email:
		                    	</label>
		                    	<input id='field-email' name='email'  class='form-control nombres' type='email' value="<?= @$edit->email ?>" />
			                </div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class='form-group' id="celular_field_box">
			                    <label for='field-celular' id="celular_display_as_box" style="width:100%">
		                    		Celular:
		                    	</label>
		                    	<input id='field-celular' name='celular'  class='form-control celular' type='text' value="<?= @$edit->celular ?>" />
			                </div>
						</div>
					</div>
				</div>
			</div><!--- EnD contacto --->


		</div>
	</div>


<div class="row">
	<div class="col-xs-12 col-md-12">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">		  
		  <div class="btn-group" role="group">
		    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#procesar">Guardar cliente</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" <?= empty($edit)?'disabled':'data-target="#masdetalles" data-toggle="modal"' ?>>Más datos</button>
		  </div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xs-12">
            <div class="responseDiv"></div>
    </div>
</div>
</form>


<?php if(!empty($edit)): ?>
<div class="modal fade" id="masdetalles" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Más detalles</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-xs-12 col-md-6">        		
        		<table class="table table-bordered">
        			<thead>
        				<tr>
        					<th colspan="3">
        						Últimas 5 ventas
        					</th>
        				</tr>
        				<tr>
        					<th>#venta</th>
        					<th>Fecha</th>        					
        					<th>Total</th>
        				</tr>
        			</thead>
        			<tbody>
        				<?php foreach($this->elements->ventas(array('cliente'=>$edit->id),5,array('id','DESC'))->result() as $v): ?>
	        				<tr>
	        					<th><?= $v->id ?></th>
	        					<th><?= $v->fecha ?></th>	        					
	        					<th><?= number_format($v->total_venta,2,',','.'); ?></th>
	        				</tr>
        				<?php endforeach ?>
        			</tbody>
        		</table>
        	</div>
        	<div class="col-xs-12 col-md-6">        		
        		<table class="table table-bordered">
        			<thead>
        				<tr>
        					<th colspan="5">Últimos 5 pagos</th>
        				</tr>
        				<tr>
        					<th>#Pago</th>
        					<th>Fecha</th>        					
        					<th>Total</th>
        				</tr>
        			</thead>
        			<tbody>
        				<?php foreach($this->elements->pagocliente(array('clientes_id'=>$edit->id),5,array('id','DESC'))->result() as $v): ?>
	        				<tr>
	        					<th><?= $v->id ?></th>
	        					<th><?= $c->fecha ?></th>	        					
	        					<th><?= number_format($v->total,2,',','.'); ?></th>
	        				</tr>
        				<?php endforeach ?>
        			</tbody>
        		</table>
        	</div>
        </div>
        <div class="row">
    		<div class="col-xs-12 col-md-6">
    			<h4>Ventas por mes</h4>  
    			<div id="ventasmes"></div> 
    		</div>
    		<div class="col-xs-12 col-md-6">
    			<h4>Utilidad mensual</h4>
    			<div id="ventasSemana">
    				
    			</div>
    		</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<script>
	$("#masdetalles").on('shown.bs.modal',function(){		
		Morris.Bar({
		  element: 'ventasmes',
		  data: [
		  	<?php 
		  		for($i=1;$i<=12;$i++): 
		  		$val = $this->db->query("SELECT COUNT(ventas.id) as val FROM ventas WHERE ventas.cliente = '".$edit->id."' AND MONTH(ventas.fecha) = '".$i."' AND YEAR(ventas.fecha) = '".date("Y")."'")->row()->val;
		  	?>
		    	{ y:'<?= meses_short($i) ?>', a:<?= $val ?>},
			<?php endfor ?>		    
		  ],
		  xkey: 'y',
		  ykeys: ['a'],
		  labels: ['Meses 2019']
		});

		Morris.Bar({
		  element: 'ventasSemana',
		  data: [
		  	<?php 
		  		for($i=1;$i<=12;$i++): 
		  		$val = $this->db->query("SELECT SUM(ventadetalle.totalcondesc) - SUM(ventadetalle.preciocosto) as val FROM ventadetalle INNER JOIN ventas ON ventas.id = ventadetalle.venta WHERE ventas.cliente = '".$edit->id."' AND MONTH(ventas.fecha) = '".$i."' AND YEAR(ventas.fecha) = '".date("Y")."'")->row()->val;
		  	?>
		    	{ y:'<?= meses_short($i) ?>', a:<?= !empty($val)?$val:0 ?>},
			<?php endfor ?>		    
		  ],
		  xkey: 'y',
		  ykeys: ['a'],
		  labels: ['Meses 2019']
		});
	});
</script>
<?php endif ?>


<?php else: ?>
	Ocurrio un error al procesar
<?php endif ?>