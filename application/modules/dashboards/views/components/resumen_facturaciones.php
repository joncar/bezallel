<?php
    $dia = $this->db->query("SELECT 
        format(COALESCE(sum(ventadetalle.totalcondesc),0),0,'de_DE') as totalventa 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        WHERE ventas.status = 0 and date(ventas.fecha) ='".date("Y-m-d")."'")->row()->totalventa;

    $mes = $this->db->query("SELECT 
        format(COALESCE(sum(ventadetalle.totalcondesc),0),0,'de_DE') as totalventa 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        WHERE ventas.status = 0 and MONTH(ventas.fecha) ='".date("m")."'")->row()->totalventa;

    $year = $this->db->query("SELECT 
        format(COALESCE(sum(ventadetalle.totalcondesc),0),0,'de_DE') as totalventa 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        WHERE ventas.status = 0 and YEAR(ventas.fecha) ='".date("Y")."'")->row()->totalventa;
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-clone"></i> Resumen de facturaciones</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">
                        <div class="row" style="margin-left: 0; margin-right: 0">
                            <div class="col-sm-6 col-md-4" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Por día</span>
    							      <div class="caption">
    							        <h4><?= $dia ?> Gs.</h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>

    						<div class="col-sm-6 col-md-4" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Por mes</span>
    							      <div class="caption">
    							        <h4><?= $mes ?> Gs.</h4>       
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-4" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Por año</span>
    							      <div class="caption">
    							        <h4><?= $year ?> Gs.</h4>       
    							      </div>
    							    </div>
    							</a>
    						</div>
                        </div>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
