<div id="pagoPorFechaContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT
        sum(t.Enero) as Enero,
        sum(t.Febrero) as Febrero,
        sum(t.Marzo) as Marzo,
        sum(t.Abril) as Abril,
        sum(t.Mayo) as Mayo,
        sum(t.Junio) as Junio,
        sum(t.Julio) as Julio,
        sum(t.Agosto) as Agosto,
        sum(t.Setiembre) as Setiembre,
        sum(t.Octubre) as Octubre,
        sum(t.Noviembre) as Noviembre,
        sum(t.Diciembre) as Diciembre
        FROM(
        SELECT
        if(month(pagocliente.fecha)=1,pagocliente.total_pagado,0) as Enero,
        if(month(pagocliente.fecha)=2,pagocliente.total_pagado,0) as Febrero,
        if(month(pagocliente.fecha)=3,pagocliente.total_pagado,0) as Marzo,
        if(month(pagocliente.fecha)=4,pagocliente.total_pagado,0) as Abril,
        if(month(pagocliente.fecha)=5,pagocliente.total_pagado,0) as Mayo,
        if(month(pagocliente.fecha)=6,pagocliente.total_pagado,0) as Junio,
        if(month(pagocliente.fecha)=7,pagocliente.total_pagado,0) as Julio,
        if(month(pagocliente.fecha)=8,pagocliente.total_pagado,0) as Agosto,
        if(month(pagocliente.fecha)=9,pagocliente.total_pagado,0) as Setiembre,
        if(month(pagocliente.fecha)=10,pagocliente.total_pagado,0) as Octubre,
        if(month(pagocliente.fecha)=11,pagocliente.total_pagado,0) as Noviembre,
        if(month(pagocliente.fecha)=12,pagocliente.total_pagado,0) as Diciembre
        FROM pagocliente
        WHERE pagocliente.anulado = 0) AS t
    ")->row();
?>

<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-bar-chart"></i> Resumen de pagos por mes (<?= $year ?>)</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año</b></a>
                            </li>              
                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <li>
                                <a href="javascript:changeYearpagoPorFecha(<?= $i ?>)"><?= $i ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
                        <div id="pagoPorFecha"></div>
                    </div>
                </div>
            </div>
</div>

<script>
    Morris.Bar({
      element: 'pagoPorFecha',
      data:<?php 
        $data = array();
        foreach($resumen as $n=>$v){
            $data[] = array('y'=>$n,'a'=>$v);
        }
        echo json_encode($data);
      ?>,
      xkey: 'y',
      ykeys: ['a'],
      labels: [''],
      hoverCallback: function (index, options, content, row) {
          return row.y+': '+currencyFormat(row.a);
      }
    });

    function changeYearpagoPorFecha(y){
        $.post('dashboards/refresh/pago_por_fecha',{year:y},function(data){
            $("#pagoPorFechaContent").html(data);
        });
    }
</script>
</div>