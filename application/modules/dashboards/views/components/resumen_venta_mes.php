<div id="resumenVentaMesContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT 
        round(sum(ventames.Enero),0) as ENE, 
        round(sum(ventames.Febrero),0) as FEB, 
        round(sum(ventames.Marzo),0) as MAR, 
        round(sum(ventames.Abril),0) as ABR, 
        round(sum(ventames.Mayo),0) as MAY, 
        round(sum(ventames.Junio),0) as JUN, 
        round(sum(ventames.Julio),0) as JUL, 
        round(sum(ventames.Agosto),0) as AGO, 
        round(sum(ventames.Setiembre),0) as SEP, 
        round(sum(ventames.Octubre),0) as OCT, 
        round(sum(ventames.Noviembre),0) as NOV, 
        round(sum(ventames.Diciembre),0) as DIC 
        FROM (
        SELECT 
        date_format(ventas.fecha,'%m') as Mes, 
        date_format(ventas.fecha,'%Y') as Anho, 
        If(date_format(ventas.fecha,'%m') = 01,ventad.total_detalle,0) as Enero, 
        If(date_format(ventas.fecha,'%m') = 02,ventad.total_detalle,0) as Febrero, 
        If(date_format(ventas.fecha,'%m') = 03,ventad.total_detalle,0) as Marzo, 
        If(date_format(ventas.fecha,'%m') = 04,ventad.total_detalle,0) as Abril, 
        If(date_format(ventas.fecha,'%m') = 05,ventad.total_detalle,0) as Mayo, 
        If(date_format(ventas.fecha,'%m') = 06,ventad.total_detalle,0) as Junio, 
        If(date_format(ventas.fecha,'%m') = 07,ventad.total_detalle,0) as Julio, 
        If(date_format(ventas.fecha,'%m') = 08,ventad.total_detalle,0) as Agosto, 
        If(date_format(ventas.fecha,'%m') = 09,ventad.total_detalle,0) as Setiembre, 
        If(date_format(ventas.fecha,'%m') = 10,ventad.total_detalle,0) as Octubre, 
        If(date_format(ventas.fecha,'%m') = 11,ventad.total_detalle,0) as Noviembre, 
        If(date_format(ventas.fecha,'%m') = 12,ventad.total_detalle,0) as Diciembre,

        ventad.total_detalle as total 
        FROM ventas 
        INNER JOIN clientes on clientes.id = ventas.cliente 
        INNER JOIN (
        SELECT ventadetalle.venta as venta, sum(ventadetalle.totalcondesc) as total_detalle 
        FROM ventadetalle

        inner join productos on productos.codigo = ventadetalle.producto 
        GROUP BY ventadetalle.venta 
        ) as ventad on ventad.venta = ventas.id 
        WHERE ventas.status = 0 and year(ventas.fecha) = '".$year."') as ventames 
        GROUP by ventames.Anho
    ")->row();
?>

<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-bar-chart"></i> Resumen de ventas por mes (<?= $year ?>)</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año</b></a>
                            </li>              
                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <li>
                                <a href="javascript:changeYearResumenVentaMes(<?= $i ?>)"><?= $i ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
						<div id="resumenVentaMes"></div>
                    </div>
                </div>
            </div>
</div>

<script>
	Morris.Bar({
	  element: 'resumenVentaMes',
      data:<?php 
        $data = array();
        foreach($resumen as $n=>$v){
            $data[] = array('y'=>$n,'a'=>$v);
        }
        echo json_encode($data);
      ?>,
	  xkey: 'y',
	  ykeys: ['a'],
	  labels: [''],
      hoverCallback: function (index, options, content, row) {
          return row.y+': '+currencyFormat(row.a);
      }
	});

    function changeYearResumenVentaMes(y){
        $.post('dashboards/refresh/resumen_venta_mes',{year:y},function(data){
            $("#resumenVentaMesContent").html(data);
        });
    }
</script>
</div>