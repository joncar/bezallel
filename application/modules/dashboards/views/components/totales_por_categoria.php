<div id="totalesPorCategoriaContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $qry = $this->db->query("
        SELECT 
        categoriaproducto.denominacion as categoria, 
        TRUNCATE(SUM(ventadetalle.totalcondesc),0) as total 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        INNER JOIN productos on productos.codigo = ventadetalle.producto 
        INNER JOIN clientes on clientes.id = ventas.cliente 
        INNER JOIN categoriaproducto on categoriaproducto.id = productos.categoria_id 
        WHERE ventas.status = 0 and YEAR(ventas.fecha) = '".$year."' group by productos.categoria_id order by sum(ventadetalle.totalcondesc) desc
    ");
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-pie-chart"></i> Totales por categoría</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año</b></a>
                            </li>              
                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <li>
                                <a href="javascript:changeYeartotalesPorCategoria(<?= $i ?>)"><?= $i ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
						<div id="totalesPorCategoria"></div>
                    </div>
                </div>
            </div>
</div>
<script>
    var data = <?php
        $data = array();
        foreach($qry->result() as $q){
            $data[] = array('label'=>$q->categoria,'value'=>$q->total);
        }
        echo json_encode($data);
    ?>;
	Morris.Donut({
	  element: 'totalesPorCategoria',
	  data: data
	});

    function changeYeartotalesPorCategoria(y){
        $.post('dashboards/refresh/totales_por_categoria',{year:y},function(data){
            $("#totalesPorCategoriaContent").html(data);
        });
    }
</script>
</div>