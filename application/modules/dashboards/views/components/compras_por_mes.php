<div id="comprasPorMesContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT
		sum(t.Enero) as ENE,
		sum(t.Febrero) as FEB,
		sum(t.Marzo) as MAR,
		sum(t.Abril) as ABR,
		sum(t.Mayo) as MAY,
		sum(t.Junio) as JUN,
		sum(t.Julio) as JUL,
		sum(t.Agosto) as AGO,
		sum(t.Setiembre) as SEP,
		sum(t.Octubre) as OCT,
		sum(t.Noviembre) as NOV,
		sum(t.Diciembre) as DIC
		FROM(
		SELECT
		if(month(compras.fecha)=1,cd.total,0) as Enero,
		if(month(compras.fecha)=2,cd.total,0) as Febrero,
		if(month(compras.fecha)=3,cd.total,0) as Marzo,
		if(month(compras.fecha)=4,cd.total,0) as Abril,
		if(month(compras.fecha)=5,cd.total,0) as Mayo,
		if(month(compras.fecha)=6,cd.total,0) as Junio,
		if(month(compras.fecha)=7,cd.total,0) as Julio,
		if(month(compras.fecha)=8,cd.total,0) as Agosto,
		if(month(compras.fecha)=9,cd.total,0) as Setiembre,
		if(month(compras.fecha)=10,cd.total,0) as Octubre,
		if(month(compras.fecha)=11,cd.total,0) as Noviembre,
		if(month(compras.fecha)=12,cd.total,0) as Diciembre
		FROM compras
		INNER JOIN compradetalles cd on cd.compra = compras.id
		WHERE compras.status = 0 and year(compras.fecha)=".$year.") as t
    ")->row();
?>

<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-bar-chart"></i> Resumen de compras por mes (<?= $year ?>)</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año</b></a>
                            </li>              
                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <li>
                                <a href="javascript:changeYearcomprasPorMes(<?= $i ?>)"><?= $i ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
						<div id="comprasPorMes"></div>
                    </div>
                </div>
            </div>
</div>

<script>
	Morris.Bar({
	  element: 'comprasPorMes',
      data:<?php 
        $data = array();
        foreach($resumen as $n=>$v){
            $data[] = array('y'=>$n,'a'=>$v);
        }
        echo json_encode($data);
      ?>,
	  xkey: 'y',
	  ykeys: ['a'],
	  labels: [''],
      hoverCallback: function (index, options, content, row) {
          return row.y+': '+currencyFormat(row.a);
      }
	});

    function changeYearcomprasPorMes(y){
        $.post('dashboards/refresh/compras_por_mes',{year:y},function(data){
            $("#comprasPorMesContent").html(data);
        });
    }
</script>
</div>