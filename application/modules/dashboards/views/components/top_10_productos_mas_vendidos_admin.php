<?php
    $qry = $this->db->query("
        SELECT 
		productos.nombre_comercial as Producto,
		format(sum(ventadetalle.cantidad),0,'de_DE') as Cantidad,
		format(sum(ventadetalle.totalcondesc),0,'de_DE') as Total
		FROM ventas 
		INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
		INNER JOIN productos on productos.codigo = ventadetalle.producto 
		INNER JOIN clientes on clientes.id = ventas.cliente 
		INNER JOIN categoriaproducto on categoriaproducto.id = productos.categoria_id 
		WHERE ventas.status = 0
		group by productos.id  
		ORDER BY sum(ventadetalle.cantidad)  DESC
		LIMIT 10
    ");
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-list"></i> Top 10 productos más vendidos</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main no-padding">
                        <?php sqlToHtml($qry); ?>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
