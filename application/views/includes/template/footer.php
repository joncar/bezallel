<footer class="clearfix ng-scope">
    <div class="footer-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <h6>MENSAJEROS URBANOS</h6>
                        <a href="/caracteristicas"><p>Características</p></a>
                        <a href="/precios"><p>Precios</p></a>
                        <a href="/frequent-questions"><p>¿Cómo funciona?</p></a>
                    </div>
                    <div class="col-sm-6">
                        <h6>LINKS DE INTERÉS</h6>
                        <a target="_blank" href="http://murbanos.co/"><p>Conoce Mensajeros Urbanos</p></a>
                        <a href="/unete-a-la-red"><p>Únete a la red</p></a>
                        <a target="_blank" href="http://mensajerosurbanos.com/contactanos"><p>Contáctanos</p></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>Copyright @ 2014 MensajerosUrbanos.com | Company by MU TEAM SAS</p>
                        <a href="/politica-privacidad">
                            <p>Política de Privacidad</p>
                        </a>
                        <a href="/terminos-condiciones" id="terms">
                            <p> Términos y Condiciones</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-5">
                        <h6>RECURSOS</h6>
                        <a rel="me nofollow" target="_blank" href="http://mensajerosurbanos.com/blog/">
                            <p>Blog</p>
                        </a>
                    </div>
                    <div class="col-sm-7">
                        <h6>Conéctate con Mensajeros Urbanos</h6>
                        <ul class="social-media-buttons">
                            <li>
                                <a rel="nofollow" target="_blank" href="https://www.linkedin.com/company/mensajeros-urbanos">
                                    <img alt="Encuéntranos en LinkedIN" src="<?= base_url('img').'/' ?>03-LinkedIn.png">
                                </a>
                            </li>
                            <li>
                                <a rel="nofollow" target="_blank" href="http://twitter.com/mensajerosurban">
                                    <img alt="Encuéntranos en Twitter" src="<?= base_url('img').'/' ?>02-Twitter.png">
                                </a>
                            </li>
                            <li>
                                <a rel="nofollow" target="_blank" href="http://facebook.com/MensajerosUrbanos">
                                    <img alt="Encuéntranos en Facebook" src="<?= base_url('img').'/' ?>01-Facebook.png">
                                </a>
                            </li>
                        </ul>

                        <div class="row" id="badges">
                            <h2>
                                disponible también en:
                            </h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <img alt="Descarga nuestra aplicación" src="<?= base_url('img/').'/' ?>play.png">
                                </div>
                                <div class="col-sm-6">
                                    <img alt="Descarga nuestra aplicación" src="<?= base_url('img/').'/' ?>itunes.png">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <a href="https://mixpanel.com/f/partner">
                                <img class="mixpanel" alt="Mobile Analytics" src="//cdn.mxpnl.com/site_media/images/partner/badge_light.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>