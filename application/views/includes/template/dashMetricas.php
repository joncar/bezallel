<div class="row" style="margin:40px;">
    <div class="space-6"></div>

    <div class="col-sm-12 infobox-container">
        
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          
          <?php 
            $this->db->order_by('orden','ASC');
            foreach($this->db->get('tipo_contador')->result() as $t): 
          ?>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <?= $t->nombre ?>
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <?php foreach($this->db->get_where('contadores',array('tipo_contador_id'=>$t->id))->result() as $c): ?>
                        <div class="infobox infobox-green">
                            <div class="infobox-icon">
                                <i class="ace-icon <?= $c->icono ?>"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">
                                    <?php 
                                        $query = $c->query;
                                        $query = str_replace('$_sucursales_id',$this->user->sucursal,$query);
                                        $query = str_replace('$_user_id',$this->user->id,$query);
                                        $query = str_replace('$_caja_id',$this->user->caja,$query);
                                        $query = str_replace('$_cajadiaria_id',$this->user->cajadiaria,$query);                                        
                                        echo $this->db->query(str_replace('|selec|','SELECT',$query))->row()->total 
                                    ?>
                                </span>
                                <div class="infobox-content"><?= $c->titulo ?></div>
                            </div>
                        </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          

          


        



        
    </div> 
    
    <div class="vspace-12-sm"></div>

    <div class="col-sm-5">
        
    </div><!-- /.col -->
</div>


