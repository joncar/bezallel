<div style="height:350px; overflow: auto;">
<table id="advancedsearchtable" class="table table-bordered">
    <?php $this->load->view('searchProduct2'); ?>
</table>
</div>
<div style="margin-top:40px;">
    <table id="advancedsearchtable2" class="table table-bordered">
        <tr>
            <th><input type="text" name="codigo" id="codigo" placeholder="Codigo" class="form-control"></th>
            <th><input type="text" name="nombre" id="nombre" placeholder="Nombre Comercial" class="form-control"></th>
            <th><input type="text" name="nombre2" id="nombre2" placeholder="Nombre Genérico" class="form-control"></th>
        </tr>
    </table>
</div>
<div align="right" style="margin-top:10px;">
    <?php
        if(!empty($sucursal))
        $cantidad = $this->db->get_where('productosucursal',array('sucursal'=>$sucursal));
        else
        $cantidad = $this->db->get('productos');
    ?>
    Pagina: <input type="number" class="page" value="<?= !empty($page)?$page:'1' ?>"> de <?= round($cantidad->num_rows()/100,0) ?>
</div>
<script>
    $(document).on('change','.page',function(){
       $.post('<?= base_url('json/searchProduct') ?>',{page:$(this).val()},function(data){
           $("#advancedsearchtable").html(data);
       }); 
    });
    
    $(document).on('change','#advancedsearchtable2 input',function(){
        <?php $suc = isset($sucursal)?$sucursal:''; ?>
        $.post('<?= base_url('json/searchProduct/'.$suc) ?>',{codigo:$("#advancedsearchtable2 #codigo").val(),nombre:$("#advancedsearchtable2 #nombre").val(),nombre2:$("#advancedsearchtable2 #nombre2").val()},function(data){
           $("#advancedsearchtable").html(data);
        }); 
    });
</script>