<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Cliente: </label>
              <div class="col-sm-8" id="cliente_div">
                  <?php $sel = empty($venta)?0:$venta->cliente; ?>
                  <?= form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',$sel,'id="cliente"') ?>
              </div>
            </div>
        </div> 
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Transacción: </label>
              <div class="col-sm-8">
                  <?php $sel = empty($venta)?0:$venta->transaccion; ?>
                  <?= form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',$sel); ?>
              </div>
            </div>
        </div> 
        <div class="col-xs-2">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" value="<?= empty($venta)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($venta->fecha)) ?>" id="fecha" class="form-control" readonly>
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Nro. Factura: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="nro_factura" name="nro_factura" readonly value="<?= empty($venta)?$this->querys->get_nro_factura():$venta->nro_factura; ?>">
              </div>
            </div>
        </div> 
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Forma Pago: </label>
              <div class="col-sm-8">
                  <?php $sel = empty($venta)?0:$venta->forma_pago; ?>
                  <?= form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',$sel) ?> 
              </div>
            </div>
        </div> 
        <div class="col-xs-8">
            <div class="form-group">
              <label for="proveedor" class="col-sm-1 control-label">Observ.: </label>
              <div class="col-sm-11">
                  <input type="text" value="<?= empty($venta)?'':$venta->observacion; ?>" class="form-control" name="observacion" id="observacion" placeholder="Observacion">
              </div>
            </div>
        </div>         
    </div>  
    </div>
    <div class="row">
        <div class="col-xs-12">Detalle de ventas <a href="javascript:advancesearch()" class="btn btn-default">Busqueda avanzada de productos</a> <a target="_new" href="<?= base_url('movimientos/productos/inventario') ?>" class="btn btn-default">Inventariado</a></div>
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2" id="detall">
                <thead>
                    <tr>
                        <th style="width:18%">Código</th>
                        <th style="width:31%">Nombre artículo</th>
                        <th style="width:10%">Lote</th>                        
                        <th style="width:5%">Cant.</th>
                        <th style="width:7%">Precio Venta</th>
                        <th style="width:7%">% DESC</th>
                        <th style="width:5%">Precio Desc.</th>                        
                        <th style="width:10%">Total</th>    
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $filas = 0; ?>
                    <?php if(!empty($venta)): ?>
                        <?php $filas = $detalles->num_rows(); ?>
                        <?php foreach($detalles->result() as $n=>$d): ?>
                            <tr>
                                <td><input name="codigo_<?= $n ?>" data-name="codigo" type="text" class="form-control codigo" placeholder="Codigo" value="<?= $d->producto ?>"></td>
                                <td><input name="producto_<?= $n ?>" data-name="producto" type="text" class="form-control producto" readonly placeholder="Nombre" value="<?= $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->nombre_comercial ?>"></td>
                                <td><input name="lote_<?= $n ?>" data-name="lote" type="text" value='<?= $d->lote ?>'  class="form-control lote" placeholder="Lote"></td>
                                <td><input name="cantidad_<?= $n ?>" data-name="cantidad" type="text" class="form-control cantidad" placeholder="Cantidad"  value="<?= $d->cantidad ?>"></td>
                                <td><input name='precio_venta_<?= $n ?>' data-name="precio_venta" type="text" class="form-control precio_venta" value="<?= $d->precioventa ?>" placeholder="Precio venta"></td>
                                <td><input name='por_desc_<?= $n ?>' data-name="por_desc" type="text" class="form-control por_venta" placeholder="% Descuento" value="<?= $d->pordesc ?>"></td>
                                <td><input name='precio_desc_<?= $n ?>' data-name="precio_desc" type="text" class="form-control precio_desc" placeholder="Precio descuentos" value="<?= $d->precioventadesc ?>"></td>                                                
                                <td><input name='total_<?= $n ?>' data-name="total" type="text" class="form-control total" placeholder="Total" value="<?= $d->totalcondesc ?>">
                                    <input type="hidden" name="ivah_<?= $n ?>" data-name="ivah" id="ivah" class="ivah" value="<?= $d->iva ?>">
                                    <input type="hidden" name="disponible_<?= $n ?>" data-name="disponible" id="disponible" class="disponible" value="0">
                                    <input type="hidden" name="inventariable_<?= $n ?>" data-name="disponible" id="inventariable" class="inventariable" value="<?= $d->inventariable ?>">
                                </td>                                                
                                <td><p align="center">
                                        <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                        <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                                    </p></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <tr>
                        <td><input name="codigo_<?= $filas ?>" data-name="codigo" type="text" class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto_<?= $filas ?>" data-name="producto" type="text" class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><select name="lote_<?= $filas ?>" data-name="lote" class="lote form-control" id="lote"></select></td>                      
                        <td><input name="cantidad_<?= $filas ?>" data-name="cantidad" type="text" class="form-control cantidad" placeholder="Cantidad"></td>
                        <td><input name='precio_venta_<?= $filas ?>' data-name="precio_venta" type="text" class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='por_desc_<?= $filas ?>' data-name="por_desc" type="text" class="form-control por_venta" placeholder="% Descuento"></td>
                        <td><input name='precio_desc_<?= $filas ?>' data-name="precio_desc" type="text" class="form-control precio_desc" placeholder="Precio descuentos"></td>                                                
                        <td><input name='total_<?= $filas ?>' data-name="total" type="text" class="form-control total" placeholder="Total">
                            <input type="hidden" name="ivah_<?= $filas ?>" data-name="ivah" value="" id="ivah" class="ivah">
                            <input type="hidden" name="disponible_<?= $filas ?>" data-name="disponible" value="" id="disponible" class="disponible">
                            <input type="hidden" name="inventariable_<?= $filas ?>" data-name="disponible" id="inventariable" class="inventariable" value="">
                        </td>                                                
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Total Venta Gs: </label>
              <div class="col-sm-7">
                  <input type="text" class="form-control" name="total_venta" id="total_venta" value="<?= empty($venta)?0:$venta->total_venta ?>">
              </div>
            </div>
        </div>
    </div>        
    <div class="row">
        <div class="col-xs-2 col-xs-offset-4">
            <div class="form-group">
                <label for="proveedor" class="col-sm-4 control-label">Vuelto: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="vuelto" id="vuelto" value="<?= empty($venta)?0:$venta->vuelto ?>">
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Efectivo: </label>
              <div class="col-sm-7">
                  <input type="text" class="form-control" name="total_efectivo" id="total_efectivo" value="<?= empty($venta)?0:$venta->total_efectivo ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Debito: </label>
              <div class="col-sm-7">
                  <input type="text" class="form-control" name="total_debito" id="total_debito" value="<?= empty($venta)?0:$venta->total_debito ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Credito: </label>
              <div class="col-sm-7">
                  <input type="text" class="form-control" name="total_credito" id="total_credito" value="<?= empty($venta)?0:$venta->total_credito ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Cheque: </label>
              <div class="col-sm-7">
                  <input type="text" class="form-control" name="total_cheque" id="total_cheque" value="<?= empty($venta)?0:$venta->total_cheque ?>">
              </div>
            </div>
        </div>
    </div>
    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
        Más Detalles
    </a>
    <div class="collapse" id="collapseExample">
      <div class="well">    
          
        <div class="form-group">
          <label for="proveedor" class="col-sm-4 control-label">Total en Dolares: </label>
          <div class="col-sm-8">
              <input type="text" class="form-control" name="total_dolares" id="total_dolares"  value="<?= empty($venta)?0:$venta->total_dolares ?>">
          </div>
        </div>
          
        <div class="form-group">
          <label for="proveedor" class="col-sm-4 control-label">Total en reales: </label>
          <div class="col-sm-8">
              <input type="text" class="form-control" name="total_reales" id="total_reales" value="<?= empty($venta)?0:$venta->total_reales ?>">
          </div>
        </div>
          
        <div class="form-group">
            <label for="proveedor" class="col-sm-4 control-label">IVA 5%: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="iva" id="iva" value="<?= empty($venta)?0:$venta->iva ?>">
            </div>
        </div>
          
        <div class="form-group">
            <label for="proveedor" class="col-sm-4 control-label">IVA 10%: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="iva2" id="iva2" value="<?= empty($venta)?0:$venta->iva2 ?>">
            </div>
        </div>
          
        <div class="form-group">
            <label for="proveedor" class="col-sm-4 control-label">Exenta: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="exenta" id="exenta" value="<?= empty($venta)?0:$venta->exenta ?>">
            </div>
        </div>                  
          
          <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Total IVA: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" name="total_iva" id="total_iva" value="<?= empty($venta)?0:$venta->total_iva ?>">
              </div>
            </div>
          
          <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Total Descuentos: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" name="total_descuentos" id="total_descuentos" value="<?= empty($venta)?0:$venta->total_descuentos ?>">
              </div>
            </div>
          
      </div>
    </div>
    <div class="row" style="margin-top:40px">        
        <a href="<?= base_url('maestras/clientes/add/json') ?>" target="_new" class="btn btn-default">Agregar Nuevo Cliente</a>        
        <a href="#" class="btn btn-default">Realizar Pedido a otra Sucursal</a>
        <button type="submit" id="guardar" class="btn btn-success">Guardar Compra</button>        
        <a href="javascript:imprimir()" id="buttonPrint" style="display:<?= !empty($venta)?'':'none' ?>" class="btn btn-default buttonPrint">Imprimir Factura Legal</a>        
        <a href="javascript:imprimir2()" id="buttonPrint2" style="display:<?= !empty($venta)?'':'none' ?>" class="btn btn-default buttonPrint">Imprimir Factura Legal | Lineas</a>        
        <a href="javascript:imprimirTicket()" id="buttonticket" style="display:<?= !empty($venta)?'':'none' ?>" class="btn btn-default buttonPrint">Imprimir Ticket</a>        
    </div>
</form>
<?php $this->load->view('predesign/datepicker',array('scripts'=>'a')) ?>
<?= $this->load->view('predesign/chosen.php') ?>
<script>
    var callbackAfterAddRow = function(){};
    var bandera = 0;
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');
            $("tbody tr:last-child").find('.lote').parents('td').html('<select data-name="lote" class="lote form-control"></select>');
            date_init_calendar();
            refreshNames(obj);
    }
    function removerow(obj){
        var o = $(obj).parents('tbody');
        $(obj).parent('td').parent('tr').remove();        
        refreshNames(o);
        $(document).trigger('total');
    }
    
    function refreshNames(obj){
        bandera = 0;
        $(obj).parents('table').find('tr').each(function(){
            console.log('tr');
            $(this).find('input, select').each(function(){
               $(this).attr('name',$(this).data('name')+'_'+(bandera-1)); 
            });
            bandera+=1;
        });
    }
    
    function tryagain(){
        $.post('<?= base_url('json/clientes') ?>',{},function(data){
            
            $("#cliente_div #cliente_chzn").remove();
            $("#cliente").html(data);
            $("#cliente").removeClass('chzn-done');            
            $(".chosen-select,.chosen-multiple-select").chosen({allow_single_deselect:true});
        });
    }
    
    //Eventos
    $(document).on('keydown','input',function(event){        
            
        if (event.which == 13){
            if($(this).hasClass('total')){
                addrow($(this));
            }
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } 
            else if($(this).val()!='' && $(this).hasClass('codigo')){
                $(this).trigger('change');                
            }
            else if($(this).val()=='' && $(this).hasClass('codigo')){
                return false;
            }
            else
            {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });
    
    $(document).on('keyup','body',function(event){        
        if(event.which=='113'){
            $("#formulario").submit();
        }
    });
    
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    });

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    }); 
    
   $("body").on('change','.codigo',function(){
       if($(this).val()!=''){
        focused = $(this);
        elements = $(".codigo").length;
        focusedcode = $(this).val();
        focusedlote = $(this).parents('tr').find('.lote').val();
        i = 0;
        $(".codigo").each(function(obj){
            i++;            
            if($(this).val()==focusedcode && $(this).parents('tr').find('.lote').val() == focusedlote && !$(this).is(focused))
            {
               row = $(this).parent().parent().find('.cantidad');
               row.val(parseInt(row.val())+1);
               focused.parents('tr').find('input').val('');
            }
            
            else if(i==elements && $(this).val()!=''){
                $.post('<?= base_url('json/getProduct') ?>',{codigo:$(this).val()},function(data){
                    data = JSON.parse(data);
                    data = data['producto'];
                    if(data!=''){
                    focused = focused.parent().parent();
                    focused.find('.producto').val(data['nombre_comercial'])
                    focused.find('.cantidad').val(1);
                    focused.find('.precio_venta').val(data['precio_venta']);
                    focused.find('.por_venta').val(data['descuentos']);
                    focused.find('.precio_desc').val(data['descuentos']);
                    focused.find('.total').val(data['precio_venta']);  
                    focused.find('.ivah').val(data['iva_id']);
                    focused.find('.disponible').val(data['disponible']);
                    focused.find('.lote').parents('td').html(data['lotelist']);
                    focused.find('.lote').val(focused.find('.lote option:first').val());
                    focused.find('.por_venta').val(data['descmax']);
                    focused.find('.inventariable').val(data['inventariable']);
                    $(document).trigger('total');
                    callbackAfterAddRow();
                    callbackAfterAddRow = function(){};
                    addrow(focused.find('a'));
                    $("tbody tr").last().find('.codigo').focus();
                    }
                    else{
                        emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
                    }                                        
                });
            }               
        });
        }
    });
    
    $(document).on('change','.cantidad,.por_venta',function(){$(document).trigger('total')});
    $(document).on('change','#total_efectivo,#total_debito,#total_credito,#total_cheque',function(){
        total_venta = (parseInt($("#total_venta").val()) - parseInt($("#total_efectivo").val()) - parseInt($("#total_debito").val()) - parseInt($("#total_credito").val()) - parseInt($("#total_cheque").val()))*-1;
        $("#vuelto").val(total_venta);
    });
    $(document).on('total',function(){      
        $("#total_venta").val(0);
        $("#total_iva").val(0);
        $("#iva").val(0);
        $("#iva2").val(0);
        $("#exenta").val(0);
        $("#total_descuentos").val(0);
        $("tbody tr").each(function(){
            self = $(this);   
            if(parseInt(self.find('.inventariable').val())===1 && parseFloat(self.find('.cantidad').val())>parseInt(self.find('.disponible').val())){
                self.find('.cantidad').val(self.find('.disponible').val());
                emergente('Ha excedido el limite de existencias en inventario.');
            }            
            total = parseFloat(self.find('.cantidad').val())*parseInt(self.find('.precio_venta').val());                        
            //Calcular descuento
            descuento = self.find('.por_venta').val();
            if(descuento==''){
                descuento = 0;
                self.find('.por_venta').val(0);
            }

            precio_desc = parseInt(self.find('.precio_venta').val() - (parseInt(self.find('.precio_venta').val() * descuento)/100));
            self.find('.precio_desc').val(precio_desc);
            total = precio_desc * parseFloat(self.find('.cantidad').val());
            
            if(!isNaN(total)){
                self.find('.total').val(total);
                /*Total Venta*/
                total_venta = parseInt($("#total_venta").val());            
                total_venta += total;
                $("#total_venta").val(total_venta); 
                /*Total Descuento*/
                total_descuento = parseInt($("#total_descuentos").val());            
                total_descuento += (parseInt(self.find('.precio_venta').val()) - precio_desc)*parseFloat(self.find('.cantidad').val());
                $("#total_descuentos").val(total_descuento);
                /* IVA */
                total_iva = parseInt($("#total_iva").val());
                iva = parseInt(self.find('.ivah').val());
                //ivar = iva>0?(iva*total)/100:0;
                ivar = iva==0?0:iva==5?total/21:total/11;
                total_iva+=ivar;
                $("#total_iva").val(total_iva.toFixed(0));  
                                
                i = iva==0?'exenta':iva==5?'iva':'iva2';
                x = parseInt($("#"+i).val())+(ivar);
                $("#"+i).val(x.toFixed(0));
            }
        });
    });
    var ajax = undefined;
    function val_send(form){
        console.log(form);
        if(ajax==undefined){
            $(".mask").show();
            var data = form;
            $(".alert").removeClass('alert-danger').removeClass('alert-success').addClass('alert-info').html('Se esta enviando el registro').show();
            $(document).trigger('total');
            data = new FormData(data);
            data.append('filas',$("#detall tbody").find('tr').length);
            $("#guardar").attr('disabled','disabled');
            ajax = $.ajax({
                url:'<?= empty($venta)?base_url('json/ventas'):base_url('json/ventas/edit') ?>',
                method:'post',
                data:data,
                processData:false,
                cache: false,
                contentType: false,
                success:function(data){
                    ajax = undefined;
                    data = JSON.parse(data);                    
                    if(data['status']){
                        $(".alert").removeClass('alert-info');
                        $(".alert").removeClass('alert-danger').addClass('alert-success').html('Se han guardado los datos con exito').show();                        
                        document.location.reload();
                    }
                    else{
                        $(".alert").removeClass('alert-info');
                        $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
                        $(".mask").hide();
                    }
                    $("#guardar").removeAttr('disabled');
                },
                error:function(data){
                    $(".mask").hide();
                    ajax = undefined;
                    emergente('Ocurrio un error intentando guardar el registro, por favor intente de nuevo');
                }
                });
            }
            else emergente('Se esta enviando el registro, por favor espere!!...');
            return false;
    }
    
    function advancesearch()
    {
        $.post('<?= base_url('json/searchProduct/') ?>',{},function(data){
            emergente(data);
        });
    }
    
    function selCod(cod,lote){
        $("tbody tr:last-child").find('.codigo').val(cod);
        $("tbody tr:last-child").find('.codigo').trigger('change');
        if(lote===''){
            $("tbody tr:last-child").find('.lote').val(lote);
        }
        callbackAfterAddRow = function(){
            if(lote!==''){
                $("tbody tr:last-child").find('.lote').val(lote);
            }
        };
        $('#myModal').modal('hide');
    }    
    <?php if(!empty($venta->id)): ?>
        function imprimir(){
            window.open('<?= base_url('movimientos/'.$this->router->fetch_class()) ?>/ventas/imprimir/<?= $venta->id ?>');
        }
        function imprimir2(){
            window.open('<?= base_url('movimientos/'.$this->router->fetch_class()) ?>/ventas/imprimir2/<?= $venta->id ?>');
        }
        function imprimirTicket(){
            window.open('<?= base_url('movimientos/'.$this->router->fetch_class()) ?>/ventas/imprimirticket/<?= $venta->id ?>');
        }
    <?php else: ?>
    function imprimir(codigo){
            window.open('<?= base_url('movimientos/ventas/ventas/imprimir/') ?>/'+codigo);
     }
     function imprimir2(codigo){
            window.open('<?= base_url('movimientos/ventas/ventas/imprimir2/') ?>/'+codigo);
     }
      function imprimirTicket(codigo){
            window.open('<?= base_url('movimientos/ventas/ventas/imprimirticket/') ?>/'+codigo);
     }
     function showButton(codigo){
            $("#buttonPrint").attr('href','javascript:imprimir('+codigo+')');
            $("#buttonPrint2").attr('href','javascript:imprimir2('+codigo+')');
            $("#buttonticket").attr('href','javascript:imprimirTicket('+codigo+')');
            $(".buttonPrint").show();
     }
    <?php endif ?>
</script>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>