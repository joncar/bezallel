<thead>
        <tr>
            <th>Codigo</th>
            <th>Nombre Generico</th>
            <th>Nombre Comercial</th>
            <?php if($productos->num_rows()>0 && isset($productos->row()->st)): ?>
            <th>Stock</th>
            <th>Lote</th>
            <th>Precio</th>
            <th>Vencimiento</th>
            <th>Sucursal</th>
            <th>Propiedades</th>
            <?php endif ?>
        </tr>
    </thead>
<?php foreach($productos->result() as $p): ?>
     <tbody>
        <tr>
            <td><?php if(isset($sucursal) || empty($_SESSION['sucursal']) || (!empty($_SESSION['sucursal']) && ($p->sucursal_id==$_SESSION['sucursal'] || $sucursal==0))): $p->vencimiento = empty($p->vencimiento)?'':date("d/m/Y",strtotime($p->vencimiento)); ?>
                <?php 
                    $where = array('producto'=>$p->codigo);
                    if(!empty($p->lote))
                        $where['lote'] = $p->lote;
                    $p->precio_costo = $this->db->get_where('compradetalles',$where);
                    $p->precio_costo = $p->precio_costo->num_rows()>0?$p->precio_costo->row()->precio_costo:0;
                    $p->precio_venta = $this->db->get_where('productos',array('codigo'=>$p->codigo));
                    $p->precio_venta = $p->precio_venta->num_rows()>0?$p->precio_venta->row()->precio_venta:0;
                    if(isset($sucursal) && $sucursal==0){
                        $this->db->select('SUM(productosucursal.stock) as suc');
                        $this->db->where('productosucursal.producto',$p->codigo);
                        if(!empty($p->lote)){
                            $this->db->where('productosucursal.lote',$p->lote);
                        }
                        $p->cantidad = $this->db->get_where('productosucursal')->row()->suc;
                    }
                ?>
                <a href="javascript:selCod('<?= $p->codigo ?>','<?= $p->lote ?>',<?= str_replace('"',"'",json_encode($p)) ?>)"><?= $p->codigo ?></a>
                <?php else: ?>
                <span style="color:gray"><?= $p->codigo ?></span>
                <?php endif ?>
            </td>
            <td><?= $p->nombre_generico ?></td>
            <td><?= $p->nombre_comercial ?></td>
            <?php if(isset($p->st)): ?>
            <td><?= $p->st ?></td>
            <td><?= $p->lote ?></td>
            <td><?= $p->precio_venta ?></td>
            <?php $fecha = date("d/m/Y",strtotime($p->vencimiento)); 
                  $fecha = $fecha=='31/12/1969'?'N/A':$fecha; ?>
            <td><?= $fecha ?></td>
            <td><?= $p->sucursal ?></td>
            <td><?= $p->propiedades ?></td>
            <?php endif ?>
        </tr>
    </tbody>
<?php endforeach; ?>