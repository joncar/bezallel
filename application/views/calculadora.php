<div class="modal fade" id="calculator" tabindex="-1" role="dialog" aria-hidden="false">
  <div class="modal-dialog" role="document" style="width: 90vw;">
    <div class="modal-content" style="width: 90vw;height: 92vh;overflow: auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Calculadora de cortes</h4>
      </div>
      <div class="modal-body">
        <iframe src="<?= base_url() ?>calculadora.html" style="width: 100%;height: 72vh; border:0"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>