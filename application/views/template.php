<!Doctype html>
<html lang="es">
    <head>
            <title><?= empty($title)?'SysFarma':$title.'' ?></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <script>window.URI = '<?= base_url() ?>';</script>
            <script>window.wURL = window.URL;</script>
            <?php if(empty($crud) || empty($css_files) || !empty($loadJquery)): ?>
            <script src="<?= base_url('js/jquery-1.10.0.js') ?>"></script>      
            <script src="<?= base_url('js/dist/js/bootstrap.min.js') ?>"></script>                
            <link rel="stylesheet" type="text/css" href="<?= base_url('js/dist/css/bootstrap.min.css') ?>">
            <?php endif ?>
            <?php 
            if(!empty($css_files) && !empty($js_files)):
            foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
            <?php endforeach; ?>
            <?php foreach($js_files as $file): ?>
            <script src="<?= $file ?>"></script>
            <?php endforeach; ?>                
            <?php endif; ?>
        <script src="<?= base_url().'js/jquery-migrate.min.js' ?>"></script>
        <link rel="stylesheet" href="<?= base_url('js/font-awesome-4.7.0/css/font-awesome.min.css') ?>">    
        <link href="<?= base_url('css/opensans/opensans.css') ?>" rel="stylesheet">
        <?php if($this->router->fetch_class()=='main'): ?>
            <link href="<?= base_url('css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/components.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?= base_url('css/plugin.min.css') ?>" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/login.css') ?>" rel="stylesheet" type="text/css" />
        <?php else: ?>
            <link rel="stylesheet" type="text/css" href="<?= base_url('css/ace.min.css') ?>">
            <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
            <script src="<?= base_url('js/ace-extra.min.js') ?>"></script>  
            <script src="<?= base_url().'js/frame.js' ?>"></script>
            <script src="<?= base_url().'js/numeral.min.js' ?>"></script>
            <?php $this->load->view('predesign/multiselect') ?>                
        <?php endif ?>
                  
    </head>  
    <?php $this->load->view($view) ?>     
    <?php $this->load->view('calculadora') ?> 
    <script>
        var sec = 0;
        var time = <?php echo time()*1000; ?>;
        var fecha;

        function updateReloj(){
            time = time+1000;
            fecha = new Date();
            fecha.setTime(time);
            $('#fechaSistema').html(fecha.getDate()+'/'+fecha.getMonth()+'/'+fecha.getFullYear()+' '+fecha.getHours()+':'+fecha.getMinutes()+':'+fecha.getSeconds());
            setTimeout(updateReloj,1000);
        }
        updateReloj();        
    </script>     
</html>
