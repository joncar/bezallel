<?php if(!empty($msj)): ?>
<?= $msj ?>
<?php endif ?>
<form class="form-horizontal" id='formulario' method="post" action="" role="form">
    <div class="row well">
        <div class="col-xs-6">
            Cliente: <b><?= $cliente ?></b>
        </div>
        <div class="col-xs-6" align="right">
            Fecha: <b><?= date("d-m-Y H:i:s") ?></b>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
            <div align="right">
                Saldo de notas de crédito: <span style="font-size:29px"><b><?= number_format($notas,2,',','.') ?> Gs</b></span>
            </div>
            <div align="right">
                Saldo del cliente: <span style="font-size:29px"><b><?= number_format($saldo,2,',','.') ?> Gs</b></span>
            </div>
            <?php if($saldo>0): ?>
            <div class="form-group">
                <label for="monto" class="col-sm-4 control-label">Monto</label>
                <div class="col-sm-8">
                    <input type="text" name="monto" id="monto" class="form-control" placeholder="Monto a abonar" required="" min="1">
                </div>
            </div>
            <div align="center">
                <button type="submit" class="btn btn-success">Abonar monto</button>
                <a href="<?= base_url() ?>cajas/admin/saldos" class="btn btn-danger">Volver a saldos</a>
            </div>
            <?php else: ?>
            Este cliente no posee deudas, o ya ha cancelado la totalidad de las mismas
            <?php endif ?>
        </div>
    </div>
</form>
<script>    
    function imprimir(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimir/'+codigo);
     }
      function imprimirTicket(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimirticket/'+codigo);
     }

    <?php if(!empty($_GET['pago'])): ?>
        imprimirTicket(<?= $_GET['pago'] ?>);
    <?php endif ?>
</script>