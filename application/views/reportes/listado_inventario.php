<?php if(empty($_POST)): ?>
<div class="container">
    <h1 align="center"> Resumen de ventas</h1>
<form action="<?= base_url('reportes/listado_inventario') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un proveedor</label>
        <?= form_dropdown_from_query('proveedor','proveedores','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['proveedor']))$cliente = $this->db->get_where('proveedores',array('id'=>$_POST['proveedor']))->row()->denominacion; ?>
<? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Listado de inventario</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>
    <p><strong>Proveedor: </strong> <?= empty($_POST['proveedor'])?'Todos':$cliente ?></p>    
    
    <table border="0" cellspacing="18" class="table" width="100%">
        <thead>
                <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Lote</th>
                        <th>Vencimiento</th>
                        <th>Cantidad</th>
                        <th>P.Venta</th>
                        <th>P.Venta|Con Desc.</th>
                </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($_POST['proveedor']))$this->db->where('productos.proveedor_id',$_POST['proveedor']);
                if(!empty($_POST['sucursal']))$this->db->where('productosucursal.sucursal',$_POST['sucursal']);
                $this->db->where('productosucursal.stock > ',0);
                $total = 0;
                $total_debito = 0;
                $total_credito = 0;
                $this->db->select('productosucursal.producto as codigo, productosucursal.vencimiento, productos.nombre_comercial as producto, productosucursal.lote, productos.precio_venta, productos.descmax, productosucursal.stock');
                $this->db->join('productos','productos.codigo = productosucursal.producto');                                
                $ventas = $this->db->get('productosucursal');
            ?>
            <?php 
                    foreach($ventas->result() as $c):   
                    $c->descuento = $c->precio_venta - (($c->precio_venta *$c->descmax)/100);
                ?>
                <tr>                        
                        <td><?= $c->codigo ?></td>
                        <td><?= $c->producto ?></td>
                        <td><?= $c->lote ?></td>
                        <td><?= $c->vencimiento ?></td>
                        <td><?= $c->stock ?></td>
                        <td><?= number_format($c->precio_venta,0,',','.') ?></td>
                        <td><?= number_format($c->descuento,0,',','.') ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php endif; ?>
