<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de ventas por cliente</h1>
<form action="<?= base_url('reportes/resumen_ventas_clientes') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un cliente</label>
        <?php $this->db->order_by('apellidos','asc'); ?>
        <?= form_dropdown_from_query('clientes','clientes','id','apellidos nombres nro_documento',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Tipo de venta</label>        
        <?= form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',0); ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <? if(!empty($_POST['clientes']))$clientes = $this->db->get_where('clientes',array('id'=>$_POST['clientes']))->row(); ?>
    <? if(!empty($_POST['transaccion']))$transaccion = $this->db->get_where('tipotransaccion',array('id'=>$_POST['transaccion']))->row()->denominacion; ?>
    <? $desde = !empty($_POST['desde'])?date("d-m-Y",strtotime(str_replace("/","-",$_POST['desde']))):'Todos'; ?>
    <? $hasta = !empty($_POST['hasta'])?date("d-m-Y",strtotime(str_replace("/","-",$_POST['hasta']))):'Todos'; ?>
    <h1 align="center"> Resumen de ventas por cliente</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?><br/>
    <strong>Cliente: </strong> <?= empty($_POST['clientes'])?'Todos':$clientes->nombres.' '.$clientes->apellidos ?><br/>
    <strong>Tipo de venta: </strong> <?= empty($_POST['transaccion'])?'Todos':$transaccion ?><br/>
    <strong>Desde: </strong> <?= $desde ?> <strong>Hasta: </strong> <?= $hasta ?></p>    
    <table border="0" cellspacing="5" class="table" style='width:100%; font-size:12px; margin:0px; padding:0px;'>
        <thead>
                <tr>
                    <th style="width:60px">Nro.Venta</th>
                    <th style="width:60px">Usuario</th>
                    <th style="width:80px">Fecha</th>
                    <th style="width:60px">Hora</th>
                    <th style="width:140px">Mercaderia</th>
                    <th style="width:50px">Precio</th>
                    <th style="width:50px">Cant.</th>
                    <th style="width:50px">Total</th>
                </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
                    $this->db->where('DATE(ventas.fecha) between \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))).'\' AND \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))).'\'',null,TRUE);
                }                
                if(!empty($_POST['sucursal'])){
                    $this->db->where('ventas.sucursal',$_POST['sucursal']);
                }
                if(!empty($_POST['clientes'])){
                    $this->db->where('ventas.cliente',$_POST['clientes']);
                }
                if(!empty($_POST['transaccion'])){
                    $this->db->where('ventas.transaccion',$_POST['transaccion']);
                }
                $this->db->where('ventas.status > ',-1);
                $this->db->order_by('fecha','ASC');
                $total = 0;
                $this->db->select('ventas.nro_factura, user.nombre as usuario, ventas.fecha, productos.codigo, productos.nombre_comercial as producto, ventadetalle.precioventa, ventadetalle.cantidad, ventadetalle.totalcondesc as total');                
                $this->db->join('ventas','ventas.id = ventadetalle.venta');
                $this->db->join('user','user.id = ventas.usuario','left');                
                $this->db->join('productos','productos.codigo = ventadetalle.producto');
                $ventas = $this->db->get('ventadetalle');                
            ?>
            <?php foreach($ventas->result() as $c): ?>
                <tr>                        
                        <td><?= $c->nro_factura ?></td>
                        <td><?= $c->usuario ?></td>
                        <td><?= date("d-m-Y",strtotime($c->fecha)) ?></td>
                        <td><?= date("H:i:s",strtotime($c->fecha)) ?></td>
                        <td><?= $c->codigo.'-'.cortar_palabras($c->producto,3) ?></td>
                        <td align="right"><?= number_format($c->precioventa,0,',','.') ?></td>
                        <td><?= $c->cantidad?></td>
                        <td align="right"><?= number_format($c->total,0,',','.') ?></td>
                        <?php $total+= $c->total ?>
                </tr>
            <?php flush(); ?>
            <?php endforeach ?>
            <tr>
                    <td colspan="7" align="right" style="font-weight:bold;">Total</td>
                    <td style="font-weight:bold;"><?= number_format($total,0,',','.') ?></td>
            </tr>
        </tbody>
    </table>
<?php endif; ?>