<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de vencimientos</h1>
<form action="<?= base_url('reportes/vencimientos') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un laboratorio</label>
        <?= form_dropdown_from_query('proveedor','proveedores','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['proveedor']))$proveedor = $this->db->get_where('proveedores',array('id'=>$_POST['proveedor']))->row()->denominacion; ?>
    <? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Resumen de vencimientos</h1>
    <p><strong>Laboratorio: </strong> <?= empty($_POST['proveedor'])?'Todos':$proveedor ?></p>    
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>    
    <p><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?></p>
    
    <table border="0" cellspacing="18" class="table" width="100%" style="font-size:9px;">
        <thead>
                <tr>
                    <th>Sucursal</th>
                    <th>Laboratorio</th>
                    <th>Producto</th>
                    <th>Lote</th>
                    <th>Cantidad</th>
                    <th>Vencimiento</th>                        
                </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
                    $this->db->where('Date(productosucursal.vencimiento) between \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))).'\' AND \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))).'\'',null,TRUE);
                }
                if(!empty($_POST['proveedor']))$this->db->where('compras.proveedor',$_POST['proveedor']);
                if(!empty($_POST['sucursal']))$this->db->where('productosucursal.sucursal',$_POST['sucursal']);
                $this->db->where('productosucursal.vencimiento != ','1969-12-31');

                $this->db->select('sucursales.denominacion as suc, proveedores.denominacion as laboratorio, productos.nombre_comercial, productosucursal.lote, productosucursal.stock, productosucursal.vencimiento');                
                $this->db->join('compradetalles','compradetalles.producto = productos.codigo','inner');
                $this->db->join('compras','compras.id = compradetalles.compra','inner');                
                $this->db->join('proveedores','proveedores.id = compras.proveedor','inner');
                $this->db->join('productosucursal','productosucursal.producto = productos.codigo','inner');
                $this->db->join('sucursales','compras.sucursal = sucursales.id','inner');
                $this->db->group_by('productosucursal.id');
                $ventas = $this->db->get('productos');
            ?>
            <?php foreach($ventas->result() as $c): ?>
                <tr>
                        <td><?= cortar_palabras($c->suc,2) ?></td>
                        <td><?= $c->laboratorio ?></td>
                        <td><?= cortar_palabras($c->nombre_comercial,5) ?></td>
                        <td><?= $c->lote ?></td>
                        <td><?= $c->stock ?></td>
                        <td><?= date("d/m/Y",strtotime($c->vencimiento)) ?></td>                        
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php endif; ?>