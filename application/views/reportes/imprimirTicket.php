<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Factura</title>
                
                
        </head>
        <body style='font-size:11px; width:227px; margin:37px;'>
            <h3 align="center" style="font-size:20px; font-weight:bold; margin-bottom:5px"><?= $venta->denominacion ?></h3>
            <div align="center"><?= $venta->direccion ?></div>
            <div align="center" style='border-bottom:1px solid black'>Telef. <?= $venta->telefono ?></div>
            <table style='width:100%; font-size:11px;'>
                <tr><td><b>Nro. Venta: </b><?= $venta->id ?></td><td><b>Cajero/a</b> <?= $_SESSION['nombre'] ?></td></tr>
            </table>
        <div><b>Fecha: </b><?= date("d/m/Y H:i:s",strtotime($venta->fecha)) ?></div>
        <div><b>Caja: </b><?= $venta->caja ?></div>
        <div><b>Condición venta: </b><?= $this->db->get_where('tipotransaccion',array('id'=>$venta->transaccion))->row()->denominacion ?></div>
        <div><b>Cliente: </b><?= $venta->clientename.' '.$venta->clienteadress ?></div>
        
        <div>
            <table cellspacing="10" style="font-size:11px;">
                <thead>
                    <tr style="border-top:1px solid black; border-bottom:1px solid black">
                        <th style="width:40%;">Descripcion</th>
                        <th style="width:20%; text-align:center;">Cant.</th>
                        <th style="width:20%; text-align:center;">Precio Unit.</th>
                        <th style="width:20%; text-align:center;">Total</th>
                    </tr>
                </thead>
                    <tbody>
                        <?php foreach($detalles->result() as $d): ?>
                            <tr><td><?php $this->db->or_where('id',$d->producto); $this->db->or_where('codigo',$d->producto); echo $this->db->get_where('productos')->row()->nombre_comercial ?></td><td align="right"><?= $d->cantidad ?></td><td align="right"><?= $d->precioventa ?></td><td align="right"><?= $d->totalcondesc ?></td></tr>
                        <?php endforeach ?>
                            <tr><td style="border-top:1px solid black">Total Venta: </td><td style="border-top:1px solid black" colspan='3' align="right"><?= number_format($venta->total_venta,0,',','.').' Gs' ?></td></tr>
                            <tr><td>Efectivo: </td><td colspan='3' align="right"><?= number_format($venta->total_efectivo,0,',','.').' Gs' ?></td></tr>
                            <tr><td>Vuelto: </td><td colspan='3' align="right"><?= number_format($venta->vuelto,0,',','.').' Gs' ?></td></tr>
                    </tbody>
                </table>
        </div>
        <p align='center' style="margin:10px; font-size:14px;"><i>Agradecemos su preferencia</i></p>
        </body>
        <script>
            window.print();
        </script>
</html>
