 <div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Sucursal ID</label>
              <div class="col-sm-8">
                <?php $selected = empty($compra)?0:$compra->sucursal ?>
                <?= form_dropdown_from_query('sucursal',$this->db->get('sucursales'),'id','denominacion',$selected) ?>
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Proveedor</label>
              <div class="col-sm-8">
                <?php $selected = empty($compra)?0:$compra->proveedor ?>
                <?= form_dropdown_from_query('proveedor',$this->db->get('proveedores'),'id','denominacion',$selected) ?>
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
              <label for="transaccion" class="col-sm-4 control-label">Transacción</label>
              <div class="col-sm-8">
                <?php $selected = empty($compra)?0:$compra->transaccion ?>
                <?= form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',$selected); ?>
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
              <label for="fecha" class="col-sm-4 control-label">Fecha</label>
              <div class="col-sm-8">
                <input type="text" name="fecha" class="form-control datetime-input" id="fecha" value="<?= empty($compra)?'':date("d/m/Y",strtotime($compra->fecha)) ?>" placeholder="Fecha">
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
              <label for="forma" class="col-sm-4 control-label">Forma Pago</label>
              <div class="col-sm-8">
                  <?php $selected = empty($compra)?0:$compra->forma_pago ?>
                <?= form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',$selected) ?> 
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
              <label for="vencimiento_pago" class="col-sm-4 control-label">Vencimiento Pago</label>
              <div class="col-sm-8">
                <input type="text" value="<?= empty($compra)?'':date("d/m/Y",strtotime($compra->vencimiento_pago)) ?>" name="vencimiento_pago" class="form-control datetime-input" id="vencimiento" placeholder="Vencimiento Pago">
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
              <label for="factura" class="col-sm-4 control-label">Nro. Factura</label>
              <div class="col-sm-8">
                <input type="text" name="nro_factura" <?= empty($compra)?'':'readonly' ?> class="form-control" id="inputEmail3" placeholder="factura" value="<?= empty($compra)?'':$compra->nro_factura ?>">
                <input type="hidden" name="total_compra" id="total_compra" value='<?= empty($compra)?'':$compra->total_compra ?>'>
                <input type="hidden" name="total_descuento" id="total_descuento" value='<?= empty($compra)?'':$compra->total_descuento ?>'>
              </div>
            </div>
        </div>
    </div>
    </div>
    <div class="row">
        
        
        
        <div class="col-xs-12"><a href="javascript:searchProduct()" class="btn btn-default">Busqueda avanzada de productos</a> <a target="_new" href="<?= base_url('movimientos/productos/inventario') ?>" class="btn btn-default">Inventariado</a></div>
        <div class="col-xs-12">
            <h4>Detalle de compra</h4>
            <table class="table table-striped" style="font-size:12px;" cellspacing="2">
                <thead>
                    <tr>
                        <th style="width:18%">Código</th>
                        <th style="width:21%">Nombre artículo</th>
                        <th style="width:10%">Lote</th>
                        <th style="width:11%">Vence</th>
                        <th style="width:5%">Cant.</th>
                        <th style="width:7%">P.Costo</th>
                        <th style="width:7%">% Desc</th>
                        <th style="width:5%">% Venta</th>
                        <th style="width:7%">P. Venta</th>
                        <th style="width:10%">Total</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($detalles)): foreach($detalles->result() as $d): ?>
                    <tr>
                        <td><input name="codigo[]" type="text" value='<?= $d->producto ?>' class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto[]" type="text" value='<?= $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->nombre_comercial ?>'  class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><input name="lote[]" type="text" value='<?= $d->lote ?>'  class="form-control lote" placeholder="Lote"></td>
                        <td><input name="vencimiento[]" type="text" value='<?= $d->vencimiento!='1969-12-31'?date("d/m/Y",strtotime($d->vencimiento)):'' ?>'  class="form-control vencimiento datetime-input" placeholder="Vencimiento"></td>
                        <td><input name="cantidad[]" type="text" value='<?= $d->cantidad ?>'  class="form-control cantidad" placeholder="Cantidad"></td>
                        <td><input name='precio_costo[]' type="text" value='<?= $d->precio_costo ?>'  class="form-control precio_costo" placeholder="Precio costo"></td>
                        <td><input name='por_desc[]' type="text" value='<?= $d->por_desc ?>'  class="form-control por_desc" placeholder="% Descuento"></td>
                        <td><input name='por_venta[]' type="text" value='<?= $d->por_venta ?>'  class="form-control por_venta" placeholder="% Venta"></td>
                        <td><input name='precio_venta[]' type="text" value='<?= $d->precio_venta ?>'  class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='total[]' type="text" value='<?= $d->total ?>'  class="form-control total" placeholder="Total">
                        </td>                                                
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                    <?php  endforeach; endif ?>
                    <tr>
                        <td><input name="codigo[]" type="text" class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto[]" type="text" class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><input name="lote[]" type="text" class="form-control lote" placeholder="Lote"></td>
                        <td><input name="vencimiento[]" type="text" class="form-control vencimiento datetime-input" placeholder="Vencimiento"></td>
                        <td><input name="cantidad[]" type="text" class="form-control cantidad" placeholder="Cantidad"></td>
                        <td><input name='precio_costo[]' type="text" class="form-control precio_costo" placeholder="Precio costo"></td>
                        <td><input name='por_desc[]' type="text" value=''  class="form-control por_desc" placeholder="% Descuento"></td>
                        <td><input name='por_venta[]' type="text" class="form-control por_venta" placeholder="% Venta"></td>
                        <td><input name='precio_venta[]' type="text" class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='total[]' type="text" class="form-control total" placeholder="Total">
                        </td>                                                
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row" align="right" style="margin-top:40px; margin-bottom:40px;">
            <div class="col-xs-11">Total Descuentos: <span id="totaldescuento">0.00</span></div>
            <div class="col-xs-11">Total Factura: <span id="totalrecibo">0.00</span></div>
        </div>
        <div class="row">
            <div class="col-xs-11">
                <button class="btn btn-success" id="guardar" type="submit">Guardar Compra</button>            
                <a class="btn btn-default" href="<?= base_url('movimientos/productos/productos/add') ?>">Agregar nuevo producto</a>
                <a class="btn btn-default" href="<?= base_url('maestras/proveedores/add') ?>">Agregar proveedor</a>
            </div>
        </div>
    </div>
</form>

<script>    
    var focused,elements,focusedcode,i, totalrecibo;    
    $(document).ready(function(){
        $(".total").attr('readonly',true);
    })
    $(document).on('total',function(){        
        $("tr").each(function(){
            self = $(this);                        
            total = parseFloat(self.find('.cantidad').val())*parseInt(self.find('.precio_costo').val());
            por_desc = parseFloat(self.find('.por_desc').val());            
            por_desc = !isNaN(por_desc)||0;
            total = total-(total*(por_desc/100));
            if(self.find(".por_venta").val()!=''){
                por_venta = parseInt(self.find(".por_venta").val());
                precio_costo = parseInt(self.find('.precio_costo').val());                
                por_venta = precio_costo*(por_venta/100);
                precio_venta = (por_venta+precio_costo).toFixed(0);
                if(por_venta>0)self.find('.precio_venta').val(precio_venta);
            }
            self.find('.total').val(total);
        });
    });
    
    $(document).on('keyup','body',function(event){        
        if(event.which=='113'){
            $("#formulario").submit();
        }
    });
    
    $(document).on('blur','.total',function(){
        totalrecibo = 0;
        $(".total").each(function(){
            if($(this).val()!=''){
                totalrecibo=totalrecibo+parseInt($(this).val())
                $("#totalrecibo").html(totalrecibo);
                $('#total_compra').val(totalrecibo);
                $('#total_descuento').val(0);
            }
        });
    })
    
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');            
            date_init_calendar();
    }
    function removerow(obj){
        $(obj).parent('td').parent('tr').remove();
    }
    
    var ajax = undefined;
    function val_send(form){
       if(ajax==undefined){
        $(".mask").show();
        var data = document.getElementById('formulario');
        $("#guardar").attr('disabled','disabled');
        $(document).trigger('total')
        data = new FormData(data);
        ajax = $.ajax({
            url:'<?= empty($compra)?base_url('json/compras'):base_url('json/compras/edit') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                ajax = undefined;
                data = JSON.parse(data);                
                if(data['status']){
                    document.location.reload();
                }
                else{
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
                    $(".mask").hide();
                }
                $("#guardar").removeAttr('disabled');
            },
            error:function(data){
                $(".mask").hide();
                ajax = undefined;
                emergente('Ocurrio un error intentando guardar el registro, por favor intente de nuevo');
            }
            });
             }
            else emergente('Se esta enviando el registro, por favor espere!!...');
            return false;
    }
    
    $(document).on('keydown','input',function(event){        
        if (event.which === 13){
            if($(this).hasClass('total')){
                addrow($(this));
            }
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select();
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });
    
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    });

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    });
    
    $("body").on('change',".cantidad,.precio_costo,.por_venta,.por_desc",function(){$(document).trigger('total')});
    
    $("body").on('change','.codigo',function(){
        focused = $(this);
        elements = $(".codigo").length;
        focusedcode = $(this).val();
        focusedlote = $(this).parents('tr').find('.lote').val();
        i = 0;
        $(".codigo").each(function(obj){
            if($(this).val()==focusedcode && $(this).parents('tr').find('.lote').val() == focusedlote && !$(this).is(focused))
            {
               row = $(this).parent().parent().find('.cantidad');
               row.val(parseInt(row.val())+1);
               $(this).parents('tr').find('.lote').val();
               $(document).trigger('total');
               i = 0;
            }
            i++;
            if(i==elements){
                $.post('<?= base_url('json/getProduct') ?>',{codigo:$(this).val()},function(data){
                    data = JSON.parse(data);
                    data = data['producto'];
                    if(data!=''){
                    focused = focused.parent().parent();
                    focused.find('.producto').val(data['nombre_comercial'])
                    focused.find('.cantidad').val(1);
                    focused.find('.precio_costo').val(data['precio_costo']);
                    focused.find('.por_venta').val(0);
                    focused.find('.precio_venta').val(data['precio_venta']);
                    focused.find('.total').val(data['precio_venta']);
                    }
                    else{
                        emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
                    }
                });
            }
               
        });
    });

    $(window).keyup(function(e){  
         //alert(e.which);
         if(e.which==27){ //ESC
             $(".modal-footer button").trigger('click');
         }
         if(e.which==115){ //F4             
             $(".pro").parent().find('.chosen-container').addClass('chosen-with-drop');
             $(".pro").parent().find('.chosen-search input').focus();
         }
         if(e.which==119){ //F8
             imprimirCuenta(); 
         }
         if(e.which==113){ //F2
             searchProduct();
         }
     });

     function searchProduct()
    {
        $.post('<?= base_url('json/searchProduct/') ?>',{},function(data){
            emergente(data);
        });
    }
    
    function selCod(cod,lote){
        $("tbody tr:last-child").find('.codigo').val(cod);
        $("tbody tr:last-child").find('.codigo').trigger('change');
        if(lote===''){
            $("tbody tr:last-child").find('.lote').val(lote);
        }
        callbackAfterAddRow = function(){
            if(lote!==''){
                $("tbody tr:last-child").find('.lote').val(lote);
            }
        };
        $('#myModal').modal('hide');
    }    
   
    function tryagain(){
        focused.trigger('change');
        $('#myModal').modal('hide');
        focused.focus();
    }
</script>
<?php $this->load->view('predesign/datepicker') ?>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>