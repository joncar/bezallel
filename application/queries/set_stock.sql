BEGIN
DECLARE stock float;
DECLARE compras float;
DECLARE transferencias_in float;
DECLARE entrada_productos float;
DECLARE ventas float;
DECLARE salidas float;
DECLARE transferencias_ou float;
DECLARE sucursales int;
DECLARE rownumber int;
DECLARE sucursalV int;
DECLARE insumos int;

    SET rownumber = 0;
    SELECT COUNT(id) sucursales FROM sucursales INTO sucursales;

    WHILE rownumber<sucursales DO

    SELECT id from sucursales limit rownumber,1 into sucursalV;
    SET rownumber = rownumber+1;
	
        SELECT 
    IFNULL(SUM(cantidad),0) into compras
    FROM compradetalles 
    INNER JOIN compras ON compras.id = compradetalles.compra 
    WHERE producto = productoV AND compras.sucursal = sucursalV AND compras.status = 0;

	    SELECT
    IFNULL(SUM(cantidad),0) into transferencias_in
    FROM transferencias_detalles
    INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia
    WHERE producto = productoV AND transferencias.sucursal_destino = sucursalV AND transferencias.procesado = 2;

	    SELECT
    IFNULL(SUM(cantidad),0) INTO entrada_productos
    FROM entrada_productos_detalles 
    WHERE entrada_productos_detalles.producto = productoV AND sucursal = sucursalV;

		SELECT 
	IF(inventariable=0,0,IFNULL(SUM(cantidad),0)) INTO ventas
	FROM (
	SELECT
	ventas.id,
	ventadetalle.producto,
	ventadetalle.cantidad,
	productos.inventariable
	FROM ventadetalle 
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	WHERE ventadetalle.producto = productoV AND 
	ventas.sucursal = sucursalV AND 
	(ventas.status IS NULL OR ventas.status = 0)
	UNION ALL
	SELECT 
	ventas.id,
	descontarde.codigo,
	ventadetalle.cantidad * producto_asociado.cant,
	descontarde.inventariable
	FROM ventadetalle
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	INNER JOIN producto_asociado ON producto_asociado.productos_id = productos.id
	INNER JOIN productos as descontarde ON descontarde.id = producto_asociado.descontar
	WHERE descontarde.codigo = productoV AND 
	ventas.sucursal = sucursalV) AS total_venta;
	
        SELECT 
    IFNULL(SUM(cantidad),0) INTO salidas
    FROM salida_detalle
    WHERE salida_detalle.producto = productoV AND salida_detalle.sucursal = sucursalV;

	    SELECT
    IFNULL(SUM(cantidad),0) INTO transferencias_ou
    FROM transferencias_detalles 
    INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia
    WHERE producto = productoV AND transferencias.sucursal_origen = sucursalV AND transferencias.procesado = 2;

    	SELECT 
    IFNULL(SUM(insumos_servicios.cantidad),0) INTO insumos
    FROM insumos_servicios
    INNER JOIN productos ON productos.id = insumos_servicios.productos_id
    WHERE productos.codigo = productoV AND insumos_servicios.sucursales_id = sucursalV;

    SELECT (compras+transferencias_in+entrada_productos)-(ventas+salidas+transferencias_ou+insumos) INTO stock;

    UPDATE productosucursal SET productosucursal.stock = stock WHERE productosucursal.producto = productoV AND productosucursal.sucursal = sucursalV;
    END WHILE;

END