DROP VIEW IF EXISTS view_servicios;
CREATE VIEW view_servicios AS SELECT
servicios_detalles.id,
servicios.id as servicio_id,
date(servicios.fecha) as fecha_entrada,
date(servicios.fecha_a_entregar) as fecha_a_entregar,
clientes.nro_documento as cedula,
clientes.nombres as cliente_nombre ,
clientes.apellidos as cliente_apellido,
productos.nombre_comercial as producto,
servicios.descripcion_trabajo as detalle_trabajo,
servicios_detalles.cantidad as cantidad,
concat_ws(' ',user.nombre, user.apellido) as encargado,
estado_servicio.nombre_estado as estado,
estado_servicio.color,
user.id as user_id
FROM servicios_detalles
INNER JOIN servicios on servicios.id = servicios_detalles.servicios_id
INNER JOIN clientes on clientes.id = servicios.clientes_id
INNER JOIN productos on productos.id = servicios_detalles.productos_id
INNER JOIN estado_servicio on estado_servicio.id = servicios_detalles.estado_servicio_id
LEFT JOIN empleados on empleados.id = servicios.empleados_id
LEFT JOIN user on user.id = empleados.user_id
WHERE (servicios.anulado = 0 or servicios.anulado is null) AND productos.inventariable = 0
ORDER BY date(servicios.fecha_a_entregar) desc