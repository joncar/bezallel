<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->library('html2pdf/html2pdf');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url());
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operaci贸n','403');
                    exit;
                }                
                $this->as = array('usuarios'=>'user','categorias'=>'categoriaproducto');                               
        }

        function validate_caja(){
            if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
                header("Location:".base_url('panel/selsucursal'));
                die();
            }
            if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
                header("Location:".base_url('panel/selcaja'));
                die();
            }
            if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
                header("Location:".base_url('panel/selcajadiaria'));
                die();
            }  
        }

        public function index($url = 'main',$page = 0)
        {
                if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
                    header("Location:".base_url('panel/selsucursal'));
                    die();
                }
                if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
                    header("Location:".base_url('panel/selcaja'));
                    die();
                }
                if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
                    header("Location:".base_url('panel/selcajadiaria'));
                    die();
                }            
                $this->loadView('panel');
        }
        
        public function loadView($param = array('view' => 'main'))
        {
            if(empty($_SESSION['user'])){
                header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            }            
            else{
                if(!empty($param->output)){
                    $param->view = empty($param->view)?'panel':$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                parent::loadView($param);            
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }          
               
        function reportes($crud = '',$x = '',$y = '')
        {            
            $crud->set_relation('controlador','controladores','nombre')
                 ->set_relation('funcion','funciones','nombre');
            $crud->set_relation_dependency('funcion','controlador','controlador');
            $crud->field_type('tipo','true_false',array('0'=>'General','1'=>'Especifico'));
            $crud->field_type('orientacion','dropdown',array('P'=>'Vertical','L'=>'Horizontal'));
            $crud->field_type('papel','dropdown',array('Letter'=>'Carta','A4'=>'A4','otro'=>'Medida'));
            $crud->field_type('variables','products',array(
                'table'=>'reportes_variables',
                'dropdown'=>'tipo_dato',
                'relation_field'=>'reporte',
                'source'=>array('input'=>'input','date'=>'date'),                
            ));
            $crud->add_action('<i title="Imprimir reporte" class="fa fa-print"></i>','',base_url('panel/imprimir_reporte').'/');
            $crud->callback_add_field('margen_izquierdo',function($val){return form_input('margen_izquierdo',5,'id="field-margen_izquierdo"');});
            $crud->callback_add_field('margen_derecho',function($val){return form_input('margen_derecho',5,'id="field-margen_derecho"');});
            $crud->callback_add_field('margen_superior',function($val){return form_input('margen_superior',5,'id="field-margen_superior"');});
            $crud->callback_add_field('margen_inferior',function($val){return form_input('margen_inferior',8,'id="field-margen_inferior"');});
            $crud->required_fields('controlador','funcion','nombre','contenido','tipo','orientacion','papel','margen_izquierdo','margen_derecho',
                  'margen_superior','margen_inferior');
            $crud->columns('tipo','nombre','controlador','funcion');
            if(!empty($_POST) && !empty($_POST['papel']) && $_POST['papel']=='otro')
            {
                $crud->set_rules('ancho','Ancho','required');
                $crud->set_rules('alto','Alto','required');
            }
            
            $crud->callback_before_insert(function($post){$post['contenido'] = base64_encode($post['contenido']); return $post;});
            $crud->callback_before_update(function($post){$post['contenido'] = base64_encode($post['contenido']); return $post;});
            return $crud; 
        }

        function compras($crud = '',$x = '',$y = ''){
            $crud->set_relation('proveedor','proveedores','denominacion');            
            $crud->field_type('descripcion','products',array(
                'table'=>'compradetalles',
                'dropdown'=>'producto_id',
                'relation_field'=>'compra_id',
                'source'=>'productos',
                'field_detalle'=>'nombre_comercial',
                'class'=>array('articulo'=>'chosen-select','codigo'=>'codigoproducto','cantidad'=>'cantidades','precio'=>'precios','total'=>'totales','preciomayorista'=>'p1','preciominorista'=>'p2')
            ));

            return $crud;
        }
                    
        
        //Esto es para la parte de reportes variables
        protected function get_reportes($crud)
        {
            $this->db->where('funciones.nombre',$this->router->fetch_method());           
            $this->db->where('controladores.nombre',$this->router->fetch_class());           
            $this->db->join('controladores','controladores.id = reportes.controlador');            
            $this->db->join('funciones','funciones.id = reportes.funcion');            
            $this->db->select('reportes.*');
            foreach($this->db->get('reportes')->result() as $r){
                if($r->icono!='')
                $crud->add_action('<i title="'.$r->nombre.'" class="fa fa-'.$r->icono.'"></i>','',base_url('panel/imprimir_reporte/'.$r->id).'/');
                else
                $crud->add_action($r->nombre,'',base_url('panel/imprimir_reporte/'.$r->id).'/');
            }
            return $crud;
        }
        
        public function imprimir_reporte($id,$idtable = '')
        {
            $idtable = empty($idtable)?$id:$idtable;
            $reporte = $this->db->get_where('reportes',array('id'=>$id));
            if($reporte->num_rows()>0)
            {
                //Es un reporte con variables? Pues se recogen antes de procesar                
                $variables = $this->db->get_where('reportes_variables',array('reporte'=>$id));
                if($variables->num_rows()>0 && empty($_POST))
                {
                    $output = '<form action="'.base_url('panel/imprimir_reporte/'.$id.'/'.$idtable).'" method="post" class="form-group" role="form" onsubmit="return validar(this)">';
                    $output.= '<h3>Por favor complete los datos para generar el reporte</h3>';
                    //Cargar librerias
                    $date = 0;
                    foreach($variables->result() as $v)
                    {
                        switch($v->tipo_dato)
                        {
                            case 'date': $date = 1; break;                            
                        }
                    }
                    
                    if($date == 1)
                     $output.= $this->load->view('predesign/datepicker',null,TRUE);                    
                    //dibujar forms
                    foreach($variables->result() as $v){
                        switch($v->tipo_dato)
                        {
                            case 'date': $class = 'datetime-input'; break;
                        }
                        $output.= '<div class="form-group">'.form_input($v->variable,'','id="field-'.$v->variable.'" class="form-control '.$class.'" placeholder="'.ucfirst($v->variable).'"').'</div>';
                    }
                    $output.= '<div align="center"><button type="submit" class="btn btn-success">Procesar</button></div>';
                    $output.= '</form>';
                    $this->loadView(array('crud'=>'user','view'=>'panel','output'=>$output));
                }
                else{
                $reporte = $reporte->row();
                $texto = base64_decode($reporte->contenido);
                $texto = str_replace('$_USER',$_SESSION['user'],$texto);
                $texto = str_replace('$_ID',$idtable,$texto);
                //Reemplazar variables recogidas en el formulario
                if($variables->num_rows()>0)
                {
                    foreach($variables->result() as $v)
                    {
                        if(!empty($_POST[$v->variable]))
                        {
                            if($v->tipo_dato=='date')$_POST[$v->variable] = date("Y-m-d",strtotime(str_replace("/","-",$_POST[$v->variable])));
                            $texto = str_replace('$_'.$v->variable,$_POST[$v->variable],$texto);
                        }
                    }
                }
                //Funciones 
                $funciones = fragmentar($texto,'{function=','}');
                foreach($funciones as $f){
                    $texto = call_user_func($f,$idtable,$texto);
                }
                $texto = str_replace("&gt;", '>', $texto);
                $texto = str_replace("&lt;", '<', $texto);
                $sql = fragmentar($texto,'{sql=','}');                
                foreach($sql as $s)
                {
                    list($sentencia,$type) = explode(";",$s);
                    $sentencia = str_replace('"','',$sentencia);                    
                    switch($type)
                    {
                        case 'data':$texto = str_replace('{sql='.$s.'}',sqltodata($this->db->query(strip_tags($sentencia))),$texto);
                        case 'table':$texto = str_replace('{sql='.$s.'}',sqltotable($this->db->query(strip_tags($sentencia))),$texto);
                    }
                }
                $papel = $reporte->papel=='otro'?array($reporte->ancho,$reporte->alto):$reporte->papel;
                $html2pdf = new HTML2PDF($reporte->orientacion,$papel,'fr', false, 'ISO-8859-15', array($reporte->margen_izquierdo, $reporte->margen_superior, $reporte->margen_derecho, $reporte->margen_inferior));
                $html2pdf->writeHTML(utf8_decode($texto));
                $html2pdf->Output($reporte->nombre.'.pdf');
                }
                
            }
            else $this->loadView('404');
        }
        
        function selsucursal($x = '', $y = '')
        {
            unset($_SESSION['caja']);
            unset($_SESSION['cajanombre']);
            unset($_SESSION['cajadiaria']);
            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['sucursal'] = $x;
                $_SESSION['sucursalnombre'] = urldecode(str_replace('-','+',$y));
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel/selcaja'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una sucursal');
            $crud->set_table('sucursales');
            $crud->set_theme('bootstrap2');
            
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('denominacion')
                 ->callback_column('denominacion',function($val,$row){$redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; return '<a href="'.base_url('panel/selsucursal/'.$row->id.'/'.str_replace('+','-',urlencode($val)).$redirect).'">'.$val.'</a>';});
            
            $output = $crud->render();
            $output->crud = 'user';
            $output->view = 'panel';
            $output->title = 'Seleccione una sucursal';
            $output->output = '<h2>Seleccione una sucursal</h2>'.$output->output;
            $this->loadView($output);
            }
        }
        
        function selcaja($x = '',$y = '')
        {
            unset($_SESSION['cajadiaria']);
            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['caja'] = $x;    
                $_SESSION['cajanombre'] = urldecode(str_replace('-','+',$y)); 
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel/selcajadiaria'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una caja');
            $crud->set_table('cajas');
            $crud->set_theme('bootstrap2');
            if(!empty($_SESSION['sucursal']))
                $crud->where('cajas.sucursal',$_SESSION['sucursal']);
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('denominacion')
                 ->callback_column('denominacion',function($val,$row){$redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; return '<a href="'.base_url('panel/selcaja/'.$row->id.'/'.str_replace('+','-',urlencode($val)).$redirect).'">'.$val.'</a>';});            
            
            $output = $crud->render();
            $output->crud = 'user';
            $output->view = 'panel';
            $output->title = 'Seleccione una caja';
            $output->output = '<h2>Seleccione una caja</h2>'.$output->output;
            $this->loadView($output);
            }
        }
        
        function selcajadiaria($x = '')
        {
            if(empty($_SESSION['caja'])){
                redirect('panel/selcaja');
            }
            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['cajadiaria'] = $x;
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una cajadiaria');
            $crud->set_table('cajadiaria');
            $crud->set_theme('bootstrap2');
            if(!empty($_SESSION['caja']))
                $crud->where('caja',$_SESSION['caja']);
            $crud->where('abierto',1);
            if ($this->db->get_where('cajadiaria', array('caja' => $_SESSION['caja'], 'sucursal' => $_SESSION['sucursal'], 'abierto' => 1))->num_rows() > 0) {
                $crud->unset_add();
                $crud->where('cajadiaria.abierto', 1);
            }
            if($x=='add'){
                redirect('cajas/admin/cajadiaria/add');
            }
            $crud->unset_delete()                 
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('fecha_apertura','monto_inicial','fecha_cierre','correlativo')
                 ->callback_column('fecha_apertura',function($val,$row){$redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; return '<a href="'.base_url('panel/selcajadiaria/'.$row->id.$redirect).'">'.$val.'</a>';});
            
            $output = $crud->render();
            $output->crud = 'user';
            $output->view = 'panel';
            $output->title = 'Seleccione una caja diaria';
            $output->output = '<h2>Seleccione una caja diaria</h2>'.$output->output;
            $this->loadView($output);
            }
        }
        //JSONS                   
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
