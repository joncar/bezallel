<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Panel.php');
class Json extends Panel {
        
	function __construct()
	{
		parent::__construct();
	}
        
        function getProduct($sucursal = ''){
            $this->form_validation->set_rules('codigo','Codigo','required');
            if($this->form_validation->run())
            {
                $disponible = 0;
                if(!empty($_SESSION['sucursal'])){  
                    $this->db->order_by('vencimiento','ASC');
                    if(isset($_POST['lote']))
                    {
                        foreach(explode(",",$_POST['lote']) as $l)
                        $this->db->where('productosucursal.lote != ',$l);
                    }
                    $sucursal = empty($sucursal)?$_SESSION['sucursal']:$sucursal;
                    $disponible = $this->db->get_where('productosucursal',array('sucursal'=>$sucursal,'producto'=>trim($this->input->post('codigo')),'stock >'=>0));
                    $dis = 0;
                    foreach($disponible->result() as $d){
                        $dis += $d->stock;
                    }
                    
                    //Enviar la lista del lote
                    //$this->db->order_by('vencimiento','ASC');
                    $lotelist = array();
                    foreach($this->db->get_where('productosucursal',array('sucursal'=>$sucursal,'producto'=>$this->input->post('codigo'),'stock >' => 0,'lote != '=>'','vencimiento != '=>'0000-00-00'))->result() as $l){
                        $lotelist[$l->lote] = $l->lote;
                    }
                    $lotelist = empty($lotelist)?array(''):$lotelist;
                    $lote = isset($disponible->row()->lote)?$disponible->row()->lote:'';
                    $loted = isset($disponible->row()->lote)?$disponible->row()->stock:'';                    
                    $disponible = $dis;                    
                }
                $producto = $this->db->get_where('productos',array('codigo'=>$this->input->post('codigo'),'anulado <'=>1));
                if($producto->num_rows()>0){
                    $pro['producto'] = $producto->row();
                    $pro['producto']->disponible = $disponible;
                    if(isset($lote)){
                        $pro['producto']->lote = $lote;
                    }
                    if(isset($lote)){
                        $pro['producto']->loted = $loted;
                        
                    }
                    $pro['producto']->lotelist = !empty($lotelist)?form_dropdown('lote[]',$lotelist,'0','class="lote form-control" id="lote" name="lote[]" data-name="lote"'):'';
                    
                    $where = array('producto'=>$pro['producto']->codigo);
                    if(!empty($pro['producto']->lote)){
                        $where['lote'] = $pro['producto']->lote;
                    }
                    $pro['producto']->precio_costo = $this->db->get_where('compradetalles',$where);
                    $pro['producto']->precio_costo = $pro['producto']->precio_costo->num_rows()>0?$pro['producto']->precio_costo->row()->precio_costo:0;

                    $pro['producto']->rangos = array();
                    $this->db->order_by('id','ASC');
                    $_POST['tipoprecioid'] = empty($_POST['tipoprecioid'])?'':$_POST['tipoprecioid'];
                    foreach($this->db->get_where('categoria_precios',array('tipo_precios_id'=>$_POST['tipoprecioid'],'productos_id'=>$pro['producto']->id))->result() as $p){
                        $p->codigo = $pro['producto']->codigo;
                        $p->precio_original = $pro['producto']->precio_venta;
                        $pro['producto']->rangos[] = $p;
                    }
                    
                    echo json_encode($pro);
                }
                else 
                    echo json_encode(array('producto'=>''));
                
            }
            else echo json_encode(array('producto'=>''));
        }       
        
        function getCompra(){
            $this->form_validation->set_rules('codigo','Codigo','required');
            if($this->form_validation->run())
            {
                if($_POST['codigo']!='N/A')$this->db->where('id',$this->input->post('codigo'));
                if(isset($_POST['proveedor']) && $_POST['proveedor']!=''){
                    $this->db->where('proveedor',$this->input->post('proveedor'));                    
                    $this->db->where('status',0);
                }
                $producto = $this->db->get('compras');
                if($producto->num_rows()>0){
                    $pro['producto'] = $producto->row();
                    $pro['producto']->fecha = date("d/m/Y",strtotime($pro['producto']->fecha));
                    echo json_encode($pro);
                }
                else 
                    echo json_encode(array('producto'=>''));
                
            }
            else echo json_encode(array('producto'=>''));
        }   
        
         function getVenta(){
            $this->form_validation->set_rules('codigo','Codigo','required');
            if($this->form_validation->run())
            {
                if($_POST['codigo']!='N/A')$this->db->where('id',$this->input->post('codigo'));
                if(!empty($_POST['cliente'])){
                    $this->db->where('cliente',$this->input->post('cliente'));
                    $this->db->where('transaccion',2);
                    $this->db->where('status',0);
                }
                
                $producto = $this->db->get('ventas');
                if($producto->num_rows()>0){
                    $pro['producto'] = $producto->row();
                    $pro['producto']->fecha = date("d/m/Y",strtotime($pro['producto']->fecha));
                    $total = 0;
                    $this->db->select('pagoclientesfactura.*');
                    $this->db->join('pagoclientesfactura','pagoclientesfactura.pagocliente = pagocliente.id');
                    foreach($this->db->get_where('pagocliente',array('venta'=>$pro['producto']->id))->result() as $p)
                        $total+=$p->total;
                    $pro['producto']->total_venta-=$total;
                    echo json_encode($pro);
                }
                else 
                    echo json_encode(array('producto'=>''));
                
            }
            else echo json_encode(array('producto'=>''));
        }    
        /*Cruds*/     
        function compras($edit = 'add',$x = '',$y = '')
        {
            if(!empty($_SESSION['user'])){
            foreach($_POST as $p=>$v){
                if(!is_array($v)){
                    $this->form_validation->set_rules($p,$p,'required');
                }
            }                        
            
            $err = '';            
            if(isset($_POST['codigo'])){
                foreach($_POST['codigo'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un producto</p>';
                }
            }
            else $err = '<p>Se debe incluir al menos un producto</p>';
            
            if($edit=='add' && $this->db->get_where('compras',array('nro_factura'=>$_POST['nro_factura']))->num_rows()>0)
                    $err.='El numero de factura ya se encuentra registrado';
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));
                $data['vencimiento_pago'] = date("Y-m-d",strtotime(str_replace("/","-",$data['vencimiento_pago'])));
                //$data['sucursal'] = $this->db->get_where('sucursales',array('principal'=>1))->row()->id; Cambiado para la sucursal seleccionada
                $data['sucursal'] = $_POST['sucursal'];
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                if($edit=='edit'){
                    $this->db->update('compras',$data,array('nro_factura'=>$data['nro_factura']));
                    $id = $this->db->get_where('compras',array('nro_factura'=>$data['nro_factura']))->row()->id;
                }
                else{
                    $this->db->insert('compras',$data);
                    $id = $this->db->insert_id();
                    foreach($_POST['codigo'] as $n=>$v){
                        if(!empty($v)){
                            $data = array('compra'=>$id);
                            $data['producto'] = strtoupper($v);
                            $data['lote'] = $_POST['lote'][$n];
                            $data['vencimiento'] = date("Y-m-d",strtotime(str_replace("/","-",$_POST['vencimiento'][$n])));
                            $data['cantidad'] = $_POST['cantidad'][$n];
                            $data['precio_costo'] = $_POST['precio_costo'][$n];
                            $data['por_venta'] = $_POST['por_venta'][$n];
                            $data['por_desc'] = $_POST['por_desc'][$n];
                            $data['precio_venta'] = $_POST['precio_venta'][$n];
                            $data['total'] = $_POST['total'][$n];
                            $this->db->insert('compradetalles',$data);

                            //Actualizar stock
                            $stock = $this->db->get_where('productos',array('codigo'=>$v))->row()->stock;
                            $stock+=$data['cantidad'];
                            $this->db->update('productos',array('stock'=>$stock,'precio_venta'=>$_POST['precio_venta'][$n],'precio_costo'=>$_POST['precio_costo'][$n]),array('codigo'=>$v));
                            //Envar productos a la sucursal principal
                            //$principal = $this->db->get_where('sucursales',array('principal'=>1));
                            $principal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']));
                            if($principal->num_rows()>0){
                                $this->db->where('lote',$_POST['lote'][$n]);
                                $stock = $this->db->get_where('productosucursal',array('sucursal'=>$principal->row()->id,'producto'=>$v));
                                if($stock->num_rows()>0){
                                    $stock = $stock->row()->stock;
                                    $stock+=$data['cantidad'];

                                    $this->db->update('productosucursal',array('fechaalta'=>date("Y-m-d H:i:s"),'proveedor_id'=>$_POST['proveedor'],'vencimiento'=>$data['vencimiento'],'stock'=>$stock,'precio_venta'=>$_POST['precio_venta'][$n],'precio_costo'=>$_POST['precio_costo'][$n]),array('sucursal'=>$principal->row()->id,'producto'=>$v,'lote'=>$data['lote']));
                                }
                                else{
                                    $this->db->insert('productosucursal',array('fechaalta'=>date("Y-m-d H:i:s"),'proveedor_id'=>$_POST['proveedor'],'vencimiento'=>$data['vencimiento'],'stock'=>$data['cantidad'],'precio_venta'=>$_POST['precio_venta'][$n],'precio_costo'=>$_POST['precio_costo'][$n],'sucursal'=>$principal->row()->id,'producto'=>$v,'lote'=>$_POST['lote'][$n],'precio_venta'=>$_POST['precio_venta'][$n]));
                                }
                            }

                        }
                    }
                }                
                $_SESSION['msj'] = 'Se han guardado los datos con exito';
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            }
            else echo json_encode(array('status'=>FALSE,'message'=>'<script>document.location.href="'+base_url('panel')+'"</script>'));
        }
        
        function distribucion(){
            foreach($_POST as $p=>$v){    
                if($p!='lote')
                $this->form_validation->set_rules($p,$p,'required');
            }
            
            if($this->form_validation->run()){
                foreach($this->db->get('sucursales')->result() as $s){
                    if(isset($_POST['sucursal_'.$s->id])){
                        $data = array();
                        $data['compra'] = $_POST['compra'];
                        $data['producto'] = $_POST['producto'];
                        $data['sucursal'] = $s->id;
                        $data['cantidad'] = $_POST['sucursal_'.$s->id];
                        $data['stock'] = $_POST['sucursal_'.$s->id];
                        $data['lote'] = $_POST['lote'];
                        $data['vencimiento'] = $_POST['vencimiento'];                                                
                        $data['precio_venta'] = $_POST['precio_venta'];                                                
                        $distribucion = $this->db->get_where('distribucion',array('compra'=>$_POST['compra'],'producto'=>$_POST['producto'],'sucursal'=>$s->id));
                        
                        
                        $where = array('producto'=>$data['producto'],'sucursal'=>$data['sucursal']);
                        if(!empty($data['lote']))$where['lote'] = $data['lote'];
                        $producto = $this->db->get_where('productosucursal',$where);
                        
                        if($distribucion->num_rows()==0 || $distribucion->row()->cantidad<$_POST['sucursal_'.$s->id]){
                            $stock = $distribucion->num_rows()>0?$_POST['sucursal_'.$s->id] - $distribucion->row()->cantidad:$_POST['sucursal_'.$s->id];
                            if($producto->num_rows()==0)
                            $this->db->insert('productosucursal',$data);
                            else
                            $this->db->update('productosucursal',array('stock'=>$producto->row()->stock+$stock,'precio_venta'=>$_POST['precio_venta']),$where);                        

                            $this->db->delete('distribucion',array('compra'=>$_POST['compra'],'producto'=>$_POST['producto'],'sucursal'=>$s->id));
                            $this->db->insert('distribucion',$data);
                        }
                    }
                }
                
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string()));
        }
        
        function seldistribucionproducto($compra,$producto){
            $str = '<script>';
            foreach($this->db->get('sucursales')->result() as $s){
                $cantidad = $this->db->get_where('distribucion',array('producto'=>$producto,'sucursal'=>$s->id,'compra'=>$compra));
                if($cantidad->num_rows()>0){
                    $str .= '$("#sucursal_'.$s->id.'").val(\''.$cantidad->row()->cantidad.'\'); ';                    
                }
                else
                    $str .= '$("#sucursal_'.$s->id.'").val(0); ';
            }
            
            $str.= '</script>';
            echo $str;
        }
        
        //Ventas
        function clienteautocomplete()
        {
            $this->form_validation->set_rules('query','Query','required');
            if($this->form_validation->run()){
                $result = array();
                $this->db->like('nombres',$_POST['query']);
                $this->db->or_like('apellidos',$_POST['query']);
                $this->db->or_like('nro_documento',$_POST['query']);
                foreach($this->db->get('clientes')->result() as $q){
                    array_push($result,$q->id.'-'.$q->nombres.' '.$q->apellidos);
                }
                echo json_encode($result);
            }
            else echo json_encode(array(''));
        }
        
        function ventas($edit = 'add'){

            if(!empty($_SESSION['user'])){
            $err = '';                         
            $productos = array();
            if(isset($_POST['codigo_0'])){                
                for($i=0;$i<$_POST['filas'];$i++){
                    if(isset($_POST['codigo_'.$i])){
                        //Creamos el array desde aqui
                        if(
                            !isset($_POST['lote_'.$i]) ||
                            !isset($_POST['cantidad_'.$i]) ||
                            !isset($_POST['por_desc_'.$i]) ||
                            !isset($_POST['precio_venta_'.$i]) ||
                            !isset($_POST['precio_desc_'.$i]) ||
                            !isset($_POST['total_'.$i]) ||
                            !isset($_POST['ivah_'.$i])
                        ){
                            if(!empty($_POST['codigo_'.$i])){
                                $err.= '<p>Incongruencia en los datos recibidos</p>';
                            }
                        }else{
                            $productos[$i] = array();
                            $productos[$i]['codigo'] = $_POST['codigo_'.$i];                            
                            $productos[$i]['lote'] = $_POST['lote_'.$i]=='0'?'':$_POST['lote_'.$i];
                            $productos[$i]['cantidad'] = $_POST['cantidad_'.$i];                            
                            $productos[$i]['por_desc'] = $_POST['por_desc_'.$i];                            
                            $productos[$i]['precio_venta'] = $_POST['precio_venta_'.$i];                            
                            $productos[$i]['precio_desc'] = $_POST['precio_desc_'.$i];                            
                            $productos[$i]['totalsindesc'] = 0;
                            $productos[$i]['total'] = $_POST['total_'.$i];                            
                            $productos[$i]['ivah'] = $_POST['ivah_'.$i];                             
                            $productos[$i]['categoria_precios_id'] = $_POST['categoria_precios_id_'.$i];                             
                        }
                        unset($_POST['codigo_'.$i]);
                        unset($_POST['lote_'.$i]);
                        unset($_POST['cantidad_'.$i]);
                        unset($_POST['por_desc_'.$i]);
                        unset($_POST['precio_venta_'.$i]);
                        unset($_POST['precio_desc_'.$i]);
                        unset($_POST['total_'.$i]);
                        unset($_POST['ivah_'.$i]);
                        unset($_POST['producto_'.$i]);
                        unset($_POST['disponible_'.$i]);
                        unset($_POST['categoria_precios_id_'.$i]);
                    }
                }
            }
            else{
                $err .= '<p>Se debe incluir al menos un producto</p>';
            }
            unset($_POST['filas']);
            foreach($_POST as $p=>$v){
                if($p!='observacion'){
                    if($p=='total_venta'){                    
                        $this->form_validation->set_rules($p,$p,'required|integer|greater_than[0]');
                    }
                    else
                        $this->form_validation->set_rules($p,$p,'required');
                }
            }
            //if($x==1)$err.= '<p>La cantidad de los productos deben ser mayores a 0</p>';
            
            if($edit=='add' && $this->db->get_where('ventas',array('nro_factura'=>$_POST['nro_factura']))->num_rows()>0)
                    $err.='<p>El numero de factura ya se encuentra registrado</p>';
            
            if($_POST['total_efectivo']==0 && $_POST['total_debito']==0 && $_POST['total_credito']==0 && $_POST['total_cheque']==0 && $_POST['transaccion']==1)
                $err.= '<p>No se ha realizado el pago de esta factura</p>';
            else{
                $deduccion = $_POST['total_venta'] - $_POST['total_efectivo'] - $_POST['total_debito'] - $_POST['total_credito'] - $_POST['total_cheque'];
                if($deduccion > 0 && $_POST['transaccion']==1)
                    $err .= '<p>No se ha realizado el pago de esta factura en su totalidad. Restan: <b>'.$deduccion.'</b></p>';
            }
            
            //Validamos el lote
            foreach($productos as $n=>$p){
                if(!empty($p['codigo'])){
                    $producto = $this->db->get_where('productos',array('codigo'=>$p['codigo']));
                    /*$this->db->select('productosucursal.*, productos.inventariable');
                    $this->db->join('productos','productos.codigo = productosucursal.producto');*/
                    $this->db->where('productosucursal.producto',$p['codigo']);
                    $this->db->where('productosucursal.sucursal',$_SESSION['sucursal']);
                    $this->db->where('productosucursal.stock >',0);
                    if(empty($p['lote'])){
                        $this->db->where("(productosucursal.lote=' ' OR productosucursal.lote='0')",null,TRUE);
                    }else{
                        $this->db->where("productosucursal.lote",$p['lote']);
                    }
                    $l = $this->db->get('productosucursal');
                    if($l->num_rows()==0 && ($producto->num_rows()>0 && $producto->row()->inventariable==1)){
                        $err.= '<p>La sucursal seleccionada no posee el producto '.$p['codigo'].' con lote "'.$p['lote'].'" en existencia, verifique el lote del mismo</p>';
                    }else{
                        if($producto->row()->inventariable==1){
                            $productos[$n]['lote'] = $l->row()->lote;
                        }
                    }
                }
            }
                        
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v) && $p!='observaciones')$data[$p] = $v;
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));
                $data['sucursal'] = $_SESSION['sucursal'];
                $data['caja'] = $_SESSION['caja'];
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                $data['usuario'] = $_SESSION['user'];
                if($edit=='edit'){
                    $this->db->update('ventas',$data,array('nro_factura'=>$data['nro_factura']));
                    $id = $this->db->get_where('ventas',array('nro_factura'=>$data['nro_factura']))->row()->id;                    
                }
                else{
                    $this->db->insert('ventas',$data);
                    $id = $this->db->insert_id();
                    foreach($productos as $n=>$v){
                        if(!empty($v['codigo'])){
                            $data = array('venta'=>$id);
                            $data['producto'] = $v['codigo'];
                            $data['lote'] = $v['lote'];
                            $data['cantidad'] = $v['cantidad'];
                            $data['pordesc'] = $v['por_desc'];
                            $data['precioventa'] = $v['precio_venta'];
                            $data['precioventadesc'] = $v['precio_desc'];
                            $data['totalsindesc'] = 0;
                            $data['totalcondesc'] = $v['total'];
                            $data['iva'] = $v['ivah'];
                            $this->db->insert('ventadetalle',$data);

                            //Actualizar stock
                            $producto = $this->db->get_where('productos',array('codigo'=>$v['codigo']));
                            if($producto->row()->inventariable==1){
                                $where = array('producto'=>$v['codigo'],'sucursal'=>$_SESSION['sucursal'],'stock >'=>0);
                                if(!empty($data['lote']))$where['lote'] = $data['lote'];
                                $stock = $this->db->get_where('productosucursal',$where);
                                $st = $data['cantidad'];
                                foreach($stock->result() as $s){  
                                    if($st<0)$st = $st*-1;
                                    $st = $s->stock-$st;
                                    $st2 = $st<0?0:$st;
                                    $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$s->id));                            
                                }
                            }
                        }
                    }

                    $caja = $this->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'abierto'=>1));
                    $correlativo = $caja->row()->correlativo+1;
                    $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));

                    if(!empty($_POST['servicios_id']) && is_numeric($_POST['servicios_id'])){
                        //Actualizamos la venta en servicios id y facturado = 0
                        $servicio = $this->db->get_where('servicios',array('id'=>$_POST['servicios_id']));
                        if($servicio->num_rows()>0){
                            $this->db->update('servicios',array('facturado'=>1,'nro_factura'=>$id),array('id'=>$_POST['servicios_id']));
                        }
                    }
                }                
                $_SESSION['msj'] = 'Se han guardado los datos con exito <a href="javascript:imprimir(\''.$id.'\')">Imprimir Factura</a> | <a href="javascript:imprimir2(\''.$id.'\')">Imprimir Factura | Lineas</a> | <a href="javascript:imprimirTicket(\''.$id.'\')">Imprimir Ticket</a> <script>$(document).ready(function(){showButton('.$id.')});</script>';

                if($_POST['transaccion']==2){
                    $modulo_credito_activo = $this->db->get('ajustes')->row();

                    if(!empty($modulo_credito_activo->plan_credito)){
                        if($modulo_credito_activo->plan_credito==1){
                            $_SESSION['msj'] = 'Se han guardado los datos con exito <script>document.location.href="'.base_url('movimientos/creditos/creditos/'.$id).'/add";</script>';
                        }else{
                            $_SESSION['msj'] = 'Se han guardado los datos con exito <script>document.location.href="'.base_url('cajas/admin/abonarDeuda/'.$id).'";</script>';
                        }
                    }                    

                }
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            }
            else echo json_encode(array('status'=>FALSE,'message'=>'<script>document.location.href="'+base_url('panel')+'"</script>'));
            
        }
        
        function searchProduct($sucursal = '')
        {
            $page = !isset($_POST['page'])?0:$_POST['page'];
            $page = $page*100;
            $sucursal = empty($sucursal) && !empty($_SESSION['sucursal'])?$_SESSION['sucursal']:$sucursal;
            $this->db->limit(100,$page);
            if(!empty($_POST['codigo']))
                $this->db->like('codigo',$_POST['codigo']);
            if(!empty($_POST['nombre']))
                $this->db->like('nombre_comercial',$_POST['nombre']);
            if(!empty($_POST['nombre2']))
                $this->db->like('nombre_generico',$_POST['nombre2']);
            $this->db->where('productos.anulado <',1);
            //if(!empty($sucursal)){
                $this->db->select('productosucursal.stock as st,sucursales.denominacion as sucursal,sucursales.id as sucursal_id, productosucursal.lote, productosucursal.vencimiento, productos.*');
                $this->db->join('productos','productos.codigo = productosucursal.producto','RIGHT');
                $this->db->join('sucursales','sucursales.id = productosucursal.sucursal','LEFT');
                /*$data = array('productosucursal.stock > '=>0);*/
                if(!empty($sucursal)){
                    //$sucursalProduct = '(productosucursal.stock > 0 AND productosucursal.sucursal = '.$sucursal.')';
                    $sucursalProduct = '(productosucursal.sucursal = '.$sucursal.')';
                }else{
                    //$sucursalProduct = 'productosucursal.stock > 0';
                }

                $this->db->where('(productos.inventariable = 0 OR '.$sucursalProduct.')',NULL,FALSE);                
                $productos = $this->db->get_where('productosucursal');
            /*}
            else
            $productos = $this->db->get('productos');*/
            $data = array('productos'=>$productos);
            if($sucursal!=''){
                $data['sucursal'] = $sucursal;
            }
            if(!isset($_POST['page']) && !isset($_POST['codigo']) && !isset($_POST['nombre']))
            $this->load->view('searchProduct',$data);
            else
            $this->load->view('searchProduct2',$data);
        }
        
        function transferencias($edit = 'add'){
            $err = ''; 
            $productos = array();
            $sucursal_origen = $_POST['sucursal_origen'];
            if(isset($_POST['codigo_0'])){                
                for($i=0;$i<$_POST['filas'];$i++){
                    if(isset($_POST['codigo_'.$i])){
                        //Creamos el array desde aqui
                        if(
                            !isset($_POST['lote_'.$i]) ||
                            !isset($_POST['cantidad_'.$i])
                        ){
                            if(!empty($_POST['codigo_'.$i])){
                                $err.= '<p>Incongruencia en los datos recibidos</p>';
                            }
                        }else{
                            $productos[$i] = array();
                            $productos[$i]['codigo'] = $_POST['codigo_'.$i];
                            $productos[$i]['lote'] = $_POST['lote_'.$i]=='0'?'':$_POST['lote_'.$i];
                            $productos[$i]['cantidad'] = $_POST['cantidad_'.$i];
                        }
                        unset($_POST['codigo_'.$i]);
                        unset($_POST['lote_'.$i]);
                        unset($_POST['cantidad_'.$i]);
                        unset($_POST['producto_'.$i]);
                    }
                }
            }
            else{
                $err .= '<p>Se debe incluir al menos un producto</p>';
            }
            unset($_POST['filas']);
            foreach($_POST as $p=>$v){                                
               $this->form_validation->set_rules($p,$p,'required');
            }
            
            //Validamos el lote
            foreach($productos as $n=>$p){
                if(!empty($p['codigo'])){
                    $this->db->where('producto',$p['codigo']);
                    $this->db->where('sucursal',$sucursal_origen);
                    $this->db->where('stock >',0);
                    if(empty($p['lote'])){
                        $this->db->where("(lote=' ' OR lote='0')",null,TRUE);
                    }else{
                        $this->db->where("lote",$p['lote']);
                    }
                    $l = $this->db->get('productosucursal');
                    if($l->num_rows()==0){
                        $err.= '<p>La sucursal seleccionada no posee el producto '.$p['codigo'].' con lote "'.$p['lote'].'" en existencia, verifique el lote del mismo</p>';
                    }else{
                        $productos[$n]['lote'] = $l->row()->lote;
                    }
                }
            }
            if($this->form_validation->run() && empty($err))
            {
                $data = array(); 
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha_solicitud'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha_solicitud'])));
                $data['sucursal_destino'] = empty($_POST['sucursal_destino'])?$_SESSION['sucursal']:$_POST['sucursal_destino'];
                $data['procesado'] = 0;
                if($edit=='edit'){
                    $this->db->update('transferencias',$data,array('id'=>$data['id']));
                    $id = $this->db->get_where('transferencias',array('id'=>$data['id']))->row()->id;
                    $this->db->delete('transferencias_detalles',array('transferencia'=>$id));
                }
                else{
                    $this->db->insert('transferencias',$data);
                    $id = $this->db->insert_id();
                }
                foreach($productos as $n=>$v){
                    if(!empty($v['codigo'])){
                        $data = array('transferencia'=>$id);                    
                        $data['producto'] = $v['codigo'];
                        $data['lote'] = $v['lote'];
                        $data['cantidad'] = $v['cantidad'];
                        $this->db->insert('transferencias_detalles',$data);
                    }
                }
                $_SESSION['msj'] = 'Se han guardado los datos de esta transferencia <a href="javascript:imprimir(\''.$id.'\')">Imprimir Comprobante</a><script>showButton('.$id.')</script>';
                echo json_encode(array('status'=>TRUE));
            }
            else{
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            }
        }
        
        function transferencia_status_change()
        {
            $this->form_validation->set_rules('id','ID','required');
            $this->form_validation->set_rules('val','Valor','required');
            if($this->form_validation->run()){
                $transferencia = $this->db->get_where('transferencias',array('id'=>$this->input->post('id')));
                if($transferencia->num_rows()>0){
                    $transferencia = $transferencia->row();
                    if($transferencia->sucursal_origen==$_SESSION['sucursal'] && ($_POST['val']==-1 || $_POST['val'] == 1)){
                        $this->db->update('transferencias',array('procesado'=>$_POST['val']),array('id'=>$_POST['id']));
                        //Descontar del stock
                        if($_POST['val']==1){
                            $detalles = $this->db->get_where('transferencias_detalles',array('transferencia'=>$_POST['id']));
                            foreach($detalles->result() as $d){
                                $where = array('producto'=>strtoupper($d->producto),'sucursal'=>$transferencia->sucursal_origen,'stock >'=>0);
                                if(!empty($d->lote))$where['lote'] = $d->lote;
                                $stock = $this->db->get_where('productosucursal',$where);
                                $st = $d->cantidad;
                                foreach($stock->result() as $s){  
                                    if($st<0)$st = $st*-1;
                                    $st = $s->stock-$st;
                                    $st2 = $st<0?0:$st;
                                    $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$s->id));                            
                                }
                            }
                        }
                        echo $this->success('Solicitud Enviada').refresh_list();
                    }
                    elseif($transferencia->sucursal_destino==$_SESSION['sucursal'] && $_POST['val']==2){
                        $this->db->update('transferencias',array('procesado'=>$_POST['val']),array('id'=>$_POST['id']));
                        $this->db->select('transferencias_detalles.*, productos.proveedor_id, productos.precio_venta');
                        $this->db->join('productos','productos.codigo = transferencias_detalles.producto');
                        $detalles = $this->db->get_where('transferencias_detalles',array('transferencia'=>$_POST['id']));
                            foreach($detalles->result() as $d){
                                //Buscamos el inventario origen
                                $where = array('producto'=>strtoupper($d->producto),'sucursal'=>$transferencia->sucursal_origen);                                
                                if(!empty($d->lote))$where['lote'] = $d->lote;
                                $inventario_origen = $stock = $this->db->get_where('productosucursal',$where)->row();
                                //Buscamos en el inventario destino
                                $where = array('producto'=>strtoupper($d->producto),'sucursal'=>$transferencia->sucursal_destino);
                                if(!empty($d->lote))$where['lote'] = $d->lote;
                                $stock = $this->db->get_where('productosucursal',$where);                                
                                if($stock->num_rows()>0)
                                    //Actualiza
                                    $this->db->update('productosucursal',array('stock'=>($stock->row()->stock+$d->cantidad)),array('id'=>$stock->row()->id));
                                else{
                                    //Inserta
                                    $data = $where;
                                    $data['stock'] = $d->cantidad;                                    
                                    $data['proveedor_id'] = $inventario_origen->proveedor_id; 
                                    $data['precio_venta'] = $d->precio_venta;
                                    $data['fechaalta'] = date("Y-m-d H:i:s");
                                    if(!empty($inventario_origen->vencimiento))$data['vencimiento'] = $inventario_origen->vencimiento;
                                    $this->db->insert('productosucursal',$data);
                                } 
                            }
                        echo $this->success('Solicitud Enviada').refresh_list();
                    }
                    else echo $this->error ('Operación no permitida');
                }
                else echo $this->error ('Ha ocurrido un error en la operación, consulte con soporte técnico');
            }
            else echo $this->error ('Ha ocurrido un error en la operación, consulte con soporte técnico');
        }
        
        function clientes(){
            $str = '';
            $this->db->order_by('id','DESC');
            foreach($this->db->get('clientes')->result() as $c)
                $str.= '<option value="'.$c->id.'">'.$c->nro_documento.' '.$c->nombres.' '.$c->apellidos.'</option>';
            echo $str;
        }
        
        function salidas($edit = 'add'){
            $err = ''; 
            foreach($_POST as $p=>$v){
                if(!is_array($v)){
                    $this->form_validation->set_rules($p,$p,'required');                
                }
            }
            
            if(isset($_POST['codigo'])){
                $x = 0;
                foreach($_POST['codigo'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un producto</p>';
                    //if($v==0)$x = 1;
                }
            }
            else $err .= '<p>Se debe incluir al menos un producto</p>';
            if($x==1)$err.= '<p>La cantidad de los productos deben ser mayores a 0</p>';
            
            if(empty($edit) && $this->db->get_where('salidas',array('id'=>$_POST['id']))->num_rows()>0)
                    $err.='<p>El numero de salida ya se encuentra registrado</p>';
            
            if($_POST['total_costo']==0)
                $err.= '<p>No se ha realizado el calculo de esta salida</p>';
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));
                $data['usuario'] = $_SESSION['user'];
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                if($edit=='edit'){
                    $this->db->update('salidas',$data,array('id'=>$data['id']));
                    $id = $data['id'];
                    $this->db->delete('salida_detalle',array('salida'=>$id));
                }
                else{
                    $this->db->insert('salidas',$data);
                    $id = $this->db->insert_id();
                }
                foreach($_POST['codigo'] as $n=>$v){
                    if(!empty($v)){
                        $data = array('salida'=>$id);
                        $data['producto'] = $v;
                        $data['lote'] = $_POST['lote'][$n];                        
                        $data['vencimiento'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$_POST['vencimiento'][$n])));
                        $data['cantidad'] = $_POST['cantidad'][$n];
                        $data['precio_costo'] = $_POST['precio_costo'][$n];
                        $data['total'] = $_POST['total'][$n]; 
                        $data['sucursal'] = $_POST['sucursal'][$n];                       
                        $this->db->insert('salida_detalle',$data);
                        
                        //Actualizar stock
                        if($edit=='add'){
                            $where = array('producto'=>$v,'stock >'=>0);
                            if(!empty($data['lote']))$where['lote'] = $data['lote'];
                            $stock = $this->db->get_where('productosucursal',$where);
                            $st = $data['cantidad'];
                            foreach($stock->result() as $s){  
                                if($st<0)$st = $st*-1;
                                $st = $s->stock-$st;
                                $st2 = $st<0?0:$st;
                                $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$s->id));                            
                            }
                        }
                    }
                }
                $_SESSION['msj'] = 'Se han guardado los datos con exito <a href="javascript:imprimir(\''.$id.'\')">Imprimir Recibo</a> <script>showButton('.$id.')</script>';
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            
        }
        
        /*Cruds*/     
        function entradas($edit = 'add')
        {
            foreach($_POST as $p=>$v){
                if(!is_array($v)){
                    $this->form_validation->set_rules($p,$p,'required');
                }
            }                        
            
            $err = '';            
            if(isset($_POST['codigo'])){
                foreach($_POST['codigo'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un producto</p>';
                }
            }
            else $err = '<p>Se debe incluir al menos un producto</p>';                        
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v)){
                        $data[$p] = $v;                        
                    }
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));                
                $data['usuario'] = $_SESSION['user'];
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                if($edit=='edit'){
                    $this->db->update('entrada_productos',$data,array('id'=>$data['id']));
                    $id = $this->db->get_where('entrada_productos',array('id'=>$data['id']))->row()->id;
                    $this->db->delete('entrada_productos_detalles',array('entrada_producto'=>$id));
                }
                else{
                    $this->db->insert('entrada_productos',$data);
                    $id = $this->db->insert_id();
                }
                foreach($_POST['codigo'] as $n=>$v){
                    if(!empty($v)){
                        $data = array('entrada_producto'=>$id);
                        $data['producto'] = strtoupper($v);
                        $data['lote'] = $_POST['lote'][$n];
                        $data['vencimiento'] = date("Y-m-d",strtotime(str_replace("/","-",$_POST['vencimiento'][$n])));
                        $data['cantidad'] = $_POST['cantidad'][$n];
                        $data['precio_costo'] = $_POST['precio_costo'][$n];
                        $data['precio_venta'] = $_POST['precio_venta'][$n];
                        $data['total'] = $_POST['total'][$n];
                        $data['sucursal'] = $_POST['sucursal'][$n];
                        $this->db->insert('entrada_productos_detalles',$data);
                        
                        //Actualizar stock
                        /*if($edit=='add'){
                            $where = array('producto'=>$data['producto'],'sucursal'=>$data['sucursal']);
                            if(!empty($data['lote']))$where['lote'] = $data['lote'];
                            $producto = $this->db->get_where('productosucursal',$where);

                            $data = array(
                                'producto'=>strtoupper($v),
                                'lote'=>$_POST['lote'][$n],
                                'sucursal'=>$_POST['sucursal'][$n],                                
                                'vencimiento'=>date("Y-m-d",strtotime(str_replace("/","-",$_POST['vencimiento'][$n]))),
                                'precio_venta'=>$_POST['precio_venta'][$n],
                                'stock'=>$_POST['cantidad'][$n]
                            );
                            if($producto->num_rows()==0)
                            $this->db->insert('productosucursal',$data);
                            else
                            $this->db->update('productosucursal',array('stock'=>$producto->row()->stock+$_POST['cantidad'][$n],'precio_venta'=>$data['precio_venta']),$where);                                               
                        }*/
                    }
                }                
                $_SESSION['msj'] = 'Se han guardado los datos con éxito';
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
        }
        
        function anular_compra(){
            $this->form_validation->set_rules('id','ID','required|integer|greather_than[0]');
            if($this->form_validation->run()){
                $detalles = $this->db->get_where('compradetalles',array('compra'=>$this->input->post('id')));
                $distribuciones = $this->db->get_where('distribucion',array('compra'=>$this->input->post('id')));
                
                //Se descuenta del stock global
                foreach($detalles->result() as $d){
                    $stock = $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->stock;
                    $stock-=$d->cantidad;
                    $this->db->update('productos',array('stock'=>$stock),array('codigo'=>$d->producto));
                }
                
                if($distribuciones->num_rows()>0){ //Se descuenta de las sucursales
                {
                    foreach($detalles->result() as $d){//Detalles de la compra                        
                        $distribuciones = $this->db->query("SELECT sucursal,SUM(cantidad) as cantidad FROM distribucion WHERE producto='".$d->producto."' AND compra='".$this->input->post('id')."' group by sucursal");                            
                        foreach($distribuciones->result() as $dis){//Detalle de la distribucion                                                                                   
                                $where = array('producto'=>$d->producto,'sucursal'=>$dis->sucursal);
                                if(!empty($d->lote))$where['lote'] = $d->lote;
                                $producto = $this->db->get_where('productosucursal',$where);
                                $this->db->update('productosucursal',array('stock'=>$producto->row()->stock-$dis->cantidad),$where);                            
                        }
                    }
                }
                $this->db->update('compras',array('status'=>-1),array('id'=>$this->input->post('id')));                
                echo $this->success('factura anulada con exito').refresh_list();
            }
            }
            else echo $this->error('Ha ocurrido un error');
        }
        
         function anular_venta(){
            $this->form_validation->set_rules('id','ID','required|integer');
            if($this->form_validation->run()){
                $detalles = $this->db->get_where('ventadetalle',array('venta'=>$this->input->post('id')));                
                
                //Se descuenta del stock global
                foreach($detalles->result() as $d){
                    $stock = $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->stock;
                    $stock+=$d->cantidad;
                    $this->db->update('productos',array('stock'=>$stock),array('codigo'=>$d->producto));
                    
                    $where = array('producto'=>$d->producto,'sucursal'=>$_SESSION['sucursal']);
                    if(!empty($d->lote))$where['lote'] = $d->lote;
                    $stock = @$this->db->get_where('productosucursal',$where)->row()->stock;
                    $stock = empty($stock)?0:$stock;
                    $this->db->update('productosucursal',array('stock'=>$stock+$d->cantidad),$where);
                }                
                $this->db->update('ventas',array('status'=>-1),array('id'=>$this->input->post('id')));                
                echo $this->success('factura anulada con exito').refresh_list();
            }
            else echo $this->error('Ha ocurrido un error');
        }
        
        function loadView($view = array('view' => 'main'))
        {
            if(is_string($view))$view = array('view'=>$view,'json'=>1);
            parent::loadView($view);
        }
        
        function pagocliente($edit = 'add'){
            $err = ''; 
            foreach($_POST as $p=>$v){                
                 $this->form_validation->set_rules($p,$p,'required');
            }                        
            
                       
            if(isset($_POST['venta'])){
                $x = 0;
                foreach($_POST['venta'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un item</p>';
                    if($v==0)$x = 1;
                }
            }
            else $err .= '<p>Se debe incluir al menos un item</p>';
            if($x==1)$err.= '<p>La cantidad de los itenes deben ser mayores a 0</p>';
            
            if(empty($edit) && $this->db->get_where('pagocliente',array('recibo'=>$_POST['recibo']))->num_rows()>0)
                    $err.='<p>El numero de recibo ya se encuentra registrado</p>';
            
            if($_POST['totalefectivo']==0 && $_POST['totaldebito']==0 && $_POST['totalcredito']==0 && $_POST['totalcheque']==0)
                $err.= '<p>No se ha realizado el pago de esta factura</p>';
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));
                $_POST['totalefectivo'] = empty($_POST['totalefectivo'])?0:$_POST['totalefectivo'];
                $_POST['totalnotacredito'] = empty($_POST['totalnotacredito'])?0:$_POST['totalnotacredito'];
                $_POST['totaldebito'] = empty($_POST['totaldebito'])?0:$_POST['totaldebito'];
                $_POST['totalcheque'] = empty($_POST['totalcheque'])?0:$_POST['totalcheque'];
                $_POST['totalcredito'] = empty($_POST['totalcredito'])?0:$_POST['totalcredito'];
                $data['totalpagado'] = $_POST['totalefectivo']+$_POST['totaldebito']+$_POST['totalnotacredito']+$_POST['totalcheque']+$_POST['totalcredito'];
                $data['sucursal'] = $_SESSION['sucursal'];
                $data['caja'] = $_SESSION['caja'];
                $data['user'] = $_SESSION['user'];
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                if($edit=='edit'){
                    $this->db->update('pagocliente',$data,array('recibo'=>$data['recibo']));
                    $id = $this->db->get_where('pagocliente',array('recibo'=>$data['recibo']))->row()->id;
                    $this->db->delete('pagoclientesfactura',array('pagocliente'=>$id));
                }
                else{
                    $this->db->insert('pagocliente',$data);
                    $id = $this->db->insert_id();
                }
                foreach($_POST['venta'] as $n=>$v){
                    if(!empty($v)){
                        if($_POST['total']>0){
                            $data = array('pagocliente'=>$id);
                            $data['venta'] = $v;
                            $data['nro_factura'] = $_POST['nro_factura'][$n];                        
                            $data['monto'] = $_POST['monto'][$n];
                            $data['total'] = $_POST['total'][$n];
                            $this->db->insert('pagoclientesfactura',$data);
                            if($data['monto']==$data['total'])
                            $this->db->update('ventas',array('status'=>1),array('nro_factura'=>$_POST['nro_factura'][$n]));
                        }
                    }
                }
                $_SESSION['msj'] = 'Se han guardado los datos con exito <a href="javascript:imprimirTicket(\''.$id.'\')">Imprimir Ticket</a> <script>showButton('.$id.')</script>';
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            
        }
        
        function notascreditocliente($edit = 'add'){
            $err = ''; 
            foreach($_POST as $p=>$v){                
                $this->form_validation->set_rules($p,$p,'required');                
            }
            
            if(isset($_POST['codigo'])){
                $x = 0;
                foreach($_POST['codigo'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un producto</p>';
                    if($v==0)$x = 1;
                }
            }
            else $err .= '<p>Se debe incluir al menos un producto</p>';
            //if($x==1)$err.= '<p>La cantidad de los productos deben ser mayores a 0</p>';
            
            if(empty($edit) && $this->db->get_where('notas_credito_client',array('nro_nota_credito'=>$_POST['nro_nota_credito']))->num_rows()>0)
                    $err.='<p>El numero de salida ya se encuentra registrado</p>';
            
            if($_POST['total_monto']==0)
                $err.= '<p>No se ha realizado el calculo de esta nota</p>';
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));
                $data['usuario'] = $_SESSION['user'];
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                if($edit=='edit'){
                    $this->db->update('notas_credito_cliente',$data,array('nro_nota_credito'=>$data['nro_nota_credito']));
                    $id = $data['id'];
                    $this->db->delete('notas_credito_cliente_detalle',array('nota_credito'=>$id));
                }
                else{
                    $this->db->insert('notas_credito_cliente',$data);
                    $id = $this->db->insert_id();
                }
                foreach($_POST['codigo'] as $n=>$v){
                    if(!empty($v)){
                        $data = array('nota_credito'=>$id);
                        $data['producto'] = $v;
                        $data['lote'] = $_POST['lote'][$n];                                                
                        $data['cantidad'] = $_POST['cantidad'][$n];
                        $data['precio_venta'] = $_POST['precio_venta'][$n];
                        $data['total'] = $_POST['total'][$n];                        
                        $this->db->insert('notas_credito_cliente_detalle',$data);                                                
                    }
                }
                $_SESSION['msj'] = 'Se han guardado los datos con exito';
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            
        }
        
        function notascredito($edit = 'add'){
            $err = ''; 
            foreach($_POST as $p=>$v){                
                $this->form_validation->set_rules($p,$p,'required');                
            }
            
            if(isset($_POST['codigo'])){
                $x = 0;
                foreach($_POST['codigo'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un producto</p>';
                    if($v==0)$x = 1;
                }
            }
            else $err .= '<p>Se debe incluir al menos un producto</p>';
            //if($x==1)$err.= '<p>La cantidad de los productos deben ser mayores a 0</p>';
            
            if(empty($edit) && $this->db->get_where('notas_credito',array('nro_nota_credito'=>$_POST['nro_nota_credito']))->num_rows()>0)
                    $err.='<p>El numero de salida ya se encuentra registrado</p>';                        
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));
                $data['usuario'] = $_SESSION['user'];
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                if($edit=='edit'){
                    $this->db->update('notas_credito',$data,array('nro_nota_credito'=>$data['nro_nota_credito']));
                    $id = $data['id'];
                    $this->db->delete('notas_credito_detalles',array('nota_credito'=>$id));
                }
                else{
                    $this->db->insert('notas_credito',$data);
                    $id = $this->db->insert_id();
                }
                foreach($_POST['codigo'] as $n=>$v){
                    if(!empty($v)){
                        $data = array('nota_credito'=>$id);
                        $data['producto'] = $v;
                        $data['lote'] = $_POST['lote'][$n];                                                
                        $data['cantidad'] = $_POST['cantidad'][$n];
                        $data['precio_costo'] = $_POST['precio_costo'][$n];
                        $data['total'] = $_POST['total'][$n];                        
                        $this->db->insert('notas_credito_detalles',$data);                                                
                    }
                }                
                $_SESSION['msj'] = 'Se han guardado los datos con exito';
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            
        }
        
        function pagosproveedor($edit = 'add'){
            $err = ''; 
            foreach($_POST as $p=>$v){
                if(!is_array($v)){
                    $this->form_validation->set_rules($p,$p,'required');                
                }
            }
            
            if(isset($_POST['compra'])){
                $x = 0;
                foreach($_POST['compra'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un producto</p>';
                    if($v==0)$x = 1;
                }
            }
            else $err .= '<p>Se debe incluir al menos un producto</p>';            
            
            if($edit!='edit' && $this->db->get_where('pagoproveedores',array('nro_recibo'=>$_POST['nro_recibo']))->num_rows()>0)
                    $err.='<p>El numero de recibo ya se encuentra registrado</p>'; 
            
            if(!empty($edit) && !empty($_POST['nro_recibo']))
                $id = $this->db->get_where('pagoproveedores',array('nro_recibo'=>$_POST['nro_recibo']));
            if(!empty($edit) && !empty($_POST['nro_recibo']) && $id->num_rows()>0)
                $id = $id->row()->id;
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha'])));
                $data['cajadiaria'] = $_SESSION['cajadiaria'];
                if($edit=='edit'){
                    $this->db->update('pagoproveedores',$data,array('nro_recibo'=>$data['nro_recibo']));                    
                    $this->db->delete('pagoproveedor_detalles',array('pagoproveedor'=>$id));
                }
                else{
                    $this->db->insert('pagoproveedores',$data);
                    $id = $this->db->insert_id();
                }
                foreach($_POST['compra'] as $n=>$v){
                    if(!empty($v)){
                        $data = array('pagoproveedor'=>$id);
                        $data['compra'] = $v;
                        $data['fecha_factura'] = date("Y-m-d",strtotime(str_replace("/","-",$_POST['fecha_factura'][$n])));                                                
                        $data['monto'] = $_POST['monto_factura'][$n];
                        $data['nota_credito'] = $_POST['monto_nota_credito'][$n];
                        $data['total'] = $_POST['total'][$n];
                        $this->db->insert('pagoproveedor_detalles',$data);
                        $this->db->update('compras',array('status'=>1),array('id'=>$v));
                    }
                }
                                
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
