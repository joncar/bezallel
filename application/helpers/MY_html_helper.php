<?php
function input($name = '',$label = '',$type='text')
{
    return '<div class="control-group">
              <label for="email" class="control-label">'.$label.'</label>
               <div class="controls">
                <input type="'.$type.'" class="form-control" name="'.$name.'" id="field-'.$name.'" data-val="required" placeholder="'.$label.'">
                </div>
             </div>';
}

function form_dropdown_from_query($name,$query,$value,$label,$first = 0,$extra = '',$chosen = TRUE,$class = '',$placeholder = 'Todos')
{
    if(is_string($query))$query = get_instance()->db->get($query);
    //$data = array(''=>$placeholder);          
    $data = array(''=>'Seleccione una opcion');
    foreach($query->result() as $q){
        if(count(explode(' ',$label))>0)
        {
            $data[$q->{$value}] = '';
            foreach(explode(' ',$label) as $l)
              $data[$q->{$value}].= $q->{$l}.' ';
        }
        else
        $data[$q->{$value}] = $q->{$label};
    }
    if($chosen)
    return form_dropdown($name,$data,$first,'class="form-control chosen-select '.$class.'" data-placeholder="'.$name.'" '.$extra);
    else
    return form_dropdown($name,$data,$first,'class="form-control '.$class.'" data-placeholder="'.$name.'" '.$extra);
}

function form_relationNN_from_query($name,$query,$value,$label,$seleccionadas = array(),$priority = '',$extra = ''){
    
    $str = '
    <select style="width: 70%; display: none;" size="8" multiple="multiple" class="form-control multiselect" name="'.$name.'">';
    foreach($query->result() as $m){
        
        $labeler = '';
        foreach(explode(' ',$label) as $l)
              $labeler.= $m->{$l}.' ';
              
        $selected = in_array($m->id,$seleccionadas)?'selected':'';
        $str.= '<option '.$selected.' value="'.$m->$value.'">'.$labeler.'</option>';
    }
    $str.= '</select>';
    return $str;
}

function dropdown_query($nombre,$query,$first='0',$style='')
{
    
    $data = array();
    foreach($query->result() as $q)
    $data[$q->id] = $q->nombre;
    
    return '<div class="form-group">
              <label for="email" class="col-sm-2 control-label">'.$nombre.'</label>
               <div class="col-sm-10">
                    '.form_dropdown($nombre,$data,$first,$style).'
                </div>
             </div>';
}

function img($src = '',$style = '',$url = TRUE,$extra = '')
{
    $path = $url?base_url():'';
    $src = empty($src)?'img/vacio.png':$src;
    return '<img src="'.$path.$src.'" style="'.$style.'" '.$extra.'>';
}

function sqlToHtml($qry){
    if($qry->num_rows()>0): ?>
        <table class="table table-bordered table-striped table-hover">
            <thead class="thin-border-bottom">
                <tr>  
                    <?php foreach($qry->row() as $n=>$q): ?>
                        <th>
                            <?= $n ?>
                        </th>
                    <?php endforeach; ?>
                </tr>
            </thead>

            <tbody>
                <?php foreach($qry->result() as $nn=>$qq): ?>
                    <tr>
                        <?php foreach($qq as $n=>$q): ?>
                             <td><?= $q ?></td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif;
}
?>
