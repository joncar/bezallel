<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }
    
    function fecha($val)
    {
        return date($this->ajustes->formato_fecha,strtotime($val));
    }
    
    function moneda($val)
    {
        return $val.' '.$ajustes->moneda;
    }
    
    //Valida si tiene acceso a algun controaldor
    function has_access($controller,$function = '')
    {
        if(!is_numeric($controller))
            $con = $this->db->get_where('controladores',array('nombre'=>$controller));
        
        if(!empty($function) && !is_numeric($function) && (!empty($con) && $con->num_rows()>0 || is_numeric($controller)))
            $fun = $this->db->get_where('funciones',array('nombre'=>$function,'controlador'=>$con->row()->id));
        
        $controller = !empty($con) && $con->num_rows()>0?$con->row()->id:$controller;
        $function = !empty($fun) && $fun->num_rows()>0?$fun->row()->id:$function;        
        
        if($_SESSION['cuenta']!=1)
        {            
            if(!empty($function))
            return $this->db->get_where('roles',array('controlador'=>$controller,'funcion'=>$function,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
            else
            return $this->db->get_where('roles',array('controlador'=>$controller,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
        }
        else
            return TRUE;
    }
    
    function get_menu($controlador,$returnforeach = FALSE)
    {
        $str = '';
        $this->db->select('controladores.id as controlador,funciones.etiqueta as label, funciones.id as funcion_id, controladores.nombre, funciones.nombre as funcion');
        $this->db->join('funciones','funciones.controlador = controladores.id');
        $menu = $this->db->get_where('controladores',array('controladores.nombre'=>$controlador,'funciones.visible'=>1));
        if($menu->num_rows()>0 && $this->has_access($menu->row()->controlador)){
        $str .= '';
        foreach($menu->result() as $m){
        if($this->has_access($m->controlador,$m->funcion_id)){
        $label = empty($m->label)?ucwords($m->funcion):$m->label;
        $str.= '<li><a href="'.base_url($controlador.'/'.$m->funcion).'">'.$label.'</a></li>';
        }
        }
        $str.= '';
        }
        if(!$returnforeach)
        return $str;
        else return $menu->result();
    }
    //Funcion para saber cuantos productos hay disponibles para distribuir entre las sucursales de la compra realizada
    function get_disponible($compra){
        
            $this->db->select('SUM(distribucion.cantidad) as p');
            $productosucursal = $this->db->get_where('distribucion',array('compra'=>$compra));
            
            $this->db->select('SUM(compradetalles.cantidad) as p');
            $compradetalles = $this->db->get_where('compradetalles',array('compra'=>$compra));
            
            if($compradetalles->num_rows()>0 && $productosucursal->num_rows()>0)
            return $compradetalles->row()->p-$productosucursal->row()->p;
            
    }
    
    //Traer el nuevo nro de factura para realizar una venta en la caja seleccionada.
    function get_nro_factura(){
        $sucursal = $_SESSION['sucursal'];
        if(empty($_SESSION['caja'])){
            $caja = $this->db->get_where('cajas',array('sucursal'=>$sucursal));            
        }
        else
            $caja = $this->db->get_where('cajas',array('id'=>$_SESSION['caja']));
        if($caja->num_rows()>0){
            $caja = $caja->row();
            if(empty($_SESSION['caja']))$_SESSION['caja'] = $caja->id;
            $serie = $caja->serie;
            
            $ultima_venta = $this->db->get_where('cajadiaria',array('caja'=>$caja->id,'abierto'=>1));
            $ultima_venta = $serie.($ultima_venta->row()->correlativo+1);
            return $ultima_venta;
        }
        else
            header("Location:".base_url('panel/selsucursal').'?redirect='.$this->router->fetch_class().'/ventas');
    }

    function getVenta($y){

        $ventas = $this->db->get_where('ventas',array('id'=>$y));
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->productos = array();
            $this->db->select('ventadetalle.*,productos.nombre_comercial');
            $this->db->join('productos','productos.codigo = ventadetalle.producto');
            foreach($this->db->get_where('ventadetalle',array('venta'=>$ventas->id))->result() as $v){
                $ventas->productos[] = array(
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_venta'=>$v->precioventa,
                    'por_desc'=>$v->pordesc,
                    'precio_descuento'=>0,
                    'total'=>$v->totalcondesc
                );
            }
            return $ventas;
        }else{
            return null;
        }

    }

    function getServicioDetalle($y){

        $ventas = $this->db->get_where('servicios',array('id'=>$y));
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->productos = array();
            $ventas->fecha_a_entregar = date("Y-m-d",strtotime($ventas->fecha_a_entregar));
            $this->db->select('servicios_detalles.*,productos.codigo,productos.nombre_comercial');
            $this->db->join('productos','productos.id = servicios_detalles.productos_id');
            foreach($this->db->get_where('servicios_detalles',array('servicios_id'=>$ventas->id))->result() as $v){
                $d = array(
                    'id'=>$v->productos_id,
                    'codigo'=>$v->codigo,
                    'nombre'=>$v->nombre_comercial,
                    'categoria_precios_id'=>$v->categoria_precios_id,
                    'cantidad'=>$v->cantidad,
                    'precio_venta'=>$v->precio,                                
                    'total'=>$v->total,
                );

                $rangos = $this->db->get_where('categoria_precios',array('productos_id'=>$v->productos_id));
                $rang = array();
                foreach($rangos->result() as $r){
                    $rang[] = $r;
                }                
                $d['rangos'] = $rang;
                $ventas->productos[] = $d;
            }
            return $ventas;
        }else{
            return null;
        }

    }

    function getServicio($y,$validarFacturacion = TRUE){
        $where['id'] = $y;
        if($validarFacturacion){
            $where['facturado'] = 0;
        }
        $ventas = $this->db->get_where('servicios',$where);
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->tipo = 'Servicio';
            $ventas->productos = array();            
            $ventas->pago_pesos = 0;
            $ventas->pago_dolares = 0;
            $ventas->pago_reales = 0;
            $ventas->pago_guaranies = 0;            
            $ventas->total_efectivo = 0;
            $ventas->total_debito = 0;
            $ventas->total_credito = 0;
            $ventas->servicios_id = $ventas->id;
            $ventas->success = array($this,'successAddService');
            $this->db->select('servicios_detalles.*,productos.nombre_comercial,productos.codigo as producto');
            $this->db->join('productos','productos.id = servicios_detalles.productos_id');
            foreach($this->db->get_where('servicios_detalles',array('servicios_id'=>$ventas->id))->result() as $v){
                $ventas->productos[] = array(
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_venta'=>$v->precio,
                    'por_desc'=>$v->precio,
                    'precio_descuento'=>0,
                    'total'=>$v->total
                );
            }
            return $ventas;
        }else{
            return base_url().'boxes/admin/servicios_detalles/'.$y;
        }

    }

    function successAddService($post,$primary){
        if(!empty($post['servicios_id']) && is_numeric($post['servicios_id'])){
            //Actualizamos la venta en servicios id y facturado = 0
            $this->db->update('servicios',array('facturado'=>1,'nro_factura'=>$primary),array('id'=>$post['servicios_id']));
        }
    }

    
}
?>
