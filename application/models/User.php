<?php

class User extends CI_Model{
        var $log = false;

        function __construct(){
                parent::__construct();
                if(!empty($_SESSION['user'])){
                        $this->log = TRUE;
                        $this->set_variables();
                        $this->createKeyChain();
                }                
        }
        
        function createKeyChain(){
            if(empty($this->keychain) || 1==1){
                if(empty($_SESSION['keychain']) || 1==1){
                    $keychain = array();
                    foreach($this->getAccess('funciones.nombre')->result() as $f){
                        if(!in_array($f->nombre,$keychain)){
                            $keychain[] = $f->nombre;
                        }
                    }
                    $this->keychain = $keychain;
                    $_SESSION['keychain'] = json_encode($this->keychain);
                }else{                    
                    $this->keychain = json_decode($_SESSION['keychain']);
                }
            }
            else{
                if(!is_array($this->keychain)){
                    $this->keychain = json_decode($this->keychain);
                }
            }
        }
        
        function filtrarMenu(){            
            $menu = array();
            $this->db->order_by('orden','ASC');
            foreach($this->db->get_where('menus',array('menus_id'=>NULL))->result() as $m){
                $menu[$m->nombre] = $m;
                $menu[$m->nombre]->submenu = array();
                $this->db->order_by('orden','ASC');
                foreach($this->db->get_where('menus',array('menus_id'=>$m->id))->result() as $m2){
                    $funcion = explode('/',$m2->url);
                    if(in_array($funcion[count($funcion)-1],$this->keychain)){
                        $menu[$m->nombre]->submenu[] = $m2;
                    }
                }
            }
            return $menu;
        }
        
        function getItemMenu($ruta,$label = ''){
            $funcion = explode('/',$ruta);
            $label = empty($label)?  ucfirst(str_replace('_',' ',$funcion)):$label;
            if(in_array($funcion[count($funcion)-1],$this->keychain)){
                return '<li><a href="'.base_url($ruta).'">'.$label.'</a></li>';
            }else{
                return '';
            }
        }

        function login($user,$pass)
        {
                
                $this->db->where('email',$user);
                $this->db->where('password',md5($pass));

                $r = $this->db->get('user');
                if($r->num_rows()>0 && $r->row()->status==1)
                {
                    $this->getParameters($r);                    
                    return true;
                }
                else
                    return false;
        }

        function login_short($id)
        {
                $r = $this->db->get_where('user',array('id'=>$id));
                $this->getParameters($r);
        }

        function getParameters($row)
        {
            $r = $row->row();
            $field_data = $this->db->field_data('user');                    
            foreach($field_data as $f){
                $_SESSION[$f->name] = $r->{$f->name};
            }                    
            $this->set_variables();
        }

        function set_variables(){
            foreach($_SESSION as $n=>$x){
                $this->$n = $x;
            }
            $_SESSION['user'] = $_SESSION['id'];
            $this->db->select('grupos.*');
            $this->db->join('grupos','grupos.id = user_group.grupo');
            $grupos = $this->db->get_where('user_group',array('user'=>$_SESSION['id']));
            $_SESSION['grupos'] = array();
            foreach($grupos->result() as $g){
                $_SESSION['grupos'][$g->id] = $g->nombre;
            }
            $this->user = $_SESSION['user'];
        }
        
        function setVariable($nombre,$valor){
            $_SESSION[$nombre] = $valor;
            $this->set_variables();
        }

        function unlog()
        {
                session_unset();
        }
        
        function getOperations($operation){
            switch($operation){
                case 'list':
                case 'read':
                case 'export':
                case 'print':
                case 'success':
                    return 'lectura';
                break;
                case 'add':
                case 'edit':
                case 'delete':
                    return 'escritura';
                break;
                default: return 'lectura';
                 break;
            }
        }
        
        
        function getAccess($select,$where = array(), $user = ''){
            if(!empty($_SESSION['user'])){
                $this->db->select($select);
                $this->db->join('user_group','user_group.user = user.id');
                $this->db->join('grupos','grupos.id = user_group.grupo');
                $this->db->join('funcion_grupo','funcion_grupo.grupo = grupos.id');
                $this->db->join('funciones','funciones.id = funcion_grupo.funcion');
                $where['user'] = empty($user)?$this->user:$user;
                return $this->db->get_where('user',$where);            
            }
            else {
                header("Location:".base_url());
                exit;
            }
        }
        public $allrequired = array('serviciosDetailModal','sub_categoria_producto','sub_sub_categoria','loginasuser','refresh','unidad_medida','compras_detail','saldo','ver','ventadetalle','create','transferencia_status_change','ventas_detail','anular_venta','anular_acta','next_nro_factura','getProduct','searchProduct','getVenta','inventario_modal','getFactura','inventario_modal_compras');
        function hasAccess(){
            $funcion = $this->router->fetch_method();            
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();            
            $fun = $this->getOperations($crud->getParameters());//Almacenar si es lectura o escritura  
            $permisos = $this->getAccess('grupos.*',array('funciones.nombre'=>$funcion,$fun=>1));
            if($this->router->fetch_class()=='reportes'){
                return true;
            }
            return $permisos->num_rows()>0 || $this->router->fetch_class()=='panel' || in_array($funcion,$this->allrequired)?TRUE:FALSE;            
        }

        function hasGroup($id,$returnbool){
            return $this->getAccess('grupos.*',array('grupos.id'=>$id))->num_rows()>0?TRUE:FALSE;
        }
        
        function getGroups(){
            return $this->getAccess('grupos.*');
        }
        function getGroupsArray(){
            $grupo = $this->getAccess('grupos.*');
            $grupos = array();
            foreach($grupo->result() as $g){
                $grupos[] = $g->id;
            }
            
            return $grupos;
        }
}
?>
