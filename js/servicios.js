var Servicio = function(){
	
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.alt = false;
	this.initEvents = function(){
                var l = this;
                //Seleccionar cliente
                $(document).on('change','#clientes_id',function(){
                        if($(this).val()!=''){
                                l.datos.cliente = $(this).val();
                                if($(this).find('option:selected').data('mayorista')!='undefined'){                                    
                                    l.datos.mayorista = $(this).find('option:selected').data('mayorista');
                                    console.log(l.datos); 
                                }
                                l.updateFields();                                
                        }
                });
                //Buscar cliente
                var busc = false;
	    $(document).on('keyup',"#clientes_id_chzn input[type='text']",function(){      
	      var val = $(this).val();
	      setTimeout(function(){
	        if(val.length>3){
	          $.post(URI+'maestras/clientes/json_list',{   
	            search_field:'',     
	            search_text:val
	          },function(data){       
	            data = JSON.parse(data);   
	            l.selectClient(data,val);
	          });
	        }       
	      },600);
	    });
	    $(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){
	    		l.addProduct($(this).val());
	    	}
	    });

	    $(document).on('keyup','input[name="pago_guaranies"],input[name="total_debito"]',function(e){	    	
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		sendVenta();
	    	}
	    });


	    
	    /* Event precio, cantidad en productos */
	    $(document).on('change','.precio',function(){
	    	var precio_venta = parseFloat($(this).val());
	    	var codigo = $(this).parents('tr').data('codigo');
	    	for(var i in l.datos.productos){
	    		if(l.datos.productos[i].codigo==codigo){
	    			l.datos.productos[i].precio_venta = precio_venta;	    			
	    		}
	    	}
	    	l.updateFields();
	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].codigo==codigo){
                    l.datos.productos[i].cantidad = cantidad;
                    //buscar cat precio
                    l.datos.productos[i].precio_venta = l.getPrecio(l.datos.productos[i],cantidad);
                    l.datos.productos[i].categoria_precios_id = '';
                    /*if(typeof l.datos.productos[i].rangos != 'undefined' && l.datos.productos[i].rangos.length>0){
                    	for(var k in l.datos.productos[i].rangos){
                    		var min = parseFloat(l.datos.productos[i].rangos[k].cantidad_desde);
                  			var max = parseFloat(l.datos.productos[i].rangos[k].cantidad_hasta);
                  			if(cantidad >= min){                    
			                    l.datos.productos[i].precio_venta = parseFloat(l.datos.productos[i].rangos[k].precio);
			                    l.datos.productos[i].categoria_precios_id = l.datos.productos[i].rangos[k].id;
		                    }
                    	}
                    }*/
                }
            }
            console.log(l.datos);
            l.updateFields();
	    });

	    //Event descuentos
	    $(document).on('change','.por_desc',function(){
	    	var tr = $(this).parents('tr').data('codigo');
	    	var descuento = parseFloat($(this).val());
	    	for(var i in l.datos.productos){
	    		if(l.datos.productos[i].codigo==tr){
		    		l.datos.productos[i].por_desc = descuento;
		    		l.datos.productos[i].precio_descuento = (descuento*l.datos.productos[i].precio_venta)/100;
	    		}
	    	}
	    	l.updateFields();
	    });
	    //Event descuentos
	    $(document).on('change','.precio_descuento',function(){
	    	var tr = $(this).parents('tr').data('codigo');
	    	var descuento = parseFloat($(this).val());
	    	for(var i in l.datos.productos){
	    		if(l.datos.productos[i].codigo==tr){
		    		l.datos.productos[i].precio_descuento = descuento;
		    		l.datos.productos[i].por_desc = (descuento*100)/l.datos.productos[i].precio_venta;
	    		}
	    	}
	    	l.updateFields();
	    });

	    //Otros campos
	    $(document).on('change','input,select,textarea',function(){
	    	l.datos[$(this).attr('name')] = $(this).val();
	    	l.updateFields();
	    });

	    //Otros campos
	    $(document).on('change','input[name="imagen"]',function(){
	    	alert('');
	    	l.updateFields();
	    });

	    //Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });

	    //Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
                if(l.alt){                	
                    switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                $("#procesar").modal('toggle');		    				
                        break;
                        case 78: //[n] Nueva venta
                                nuevaVenta();
                        break;
                    }
                }
	    	}

	    });
	}

	this.selectClient = function(data,val){
		var opt = '';
        for(var i in data){
          data[i].mayorista = data[i].mayorista=='activo' || data[i].mayorista=='1'?1:0;
          opt+= '<option value="'+data[i].id+'" data-mayorista="'+data[i].mayorista+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>';
        }
        if(data.length==0){
          opt+= '<option value="">Cliente no existe</option>';
        }
        $("#clientes_id").html(opt);
        $("#clientes_id").chosen().trigger('liszt:updated');
        $("#clientes_id_chzn input[type='text']").val(val);
    	$("#clientes_id").trigger('change');
    	this.updateFields();
	}
        
    this.getPrecio = function(producto,cantidad){         
        var precio = parseFloat(producto.precio_venta);
        if(this.datos.mayorista==1){
            var enc = false;
            console.log(cantidad);
            for(var i in cantidades_mayoristas){
                if(!enc && parseInt(cantidad)>=parseInt(cantidades_mayoristas[i].desde)){
                    var campo = cantidades_mayoristas[i].campo;                                                    
                    precio = parseFloat(producto[campo]);
                    //enc = true;                                     
                }
            }
        }
        return precio;
    }

	this._addProduct = function(producto,cantidad){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
                if(pr[i].codigo==producto.codigo){
                        enc = true;
                        this.datos.productos[i].cantidad+=cantidad;				
                }
        }
        if(!enc){
                var precio = this.getPrecio(producto,cantidad);                 
                this.datos.productos.push({
                	id:producto.id,
                    codigo:producto.codigo,
                    nombre:producto.nombre_comercial,
                    cantidad:cantidad,
                    stock:producto.stock,
                    por_desc:0,
                    precio_venta:precio,
                    precio_venta_mayorista1: parseFloat(producto.precio_venta_mayorista1),
                    precio_venta_mayorista2: parseFloat(producto.precio_venta_mayorista2),
                    precio_venta_mayorista3: parseFloat(producto.precio_venta_mayorista3),
                    precio_descuento:0,
                    total: 0
                });
                var l = this;
                $.post(URI+'boxes/admin/categoria_precios/json_list',{
                	'search_field[]':'productos_id',
                	'search_text[]':producto.id,
                	'operator':'where'
                },function(data){
                	data = JSON.parse(data);
                	if(data.length>0){
	                	for(var i in l.datos.productos){
	                		if(l.datos.productos[i].id==data[0].producto){
	                			l.datos.productos[i].rangos = data;                			
	                		}
	                	}
                	}
                	l.updateFields();
                });
        }

        
	}
        
    this.getBalanza = function(codigo){
        var numerosBalanza = codigo_balanza.toString().length;
        if(codigo.toString().substring(0,numerosBalanza)==codigo_balanza){
            var peso = parseInt(codigo.toString().substring(7,12));
            peso = parseInt(peso)/1000;
            codigo = parseInt(codigo.toString().substring(2,7));
            codigo = {
                codigo: codigo,
                cantidad: peso
            }
            return codigo;  
        }else{
            return codigo;  
        }
    }

	this.addProduct = function(codigo){                                
		//buscamos el codigo
		var l = this;				
		if(codigo!=''){
            codigo = codigo.split('*');
            if(codigo.length==2){
                    cantidad = parseFloat(codigo[0]);
                    codigo = codigo[1];
            }else{
                    codigo = codigo[0];
                    codigo = codigo.split('+');
                    if(codigo.length==2){
                            cantidad = parseFloat(codigo[0]);
                            codigo = codigo[1];	
                    }
                    else{
                            cantidad = 1;
                            codigo = codigo[0];	
                    }
            }
            
            //Es balanza?                                        
            codigo = this.getBalanza(codigo);                                        
            cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
            codigo = typeof codigo == 'object'?codigo.codigo:codigo;
            console.log(codigo); 
            $.post(URI+'movimientos/productos/productos/json_list',{
                    'search_field[]':'codigo',
                    'search_text[]':codigo,
                    'operator':'where',
                    'cliente':this.datos.cliente
            },function(data){
                    data = JSON.parse(data);
                    if(data.length>0){
                    	if(vender_sin_stock==1 || (cantidad>0 && parseFloat(data[0].stock)>=cantidad) || (data[0].inventariable=='inactivo')){
                            $("#codigoAdd").val('');
                            l._addProduct(data[0],cantidad);
                            $("#codigoAdd").attr('placeholder','Código de producto');
                            $("#codigoAdd").removeClass('error');
                        }else{
                        	$("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Sin stock suficiente');
                            $("#codigoAdd").addClass('error');
                        }
                    }else{
                            $("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Producto no encontrado');
                            $("#codigoAdd").addClass('error');
                    }
            });
		}
	}

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].codigo!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateFields();
	}

	this.updateFields = function(){
		this.datos.cliente = $("#clientes_id").val();
		this.datos.transaccion = $("#transaccion").val();
		this.datos.forma_pago = $("#formapago").val();
		this.datos.observacion = $("#observacion").val();						
		this.datos.pedido = $("#field-pedido").val();
		this.datos.pagado = $("#field-pagado").val();
		this.datos.imagen = $("input[name='imagen']").val();
		//Calcular total
		var pr = this.datos.productos;
		var total = 0;
		for(var i in pr){
			this.datos.productos[i].total = pr[i].cantidad*pr[i].precio_venta;
			total+= this.datos.productos[i].total;
		}		
		this.datos.total_venta = total;
		//Calcular divisas
		this.datos.total_dolares = total/tasa_dolar;
		this.datos.total_reales = total/tasa_real;
		this.datos.total_pesos = total/tasa_peso;				
		this.refresh();
	}

	this.refresh = function(){
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		const usd = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'USD',minimumFractionDigits: 2});
		const arg = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'ARP',minimumFractionDigits: 2});
		const br = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'BRL',minimumFractionDigits: 2});
		
		$("input[name='total_venta']").val(gs.format(this.datos.total_venta));
		$("input[name='total_pesos']").val(arg.format(this.datos.total_pesos));
		$("input[name='total_reales']").val(br.format(this.datos.total_reales));
		$("input[name='total_dolares']").val(usd.format(this.datos.total_dolares));		
		$("input[name='total_debito']").val(gs.format(this.datos.total_debito));
		$("input[name='efectivo']").val(this.datos.total_efectivo);	

		$("input[name='descripcion_trabajo']").val(this.datos.descripcion_trabajo);
		$("input[name='fecha_a_entregar']").val(this.datos.fecha_a_entregar);
		$("input[name='hora_entrega']").val(this.datos.hora_entrega);
		$("input[name='entrega_inicial']").val(this.datos.entrega_inicial);	
		$("select[name='empleados_id']").val(this.datos.empleados_id);			
		$("select[name='empleados_id']").chosen().trigger('liszt:updated');

		$("#total_total span").html(gs.format(this.datos.total_venta));

		$("#cantidadProductos").html(this.datos.productos.length);		
		$("#procesar_total_efectivo").html(gs.format(this.datos.total_efectivo));

		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			newRow.attr('data-codigo',pr[i].codigo);
			$(td[0]).find('span').html(pr[i].codigo);
			$(td[1]).html(pr[i].nombre);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(pr[i].precio_venta.toFixed(0));			
			$(td[4]).html(gs.format(pr[i].total));
			$(td[5]).html(pr[i].stock);
			this.productoshtml.append(newRow);			
		}		
	}

	this.setDatos = function(datos){		
		this.datos = datos;
		this.datos.total_venta = parseFloat(this.datos.total_venta);
		this.datos.total_pesos = parseFloat(this.datos.total_pesos);
		this.datos.total_reales = parseFloat(this.datos.total_reales);
		this.datos.total_dolares = parseFloat(this.datos.total_dolares);		
		this.datos.total_efectivo = parseFloat(this.datos.total_efectivo);			
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			this.datos.productos[i].precio_venta = parseFloat(this.datos.productos[i].precio_venta);			
			this.datos.productos[i].total = parseFloat(this.datos.productos[i].total);
		}	
		this.refresh();	
	}

	this.initVenta = function(){
		this.datos = {
			fecha:'',			
			clientes_id:0,
			descripcion_trabajo:'',							
			fecha_a_entregar:'',
			hora_entrega:'',
			entrega_inicial:0,
			empleados_id:0,
			facturado:0,
			anulado:0,
			nro_factura:0,
			sucursales_id:'',			
			cajadiaria_id:'',
			imagen:'',
			total_venta:0,
			total_dolares:0,
			total_reales:0,
			total_pesos:0,
			total_efectivo:0,			
			productos:[],
            mayorista:0,        
            ubicacion:''
		};		
		var l = this;
		$(document).find('input,select,textarea').each(function(){
	    	l.datos[$(this).attr('name')] = $(this).val();
	    	l.updateFields();
	    });
	}

	this.initVenta();
}

